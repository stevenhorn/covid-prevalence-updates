0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -695.45    32.25
p_loo       24.34        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.054   0.156    0.329  ...   152.0     152.0      19.0   1.13
pu        0.786  0.027   0.740    0.825  ...    89.0      97.0      59.0   1.00
mu        0.133  0.022   0.090    0.169  ...    35.0      37.0      81.0   1.00
mus       0.171  0.031   0.125    0.225  ...   152.0     152.0     100.0   1.07
gamma     0.190  0.042   0.123    0.259  ...   152.0     152.0      53.0   1.02
Is_begin  0.742  0.724   0.007    2.137  ...    95.0     113.0      97.0   0.99
Ia_begin  1.550  1.487   0.011    4.671  ...   152.0     116.0      38.0   0.98
E_begin   0.761  0.870   0.002    2.794  ...   137.0      65.0      59.0   1.01

[8 rows x 11 columns]