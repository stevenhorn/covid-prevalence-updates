0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1078.76    44.46
p_loo       29.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.252   0.051   0.170    0.343  ...   119.0     140.0      88.0   0.99
pu         0.791   0.056   0.713    0.900  ...    26.0      27.0      14.0   1.07
mu         0.146   0.022   0.106    0.181  ...    23.0      22.0      60.0   1.07
mus        0.210   0.031   0.153    0.255  ...    72.0      71.0      96.0   0.99
gamma      0.305   0.050   0.224    0.418  ...    38.0      44.0      33.0   1.02
Is_begin   9.535   5.299   1.367   19.826  ...    57.0      55.0      58.0   1.03
Ia_begin  24.962  11.570   2.404   41.533  ...    30.0      29.0      38.0   1.02
E_begin   32.432  22.699   2.966   71.283  ...    24.0      29.0      35.0   1.02

[8 rows x 11 columns]