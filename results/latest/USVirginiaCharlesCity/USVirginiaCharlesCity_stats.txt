0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -405.85    24.14
p_loo       19.23        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.272  0.049   0.185    0.346  ...    95.0     100.0      58.0   1.11
pu        0.875  0.021   0.833    0.900  ...    34.0      33.0      40.0   1.07
mu        0.133  0.027   0.093    0.189  ...    48.0      47.0      59.0   1.02
mus       0.158  0.029   0.112    0.202  ...    70.0      69.0      60.0   1.00
gamma     0.181  0.040   0.112    0.245  ...    60.0      64.0     100.0   1.02
Is_begin  1.111  0.803   0.002    2.530  ...   152.0     115.0      42.0   1.01
Ia_begin  2.007  1.936   0.057    6.564  ...    72.0      53.0      58.0   1.03
E_begin   1.016  1.015   0.004    2.660  ...    83.0      63.0      60.0   1.02

[8 rows x 11 columns]