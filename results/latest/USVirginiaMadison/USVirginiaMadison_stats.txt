0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -496.98    21.14
p_loo       16.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.272  0.041   0.201    0.336  ...    85.0      82.0      40.0   1.02
pu        0.870  0.025   0.821    0.899  ...    53.0      62.0      53.0   1.01
mu        0.124  0.018   0.093    0.149  ...    46.0      45.0      57.0   1.03
mus       0.151  0.029   0.094    0.203  ...    78.0      72.0      59.0   1.01
gamma     0.170  0.026   0.129    0.221  ...   116.0     121.0      96.0   1.01
Is_begin  0.863  0.788   0.035    2.211  ...   139.0      86.0      43.0   1.02
Ia_begin  1.625  1.544   0.031    4.669  ...    37.0      37.0      43.0   1.04
E_begin   0.752  0.839   0.019    1.809  ...    99.0      51.0      60.0   1.02

[8 rows x 11 columns]