0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -671.00    21.24
p_loo       19.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.050   0.174    0.347  ...   126.0     117.0      59.0   1.01
pu        0.873  0.013   0.853    0.896  ...    58.0      73.0      31.0   1.03
mu        0.118  0.024   0.080    0.167  ...    22.0      23.0      79.0   1.05
mus       0.159  0.030   0.112    0.216  ...   152.0     152.0      84.0   0.99
gamma     0.183  0.048   0.112    0.283  ...    99.0     152.0      45.0   1.06
Is_begin  0.814  0.946   0.003    2.672  ...   112.0      80.0      58.0   1.02
Ia_begin  1.675  1.523   0.075    4.289  ...   106.0     145.0      61.0   0.98
E_begin   1.064  1.211   0.018    2.918  ...   102.0      65.0      60.0   1.02

[8 rows x 11 columns]