0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -971.95    33.72
p_loo       23.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.214  0.039   0.160    0.281  ...    72.0      71.0      86.0   0.99
pu        0.725  0.017   0.702    0.752  ...    76.0      75.0      59.0   0.99
mu        0.125  0.016   0.099    0.156  ...    68.0      62.0      49.0   0.99
mus       0.180  0.034   0.124    0.239  ...    36.0      36.0      93.0   1.04
gamma     0.200  0.047   0.128    0.269  ...   136.0     122.0      86.0   0.99
Is_begin  1.444  1.089   0.064    3.621  ...    68.0      48.0      97.0   1.04
Ia_begin  0.845  0.609   0.017    2.008  ...   100.0      64.0      36.0   1.07
E_begin   0.944  0.724   0.003    2.322  ...    21.0      23.0      58.0   1.08

[8 rows x 11 columns]