0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -922.53    21.31
p_loo       20.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.211  0.043   0.151    0.303  ...    73.0      57.0      17.0   1.04
pu        0.732  0.027   0.701    0.783  ...   152.0     152.0      60.0   1.04
mu        0.101  0.021   0.063    0.134  ...     9.0      12.0      40.0   1.15
mus       0.194  0.033   0.144    0.253  ...   113.0     152.0      58.0   1.02
gamma     0.285  0.046   0.203    0.363  ...   100.0     108.0      77.0   1.01
Is_begin  0.974  0.685   0.070    2.311  ...   152.0     122.0      59.0   0.99
Ia_begin  3.783  2.244   0.551    7.676  ...    34.0      23.0      55.0   1.06
E_begin   4.587  4.719   0.170   13.595  ...    66.0      55.0      59.0   1.01

[8 rows x 11 columns]