0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -605.20    23.21
p_loo       26.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.050   0.174    0.336  ...   130.0     130.0      86.0   1.00
pu        0.788  0.029   0.741    0.840  ...    13.0      14.0      47.0   1.11
mu        0.119  0.029   0.069    0.158  ...    22.0      22.0      53.0   1.07
mus       0.151  0.030   0.103    0.199  ...    92.0     152.0      49.0   1.00
gamma     0.181  0.040   0.121    0.260  ...   122.0     152.0      60.0   0.99
Is_begin  0.849  0.804   0.015    2.597  ...   122.0     152.0      97.0   1.00
Ia_begin  1.486  1.298   0.010    4.107  ...   146.0     104.0      56.0   1.01
E_begin   0.778  0.872   0.002    2.530  ...   126.0      81.0      60.0   1.00

[8 rows x 11 columns]