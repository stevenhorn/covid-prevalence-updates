0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -720.45    25.42
p_loo       21.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      283   95.9%
 (0.5, 0.7]   (ok)          7    2.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.056   0.165    0.347  ...    74.0      71.0      58.0   1.00
pu        0.816  0.021   0.781    0.854  ...    21.0      29.0      30.0   1.05
mu        0.128  0.020   0.087    0.163  ...    26.0      30.0      56.0   1.09
mus       0.165  0.043   0.103    0.256  ...   152.0     152.0      74.0   1.07
gamma     0.186  0.039   0.129    0.258  ...    95.0     113.0      60.0   1.00
Is_begin  0.654  0.607   0.014    1.711  ...   137.0      84.0      33.0   1.02
Ia_begin  1.608  1.617   0.015    4.903  ...    76.0      48.0      52.0   1.03
E_begin   0.647  0.515   0.030    1.807  ...    31.0      30.0      54.0   1.06

[8 rows x 11 columns]