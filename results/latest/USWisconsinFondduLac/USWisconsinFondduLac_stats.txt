0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1208.60    27.67
p_loo       18.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.045   0.184    0.347  ...    26.0      29.0      33.0   1.06
pu        0.794  0.027   0.740    0.830  ...    59.0      77.0      20.0   1.08
mu        0.120  0.023   0.077    0.163  ...    25.0      26.0      53.0   1.10
mus       0.141  0.028   0.096    0.191  ...   113.0     112.0      83.0   1.01
gamma     0.149  0.038   0.082    0.218  ...    97.0      92.0      55.0   1.01
Is_begin  0.914  0.765   0.001    2.165  ...    67.0      38.0      60.0   1.06
Ia_begin  1.930  1.797   0.073    5.299  ...    97.0      72.0      86.0   1.02
E_begin   0.790  0.668   0.006    2.020  ...    72.0      42.0      57.0   1.02

[8 rows x 11 columns]