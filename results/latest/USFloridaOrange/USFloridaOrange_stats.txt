4 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1735.14    31.10
p_loo       26.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      260   88.1%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)        12    4.1%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.240   0.045   0.168    0.322  ...    90.0      97.0      59.0   1.02
pu         0.754   0.037   0.703    0.822  ...   128.0     140.0      60.0   1.01
mu         0.137   0.024   0.103    0.182  ...     7.0       7.0      87.0   1.25
mus        0.197   0.035   0.140    0.262  ...   123.0     152.0      77.0   1.01
gamma      0.307   0.059   0.218    0.429  ...   149.0     150.0     100.0   1.00
Is_begin   9.351   4.790   1.974   17.360  ...   106.0      87.0      53.0   1.00
Ia_begin  22.704  12.781   0.587   44.857  ...   128.0     110.0      60.0   0.98
E_begin   27.710  18.998   1.544   71.845  ...   139.0     117.0      69.0   1.04

[8 rows x 11 columns]