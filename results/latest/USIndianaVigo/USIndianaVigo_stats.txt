0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1084.45    28.50
p_loo       25.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.057   0.153    0.337  ...    71.0      64.0      39.0   1.01
pu        0.804  0.029   0.761    0.858  ...    26.0      25.0      59.0   1.07
mu        0.126  0.026   0.088    0.175  ...    34.0      26.0      40.0   1.09
mus       0.176  0.031   0.130    0.249  ...   152.0     152.0      88.0   1.00
gamma     0.217  0.038   0.158    0.292  ...   116.0     112.0      59.0   1.05
Is_begin  1.022  0.847   0.026    2.588  ...   151.0     102.0      60.0   0.99
Ia_begin  0.714  0.502   0.005    1.682  ...   140.0     121.0      91.0   1.05
E_begin   0.672  0.795   0.002    1.862  ...   106.0      71.0      83.0   1.00

[8 rows x 11 columns]