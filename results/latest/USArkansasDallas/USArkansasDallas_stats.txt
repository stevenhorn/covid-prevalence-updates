0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -599.60    36.47
p_loo       29.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.061   0.166    0.350  ...   115.0     101.0      59.0   1.03
pu        0.853  0.020   0.806    0.881  ...    94.0      94.0      83.0   1.02
mu        0.163  0.028   0.106    0.209  ...    34.0      34.0      54.0   1.05
mus       0.178  0.038   0.114    0.243  ...    61.0      64.0      59.0   1.02
gamma     0.208  0.039   0.142    0.283  ...    56.0      56.0      80.0   1.04
Is_begin  0.584  0.553   0.015    1.648  ...   143.0     152.0     100.0   1.00
Ia_begin  1.229  1.190   0.015    3.126  ...   107.0     101.0      91.0   0.99
E_begin   0.488  0.472   0.007    1.302  ...   108.0      54.0      31.0   1.03

[8 rows x 11 columns]