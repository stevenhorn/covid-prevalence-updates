0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -492.70    33.66
p_loo       27.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      264   89.5%
 (0.5, 0.7]   (ok)         28    9.5%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.056   0.158    0.342  ...   152.0     152.0      60.0   0.98
pu        0.843  0.022   0.806    0.880  ...    82.0      84.0      25.0   1.02
mu        0.132  0.025   0.085    0.170  ...    47.0      46.0      96.0   1.03
mus       0.171  0.031   0.109    0.234  ...   152.0     152.0      60.0   0.99
gamma     0.197  0.040   0.128    0.264  ...    65.0      56.0      88.0   1.04
Is_begin  0.643  0.578   0.004    1.884  ...   120.0      79.0      40.0   1.00
Ia_begin  1.422  1.644   0.008    3.526  ...    86.0      99.0      91.0   1.01
E_begin   0.516  0.546   0.002    1.561  ...    80.0      38.0      53.0   1.03

[8 rows x 11 columns]