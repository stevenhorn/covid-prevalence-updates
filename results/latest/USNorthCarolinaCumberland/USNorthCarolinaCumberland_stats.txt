0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1281.31    30.83
p_loo       22.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.051   0.171    0.338  ...    92.0      85.0      59.0   1.02
pu        0.816  0.049   0.732    0.888  ...   102.0      89.0      60.0   0.99
mu        0.103  0.021   0.074    0.140  ...    22.0      23.0      38.0   1.11
mus       0.141  0.029   0.100    0.196  ...    93.0      91.0      60.0   1.01
gamma     0.171  0.027   0.130    0.228  ...   152.0     152.0      97.0   1.01
Is_begin  1.429  1.007   0.097    3.416  ...    97.0      90.0      34.0   1.01
Ia_begin  0.737  0.661   0.003    1.930  ...   107.0      33.0      40.0   1.07
E_begin   1.084  1.445   0.002    2.750  ...   101.0      71.0      42.0   1.03

[8 rows x 11 columns]