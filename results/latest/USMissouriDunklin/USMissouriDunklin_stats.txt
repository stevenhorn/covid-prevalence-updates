0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -903.97    40.19
p_loo       28.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.053   0.156    0.328  ...    32.0      31.0      19.0   1.05
pu        0.841  0.020   0.794    0.874  ...    12.0      13.0      17.0   1.12
mu        0.114  0.023   0.083    0.151  ...    11.0      18.0      22.0   1.32
mus       0.144  0.030   0.100    0.190  ...    72.0      78.0      88.0   1.03
gamma     0.155  0.033   0.092    0.220  ...    29.0      35.0      60.0   1.06
Is_begin  0.869  0.798   0.016    2.346  ...   152.0      76.0      40.0   1.02
Ia_begin  1.922  1.313   0.143    4.371  ...    40.0      25.0      40.0   1.09
E_begin   0.997  0.865   0.012    2.883  ...   112.0      68.0      18.0   1.02

[8 rows x 11 columns]