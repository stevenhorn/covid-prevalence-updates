0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -774.23    62.21
p_loo       50.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    5    1.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.177  0.019   0.153    0.216  ...   152.0     152.0      59.0   1.00
pu        0.710  0.010   0.700    0.731  ...    93.0      77.0      59.0   1.01
mu        0.140  0.024   0.091    0.179  ...    41.0      42.0      60.0   1.04
mus       0.215  0.038   0.148    0.281  ...   152.0     152.0      61.0   1.00
gamma     0.289  0.046   0.199    0.367  ...    95.0      98.0      60.0   1.00
Is_begin  0.228  0.281   0.000    0.751  ...    96.0      75.0     100.0   0.99
Ia_begin  0.288  0.444   0.012    1.270  ...    75.0      74.0      93.0   1.00
E_begin   0.139  0.201   0.002    0.595  ...    80.0      69.0      95.0   1.01

[8 rows x 11 columns]