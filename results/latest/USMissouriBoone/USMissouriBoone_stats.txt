0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1309.17    34.29
p_loo       27.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.059   0.159    0.347  ...   152.0     152.0      25.0   1.05
pu        0.826  0.021   0.788    0.866  ...    88.0      81.0      49.0   0.99
mu        0.130  0.028   0.083    0.189  ...    14.0      13.0      39.0   1.14
mus       0.187  0.044   0.106    0.256  ...   100.0     111.0      95.0   1.03
gamma     0.196  0.042   0.127    0.271  ...    52.0      55.0      40.0   1.03
Is_begin  1.146  1.108   0.006    3.629  ...   152.0      69.0      33.0   1.03
Ia_begin  2.407  2.399   0.000    6.574  ...   110.0     118.0      55.0   0.98
E_begin   1.256  1.594   0.001    3.387  ...   102.0      69.0      75.0   1.02

[8 rows x 11 columns]