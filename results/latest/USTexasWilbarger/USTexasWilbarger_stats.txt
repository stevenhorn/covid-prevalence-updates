2 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -892.31    43.83
p_loo       44.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      286   96.9%
 (0.5, 0.7]   (ok)          4    1.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.224  0.042   0.156    0.289  ...   122.0     124.0      81.0   0.99
pu        0.728  0.018   0.701    0.759  ...   152.0     152.0     100.0   1.00
mu        0.113  0.023   0.080    0.152  ...    49.0      67.0      59.0   1.07
mus       0.161  0.035   0.105    0.225  ...    78.0      80.0      93.0   1.00
gamma     0.163  0.035   0.119    0.225  ...    74.0      75.0      40.0   1.02
Is_begin  0.448  0.448   0.005    1.337  ...   114.0      77.0      43.0   1.00
Ia_begin  0.799  0.945   0.018    2.769  ...    61.0     112.0      59.0   1.06
E_begin   0.372  0.448   0.005    1.311  ...    56.0      63.0      59.0   1.01

[8 rows x 11 columns]