0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -761.64    47.15
p_loo       34.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.053   0.161    0.333  ...    74.0      65.0      38.0   0.99
pu        0.781  0.044   0.705    0.841  ...     6.0       7.0      22.0   1.31
mu        0.124  0.027   0.081    0.177  ...    19.0      20.0      38.0   1.06
mus       0.169  0.037   0.105    0.230  ...    66.0      68.0      57.0   1.02
gamma     0.191  0.038   0.127    0.250  ...   126.0     140.0     102.0   0.98
Is_begin  0.543  0.649   0.016    1.813  ...    60.0      67.0      74.0   1.03
Ia_begin  1.040  1.114   0.009    3.567  ...    74.0      66.0      60.0   1.05
E_begin   0.455  0.578   0.002    1.464  ...    66.0      78.0      59.0   0.99

[8 rows x 11 columns]