0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -490.07    39.06
p_loo       28.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      284   96.3%
 (0.5, 0.7]   (ok)          7    2.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.050   0.159    0.327  ...   108.0      93.0      40.0   1.05
pu        0.760  0.026   0.727    0.815  ...   107.0     110.0      38.0   1.11
mu        0.118  0.026   0.076    0.172  ...    32.0      39.0      48.0   1.04
mus       0.162  0.032   0.113    0.216  ...   152.0     152.0      93.0   0.99
gamma     0.183  0.041   0.130    0.259  ...   128.0     137.0      96.0   0.98
Is_begin  0.403  0.402   0.007    1.293  ...    61.0      62.0      88.0   1.02
Ia_begin  0.721  1.075   0.004    2.842  ...    91.0      72.0      96.0   1.05
E_begin   0.361  0.438   0.005    1.247  ...    79.0      63.0      58.0   1.01

[8 rows x 11 columns]