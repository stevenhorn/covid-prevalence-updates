0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -908.92    32.08
p_loo       20.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      286   96.9%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.055   0.153    0.333  ...    91.0      90.0      63.0   0.99
pu        0.848  0.033   0.759    0.886  ...    18.0      16.0      40.0   1.11
mu        0.135  0.029   0.083    0.185  ...     9.0       8.0      60.0   1.20
mus       0.154  0.026   0.108    0.198  ...   152.0     152.0      69.0   1.00
gamma     0.170  0.031   0.127    0.240  ...    25.0      27.0      87.0   1.06
Is_begin  0.772  0.824   0.012    2.353  ...    63.0      59.0      36.0   1.03
Ia_begin  0.621  0.505   0.002    1.762  ...   105.0      81.0      60.0   1.00
E_begin   0.475  0.526   0.003    1.538  ...    24.0      13.0      33.0   1.14

[8 rows x 11 columns]