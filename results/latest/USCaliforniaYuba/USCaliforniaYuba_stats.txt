0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1022.10    25.62
p_loo       20.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         24    8.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.060   0.151    0.341  ...    80.0      77.0      57.0   1.02
pu        0.870  0.013   0.846    0.891  ...    54.0      49.0      40.0   1.04
mu        0.154  0.025   0.114    0.196  ...    13.0      14.0      78.0   1.12
mus       0.150  0.030   0.100    0.196  ...   145.0     152.0      59.0   1.02
gamma     0.179  0.026   0.140    0.233  ...    79.0      83.0      99.0   0.99
Is_begin  0.804  0.647   0.082    2.006  ...   110.0      82.0      60.0   1.00
Ia_begin  1.833  1.787   0.003    5.378  ...    33.0      24.0      66.0   1.07
E_begin   0.954  1.016   0.002    2.980  ...    50.0      18.0      59.0   1.09

[8 rows x 11 columns]