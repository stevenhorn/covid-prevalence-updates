1 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1142.57    18.17
p_loo       19.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.055   0.151    0.321  ...   105.0     100.0      40.0   1.03
pu        0.764  0.041   0.700    0.834  ...   152.0     150.0      56.0   0.99
mu        0.116  0.017   0.084    0.144  ...    35.0      36.0      41.0   1.03
mus       0.172  0.030   0.115    0.228  ...   131.0     152.0      59.0   1.07
gamma     0.220  0.031   0.168    0.273  ...   151.0     145.0      80.0   1.09
Is_begin  0.896  0.743   0.042    2.288  ...   145.0     152.0      80.0   1.01
Ia_begin  3.164  1.836   0.192    5.814  ...   124.0     134.0      60.0   1.03
E_begin   3.150  2.917   0.018    9.218  ...    98.0      96.0      60.0   1.03

[8 rows x 11 columns]