0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -2034.85    26.10
p_loo       19.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.052   0.153    0.317  ...    97.0     104.0      93.0   0.99
pu        0.780  0.027   0.724    0.822  ...    66.0      64.0      59.0   1.00
mu        0.111  0.015   0.087    0.146  ...    33.0      37.0      59.0   1.03
mus       0.155  0.026   0.111    0.201  ...   109.0     130.0      54.0   1.00
gamma     0.213  0.033   0.160    0.280  ...    97.0      80.0      69.0   1.01
Is_begin  1.615  1.069   0.034    3.375  ...   107.0      56.0      37.0   1.01
Ia_begin  4.593  2.742   0.600    8.694  ...   152.0     140.0     100.0   0.98
E_begin   4.086  3.888   0.093   11.747  ...    78.0      73.0      43.0   1.00

[8 rows x 11 columns]