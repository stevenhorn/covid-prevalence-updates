0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -970.01    26.32
p_loo       23.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      262   88.8%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         9    3.1%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.045   0.171    0.326  ...   109.0     112.0      60.0   1.07
pu        0.779  0.024   0.732    0.822  ...    33.0      36.0      83.0   1.04
mu        0.144  0.032   0.099    0.203  ...     5.0       6.0      49.0   1.41
mus       0.184  0.032   0.130    0.231  ...    71.0      77.0      97.0   1.01
gamma     0.248  0.040   0.174    0.308  ...    52.0      54.0      48.0   1.01
Is_begin  0.864  0.795   0.003    2.445  ...   101.0      94.0      53.0   1.01
Ia_begin  1.865  1.659   0.132    5.334  ...   114.0      94.0      59.0   1.00
E_begin   1.310  1.360   0.003    4.335  ...    98.0      61.0      58.0   1.01

[8 rows x 11 columns]