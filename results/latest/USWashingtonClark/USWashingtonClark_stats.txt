0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1398.04    47.78
p_loo       47.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.055   0.172    0.345  ...    43.0      49.0      40.0   1.05
pu        0.782  0.048   0.704    0.855  ...    38.0      40.0      38.0   1.04
mu        0.161  0.035   0.109    0.234  ...     7.0       7.0      22.0   1.25
mus       0.224  0.039   0.166    0.309  ...    79.0      93.0      39.0   1.00
gamma     0.294  0.047   0.221    0.389  ...    63.0      84.0      96.0   0.99
Is_begin  0.849  0.613   0.034    2.263  ...   117.0      77.0      30.0   1.05
Ia_begin  2.781  1.772   0.297    6.298  ...    87.0      81.0      88.0   0.99
E_begin   2.005  2.136   0.019    5.677  ...    84.0      65.0      39.0   1.01

[8 rows x 11 columns]