0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -497.81    28.88
p_loo       23.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.279  0.048   0.199    0.345  ...    89.0      94.0      61.0   1.00
pu        0.888  0.010   0.873    0.900  ...    87.0      84.0      37.0   1.02
mu        0.146  0.025   0.104    0.197  ...    47.0      49.0      59.0   1.01
mus       0.166  0.027   0.106    0.204  ...   114.0     136.0      60.0   1.03
gamma     0.193  0.035   0.144    0.270  ...    51.0      63.0      60.0   1.01
Is_begin  0.855  0.870   0.012    2.996  ...   152.0      97.0      96.0   1.01
Ia_begin  1.761  1.666   0.025    5.087  ...    84.0      73.0      57.0   1.02
E_begin   0.717  0.849   0.009    2.204  ...    88.0      83.0      59.0   1.03

[8 rows x 11 columns]