0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -732.76    25.97
p_loo       25.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.053   0.150    0.320  ...    13.0      13.0      15.0   1.11
pu        0.834  0.022   0.801    0.874  ...    15.0      13.0      35.0   1.16
mu        0.140  0.033   0.085    0.195  ...    26.0      27.0      49.0   1.06
mus       0.170  0.027   0.121    0.219  ...   152.0     152.0      97.0   1.00
gamma     0.213  0.037   0.153    0.291  ...   110.0     101.0      60.0   1.00
Is_begin  0.666  0.638   0.001    1.942  ...    98.0      73.0      59.0   1.01
Ia_begin  1.399  1.582   0.080    4.318  ...    86.0      86.0      88.0   1.03
E_begin   0.545  0.593   0.004    1.742  ...    86.0      70.0      48.0   1.00

[8 rows x 11 columns]