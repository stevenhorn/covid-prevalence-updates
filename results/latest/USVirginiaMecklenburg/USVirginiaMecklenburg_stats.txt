0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -858.69    29.36
p_loo       26.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.045   0.187    0.349  ...    82.0      90.0      31.0   1.03
pu        0.856  0.035   0.791    0.896  ...     9.0      12.0      44.0   1.14
mu        0.148  0.025   0.104    0.195  ...    22.0      26.0      36.0   1.03
mus       0.186  0.036   0.132    0.254  ...    72.0      60.0      38.0   1.02
gamma     0.256  0.049   0.176    0.340  ...   152.0     152.0      91.0   0.99
Is_begin  1.218  0.878   0.047    3.048  ...    51.0      37.0      73.0   1.03
Ia_begin  3.397  2.884   0.015    9.133  ...    61.0      67.0      93.0   1.00
E_begin   2.366  2.431   0.000    7.663  ...    30.0      14.0      37.0   1.11

[8 rows x 11 columns]