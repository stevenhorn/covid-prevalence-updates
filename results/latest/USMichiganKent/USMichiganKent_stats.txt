0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1659.84    29.05
p_loo       25.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      262   88.8%
 (0.5, 0.7]   (ok)         30   10.2%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.051   0.165    0.338  ...    79.0      78.0      60.0   1.01
pu        0.832  0.042   0.751    0.895  ...    58.0      59.0      84.0   1.04
mu        0.120  0.020   0.080    0.155  ...    12.0      11.0      32.0   1.17
mus       0.162  0.025   0.118    0.204  ...    17.0      14.0      58.0   1.11
gamma     0.222  0.036   0.161    0.302  ...    90.0      85.0      54.0   1.01
Is_begin  3.056  2.467   0.029    8.282  ...   152.0     152.0      60.0   1.00
Ia_begin  5.707  4.286   0.637   14.992  ...    28.0      25.0      54.0   1.06
E_begin   4.924  4.525   0.138   13.628  ...    48.0      60.0      38.0   1.02

[8 rows x 11 columns]