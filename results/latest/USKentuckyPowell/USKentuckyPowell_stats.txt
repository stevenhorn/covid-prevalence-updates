0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -608.36    30.91
p_loo       30.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.056   0.152    0.332  ...    85.0      83.0      60.0   1.02
pu        0.841  0.030   0.773    0.888  ...    51.0      59.0      40.0   1.02
mu        0.146  0.026   0.099    0.192  ...     9.0       9.0      49.0   1.18
mus       0.210  0.037   0.152    0.270  ...    88.0     125.0      51.0   0.99
gamma     0.250  0.049   0.172    0.332  ...   152.0     152.0      93.0   1.00
Is_begin  0.827  0.847   0.011    2.580  ...   143.0      87.0      76.0   1.01
Ia_begin  1.157  1.281   0.038    3.080  ...    99.0      89.0     100.0   0.98
E_begin   0.744  1.178   0.008    2.148  ...    96.0     101.0      33.0   1.04

[8 rows x 11 columns]