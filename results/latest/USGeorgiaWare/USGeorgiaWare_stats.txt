0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1155.11    64.13
p_loo       43.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.224  0.045   0.156    0.308  ...    61.0      62.0      58.0   1.04
pu        0.725  0.019   0.700    0.761  ...    91.0      79.0      60.0   0.98
mu        0.150  0.018   0.123    0.186  ...    34.0      31.0      43.0   1.04
mus       0.277  0.041   0.208    0.329  ...   152.0     152.0      93.0   1.00
gamma     0.427  0.064   0.311    0.543  ...   119.0     126.0      72.0   1.00
Is_begin  0.797  0.754   0.061    2.158  ...   102.0      79.0      60.0   1.00
Ia_begin  1.425  1.447   0.046    4.080  ...    29.0      51.0      22.0   1.06
E_begin   0.627  0.569   0.007    1.902  ...    31.0      23.0      40.0   1.08

[8 rows x 11 columns]