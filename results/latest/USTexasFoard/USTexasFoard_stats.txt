0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -348.15    51.40
p_loo       48.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.063   0.158    0.343  ...    62.0      61.0      40.0   0.98
pu        0.816  0.040   0.735    0.863  ...     3.0       4.0      24.0   1.60
mu        0.131  0.028   0.089    0.196  ...    68.0      70.0      53.0   1.02
mus       0.180  0.035   0.127    0.250  ...    35.0      40.0      76.0   1.05
gamma     0.223  0.052   0.155    0.323  ...    27.0      17.0      23.0   1.12
Is_begin  0.519  0.494   0.011    1.472  ...    79.0      67.0      95.0   1.00
Ia_begin  0.926  1.399   0.006    3.087  ...    42.0      61.0      60.0   1.04
E_begin   0.368  0.399   0.004    1.293  ...    71.0      34.0      72.0   1.10

[8 rows x 11 columns]