0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -622.24    26.99
p_loo       26.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.061   0.163    0.350  ...   152.0     117.0      58.0   1.03
pu        0.837  0.019   0.798    0.871  ...    96.0      89.0      60.0   1.05
mu        0.124  0.024   0.078    0.169  ...    33.0      43.0      39.0   1.01
mus       0.153  0.028   0.114    0.206  ...   152.0     152.0      93.0   1.06
gamma     0.166  0.033   0.107    0.225  ...   147.0     139.0      99.0   1.00
Is_begin  0.858  0.752   0.019    2.440  ...   125.0     152.0      39.0   0.98
Ia_begin  1.685  1.662   0.035    4.929  ...   104.0      70.0      38.0   0.99
E_begin   0.815  0.822   0.003    2.566  ...   117.0      77.0      91.0   1.00

[8 rows x 11 columns]