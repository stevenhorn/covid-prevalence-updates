0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -826.30    28.61
p_loo       26.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.052   0.168    0.346  ...    49.0      42.0      22.0   1.05
pu        0.860  0.025   0.809    0.898  ...     8.0       8.0      34.0   1.21
mu        0.116  0.019   0.079    0.148  ...    19.0      19.0      51.0   1.12
mus       0.158  0.032   0.097    0.211  ...    91.0      78.0      59.0   1.11
gamma     0.175  0.028   0.124    0.221  ...    44.0      44.0      97.0   1.04
Is_begin  0.872  0.796   0.041    2.455  ...   115.0     152.0      52.0   0.98
Ia_begin  1.597  1.351   0.028    4.308  ...    49.0      26.0      60.0   1.09
E_begin   0.965  0.977   0.004    2.959  ...    58.0      36.0      59.0   1.04

[8 rows x 11 columns]