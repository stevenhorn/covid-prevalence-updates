0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo  -964.67    27.61
p_loo       26.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      259   87.5%
 (0.5, 0.7]   (ok)         28    9.5%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    1    0.3%

             mean       sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.259    0.059   0.150    0.343  ...    25.0      29.0      22.0   1.06
pu          0.798    0.055   0.701    0.872  ...     5.0       6.0      17.0   1.36
mu          0.223    0.022   0.182    0.262  ...    16.0      16.0      60.0   1.10
mus         0.281    0.040   0.203    0.345  ...    17.0      15.0      30.0   1.13
gamma       0.429    0.063   0.321    0.532  ...   152.0     152.0      91.0   1.00
Is_begin   80.608   53.066   1.257  169.497  ...    63.0      45.0      17.0   1.06
Ia_begin  255.580  107.137  72.256  472.139  ...    36.0      30.0      81.0   1.08
E_begin   305.527  180.696  19.862  594.179  ...    38.0      41.0      59.0   1.11

[8 rows x 11 columns]