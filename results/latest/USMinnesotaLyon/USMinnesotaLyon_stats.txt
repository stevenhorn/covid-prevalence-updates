0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -875.32    28.85
p_loo       25.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.059   0.157    0.350  ...    17.0      16.0      14.0   1.09
pu        0.782  0.024   0.740    0.821  ...    26.0      31.0      55.0   1.06
mu        0.117  0.021   0.075    0.153  ...    10.0      11.0      38.0   1.16
mus       0.172  0.030   0.123    0.225  ...    98.0      93.0      88.0   1.00
gamma     0.222  0.043   0.144    0.295  ...    75.0      74.0      93.0   1.02
Is_begin  0.542  0.535   0.002    1.707  ...    13.0       8.0      17.0   1.25
Ia_begin  1.085  1.333   0.003    3.516  ...   100.0      73.0      60.0   1.04
E_begin   0.494  0.567   0.002    1.532  ...    42.0      19.0      61.0   1.10

[8 rows x 11 columns]