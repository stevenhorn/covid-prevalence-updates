0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1041.32    37.92
p_loo       26.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.048   0.174    0.335  ...    76.0      76.0      59.0   1.01
pu        0.764  0.036   0.702    0.816  ...     9.0       9.0      39.0   1.19
mu        0.117  0.024   0.080    0.169  ...    13.0      13.0      40.0   1.15
mus       0.182  0.032   0.130    0.248  ...    98.0     105.0      60.0   1.00
gamma     0.244  0.044   0.186    0.324  ...   152.0     142.0     100.0   1.06
Is_begin  0.894  0.753   0.037    2.681  ...    66.0      62.0      59.0   1.03
Ia_begin  0.766  0.479   0.025    1.608  ...    90.0      81.0      59.0   1.01
E_begin   0.684  0.784   0.009    1.799  ...    74.0      61.0      57.0   1.06

[8 rows x 11 columns]