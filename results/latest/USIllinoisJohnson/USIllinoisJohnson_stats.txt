0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -664.06    39.34
p_loo       30.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      281   95.3%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.055   0.163    0.341  ...    43.0      41.0      40.0   1.02
pu        0.767  0.035   0.708    0.817  ...    24.0      27.0      43.0   1.05
mu        0.119  0.023   0.084    0.163  ...    16.0      17.0      60.0   1.09
mus       0.170  0.031   0.106    0.220  ...    91.0      94.0      59.0   1.00
gamma     0.200  0.050   0.118    0.292  ...    56.0     130.0      21.0   1.12
Is_begin  0.449  0.429   0.011    1.160  ...    86.0      71.0     100.0   1.02
Ia_begin  0.871  0.944   0.014    2.632  ...    47.0      54.0      57.0   1.03
E_begin   0.429  0.577   0.016    1.806  ...    70.0      55.0      72.0   1.03

[8 rows x 11 columns]