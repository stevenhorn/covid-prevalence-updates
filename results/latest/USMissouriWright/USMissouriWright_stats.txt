0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -704.67    28.68
p_loo       21.05        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      281   95.3%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.057   0.169    0.346  ...    72.0      73.0      43.0   1.07
pu        0.825  0.030   0.764    0.876  ...    33.0      40.0      40.0   1.11
mu        0.136  0.027   0.098    0.187  ...    16.0      18.0      39.0   1.06
mus       0.173  0.028   0.120    0.226  ...    73.0      71.0      91.0   0.99
gamma     0.216  0.039   0.158    0.304  ...   102.0     100.0      20.0   1.04
Is_begin  0.795  0.710   0.013    2.223  ...    65.0      96.0      60.0   1.03
Ia_begin  1.731  1.673   0.035    5.257  ...    89.0      77.0      88.0   1.00
E_begin   0.896  1.014   0.001    3.402  ...    96.0      83.0      79.0   1.00

[8 rows x 11 columns]