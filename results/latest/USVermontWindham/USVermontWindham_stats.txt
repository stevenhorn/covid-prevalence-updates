0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -533.70    30.72
p_loo       30.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.048   0.163    0.323  ...   152.0     152.0      72.0   1.00
pu        0.856  0.032   0.801    0.900  ...    37.0      30.0      61.0   1.06
mu        0.148  0.027   0.107    0.203  ...    44.0      47.0      21.0   1.02
mus       0.199  0.028   0.144    0.245  ...   152.0     152.0      49.0   0.99
gamma     0.270  0.042   0.191    0.355  ...   151.0     144.0      72.0   0.98
Is_begin  1.642  1.217   0.029    4.064  ...    72.0      70.0      39.0   1.02
Ia_begin  5.423  3.266   0.261   10.209  ...    75.0      78.0      95.0   1.04
E_begin   3.885  3.504   0.204   11.597  ...    78.0      24.0      19.0   1.09

[8 rows x 11 columns]