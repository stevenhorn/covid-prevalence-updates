0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -657.00    32.18
p_loo       24.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.272  0.052   0.170    0.350  ...   152.0     152.0      25.0   1.07
pu        0.878  0.025   0.828    0.900  ...   152.0     152.0      36.0   1.08
mu        0.122  0.020   0.086    0.158  ...    52.0      55.0      84.0   1.04
mus       0.160  0.031   0.112    0.218  ...   152.0     152.0      47.0   1.00
gamma     0.193  0.037   0.133    0.273  ...   152.0     152.0      61.0   1.03
Is_begin  1.356  1.216   0.021    3.841  ...   152.0     135.0      58.0   0.99
Ia_begin  2.871  2.399   0.268    8.354  ...    59.0      56.0      60.0   0.98
E_begin   1.631  1.365   0.106    4.369  ...    74.0      63.0      79.0   1.04

[8 rows x 11 columns]