0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -585.69    22.39
p_loo       17.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.057   0.151    0.337  ...    56.0      58.0      40.0   1.04
pu        0.821  0.031   0.762    0.864  ...    31.0      31.0      54.0   1.07
mu        0.112  0.021   0.080    0.152  ...    18.0      20.0      96.0   1.08
mus       0.148  0.026   0.112    0.197  ...   152.0     152.0      54.0   1.02
gamma     0.159  0.033   0.102    0.230  ...   110.0     123.0      60.0   0.99
Is_begin  0.378  0.415   0.000    1.238  ...   104.0      73.0      40.0   0.98
Ia_begin  0.726  0.865   0.000    2.463  ...    86.0     115.0      72.0   0.99
E_begin   0.317  0.491   0.003    1.113  ...    95.0      92.0      49.0   0.99

[8 rows x 11 columns]