0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -699.02    32.48
p_loo       24.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.056   0.166    0.343  ...    73.0      78.0      74.0   1.06
pu        0.845  0.018   0.814    0.879  ...    46.0      47.0      23.0   1.00
mu        0.145  0.030   0.087    0.195  ...    24.0      22.0      59.0   1.08
mus       0.152  0.031   0.104    0.210  ...    64.0      58.0      60.0   1.00
gamma     0.173  0.035   0.115    0.245  ...    97.0     121.0      63.0   1.01
Is_begin  0.634  0.616   0.021    1.759  ...    62.0      15.0      93.0   1.13
Ia_begin  1.757  1.821   0.001    5.496  ...    58.0      41.0      40.0   1.03
E_begin   0.759  0.901   0.018    2.439  ...    82.0      51.0      58.0   1.00

[8 rows x 11 columns]