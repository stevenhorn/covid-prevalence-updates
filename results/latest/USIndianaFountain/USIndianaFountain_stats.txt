0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -688.66    26.09
p_loo       23.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.051   0.173    0.344  ...    57.0      57.0      60.0   1.01
pu        0.811  0.018   0.777    0.843  ...    43.0      43.0      56.0   1.05
mu        0.129  0.024   0.092    0.176  ...    26.0      25.0      93.0   1.07
mus       0.153  0.031   0.097    0.201  ...    95.0     131.0      70.0   1.01
gamma     0.165  0.036   0.107    0.235  ...    72.0      61.0      21.0   1.03
Is_begin  0.846  0.711   0.007    2.083  ...    79.0      66.0      56.0   1.01
Ia_begin  1.570  1.593   0.010    4.681  ...    58.0      24.0      59.0   1.03
E_begin   0.707  0.760   0.003    2.233  ...    59.0      40.0      72.0   1.04

[8 rows x 11 columns]