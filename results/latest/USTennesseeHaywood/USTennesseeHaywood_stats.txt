0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -892.05    24.19
p_loo       19.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.055   0.170    0.347  ...    71.0      73.0      69.0   1.02
pu        0.744  0.023   0.704    0.780  ...    21.0      19.0      60.0   1.13
mu        0.130  0.023   0.096    0.174  ...    16.0      18.0      53.0   1.11
mus       0.162  0.029   0.113    0.218  ...    53.0      51.0     100.0   1.03
gamma     0.188  0.039   0.111    0.255  ...   152.0     152.0      60.0   1.04
Is_begin  0.772  0.668   0.002    2.032  ...   104.0      98.0      61.0   0.99
Ia_begin  1.624  1.380   0.014    3.955  ...    46.0      57.0      59.0   1.02
E_begin   0.694  0.620   0.027    1.738  ...    75.0      80.0      93.0   1.03

[8 rows x 11 columns]