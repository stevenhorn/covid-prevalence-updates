0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1086.53    28.03
p_loo       17.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.053   0.165    0.331  ...     4.0       5.0      15.0   1.48
pu        0.845  0.015   0.821    0.871  ...     8.0       7.0      21.0   1.22
mu        0.225  0.013   0.207    0.245  ...     5.0       6.0      32.0   1.33
mus       0.170  0.028   0.127    0.221  ...    28.0      28.0      54.0   1.05
gamma     0.116  0.017   0.087    0.141  ...     5.0       6.0      25.0   1.34
Is_begin  1.675  1.136   0.300    4.022  ...     9.0      11.0      59.0   1.19
Ia_begin  2.801  1.913   0.292    5.775  ...    20.0      17.0      25.0   1.10
E_begin   2.106  1.849   0.293    5.544  ...    14.0      17.0      55.0   1.17

[8 rows x 11 columns]