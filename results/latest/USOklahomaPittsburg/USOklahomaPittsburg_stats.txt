0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -966.31    49.99
p_loo       34.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.052   0.175    0.348  ...    69.0      68.0      57.0   1.00
pu        0.818  0.023   0.778    0.859  ...    52.0      53.0      60.0   0.99
mu        0.136  0.022   0.100    0.174  ...    39.0      44.0      60.0   0.99
mus       0.187  0.037   0.117    0.259  ...   107.0     104.0      54.0   1.01
gamma     0.234  0.050   0.150    0.320  ...   112.0     133.0      53.0   1.10
Is_begin  0.743  0.780   0.004    2.183  ...   129.0      56.0      59.0   1.02
Ia_begin  1.645  1.297   0.054    3.850  ...    89.0      43.0      91.0   1.04
E_begin   0.712  0.672   0.004    2.294  ...    68.0      65.0      37.0   1.02

[8 rows x 11 columns]