0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -932.43    29.59
p_loo       22.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.052   0.161    0.343  ...    66.0      60.0      38.0   1.08
pu        0.843  0.021   0.797    0.874  ...    37.0      38.0      35.0   1.01
mu        0.110  0.022   0.079    0.156  ...    14.0      20.0      15.0   1.18
mus       0.148  0.027   0.107    0.201  ...    74.0      89.0      56.0   0.99
gamma     0.174  0.044   0.118    0.242  ...   139.0     152.0      48.0   1.03
Is_begin  0.985  0.797   0.006    2.766  ...    70.0      70.0      60.0   1.03
Ia_begin  2.163  1.802   0.017    4.814  ...    67.0      49.0      38.0   1.02
E_begin   1.384  1.403   0.037    4.028  ...    94.0      59.0      65.0   1.03

[8 rows x 11 columns]