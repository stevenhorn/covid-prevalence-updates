0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -627.35    35.65
p_loo       31.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.051   0.153    0.328  ...   152.0     119.0      37.0   1.05
pu        0.778  0.039   0.709    0.836  ...    38.0      38.0      20.0   1.21
mu        0.113  0.019   0.080    0.148  ...    88.0      90.0      57.0   1.00
mus       0.158  0.029   0.114    0.218  ...   152.0     152.0      65.0   1.00
gamma     0.187  0.033   0.132    0.241  ...    24.0      25.0      74.0   1.07
Is_begin  0.766  0.726   0.005    2.321  ...    87.0     105.0      60.0   1.00
Ia_begin  1.326  1.744   0.000    5.308  ...    78.0      48.0      58.0   1.02
E_begin   0.817  0.957   0.012    2.620  ...   120.0     152.0     100.0   1.02

[8 rows x 11 columns]