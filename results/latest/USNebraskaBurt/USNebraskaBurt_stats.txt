0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -476.68    29.26
p_loo       17.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      283   95.9%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.053   0.179    0.346  ...    61.0      60.0      56.0   1.04
pu        0.854  0.013   0.833    0.877  ...    78.0      81.0      96.0   0.99
mu        0.145  0.025   0.097    0.185  ...    25.0      26.0      35.0   1.02
mus       0.171  0.033   0.123    0.245  ...    40.0      67.0      35.0   1.06
gamma     0.205  0.037   0.146    0.274  ...    21.0      18.0      18.0   1.08
Is_begin  0.596  0.605   0.001    1.757  ...    57.0      33.0      15.0   1.02
Ia_begin  1.268  1.205   0.013    3.693  ...   133.0     148.0      69.0   1.01
E_begin   0.617  0.512   0.024    1.514  ...    94.0      84.0      63.0   0.99

[8 rows x 11 columns]