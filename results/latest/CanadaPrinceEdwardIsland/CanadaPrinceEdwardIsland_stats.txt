0 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo  -391.50    35.76
p_loo       40.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      291   94.2%
 (0.5, 0.7]   (ok)         14    4.5%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.229  0.051   0.151    0.317  ...    77.0      71.0      38.0   1.04
pu        0.738  0.034   0.700    0.795  ...    71.0      55.0      22.0   1.01
mu        0.151  0.031   0.096    0.207  ...    97.0      97.0      49.0   1.02
mus       0.250  0.038   0.193    0.324  ...    56.0      53.0      79.0   1.02
gamma     0.284  0.045   0.212    0.381  ...   152.0     152.0      86.0   0.98
Is_begin  1.528  1.096   0.251    3.570  ...    78.0      43.0      54.0   1.04
Ia_begin  0.719  0.718   0.003    2.221  ...    70.0      48.0      42.0   1.04
E_begin   0.983  1.022   0.004    3.303  ...    69.0      25.0      54.0   1.07

[8 rows x 11 columns]