0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -993.66    33.32
p_loo       28.34        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      281   95.3%
 (0.5, 0.7]   (ok)          7    2.4%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.036   0.181    0.312  ...    11.0      12.0      14.0   1.15
pu        0.720  0.016   0.701    0.747  ...     5.0       5.0      22.0   1.50
mu        0.168  0.015   0.139    0.194  ...     4.0       4.0      16.0   1.68
mus       0.222  0.042   0.135    0.305  ...    10.0       7.0      42.0   1.25
gamma     0.163  0.023   0.129    0.205  ...     4.0       4.0      20.0   1.65
Is_begin  0.992  0.611   0.098    2.030  ...     5.0       5.0      38.0   1.40
Ia_begin  1.588  0.868   0.550    3.330  ...     5.0       5.0      14.0   1.46
E_begin   1.183  1.097   0.094    3.655  ...     6.0       6.0      24.0   1.38

[8 rows x 11 columns]