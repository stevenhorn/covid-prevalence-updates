12 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1483.81    26.89
p_loo       29.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.272   0.053   0.169    0.343  ...     8.0      11.0      60.0   1.28
pu         0.746   0.036   0.707    0.813  ...    19.0      17.0      39.0   1.08
mu         0.140   0.017   0.101    0.174  ...    38.0      34.0      38.0   1.45
mus        0.208   0.030   0.149    0.252  ...    10.0      11.0      20.0   1.14
gamma      0.317   0.052   0.221    0.409  ...   102.0     145.0      84.0   1.05
Is_begin  10.425   4.933   1.114   18.831  ...    63.0      51.0      34.0   1.13
Ia_begin  25.113  12.981   2.330   41.900  ...    24.0      21.0      14.0   1.04
E_begin   35.142  19.201   3.853   71.970  ...    24.0      22.0      39.0   1.06

[8 rows x 11 columns]