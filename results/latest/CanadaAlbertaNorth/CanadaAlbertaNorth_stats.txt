0 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo -1290.53    33.93
p_loo       25.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      283   91.6%
 (0.5, 0.7]   (ok)         21    6.8%
   (0.7, 1]   (bad)         4    1.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.285  0.044   0.208    0.348  ...    63.0      69.0      54.0   1.01
pu        0.880  0.022   0.841    0.900  ...    31.0      45.0      88.0   1.07
mu        0.143  0.026   0.089    0.184  ...     7.0       7.0      59.0   1.27
mus       0.169  0.038   0.112    0.238  ...   107.0      82.0      59.0   1.00
gamma     0.189  0.037   0.117    0.246  ...   115.0     114.0      40.0   1.07
Is_begin  2.027  1.761   0.037    5.492  ...   100.0      64.0      93.0   1.00
Ia_begin  3.194  2.164   0.340    6.698  ...    88.0      48.0      88.0   1.01
E_begin   2.209  2.331   0.037    6.841  ...    64.0      31.0      28.0   1.05

[8 rows x 11 columns]