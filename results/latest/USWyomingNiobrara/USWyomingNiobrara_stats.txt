2 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -252.66    27.56
p_loo       23.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.306  0.025   0.269    0.347  ...    58.0      47.0      44.0   1.01
pu        0.895  0.004   0.888    0.900  ...    41.0      30.0      24.0   1.01
mu        0.112  0.019   0.080    0.154  ...    26.0      30.0      60.0   1.07
mus       0.180  0.028   0.129    0.233  ...     8.0      10.0      43.0   1.21
gamma     0.216  0.033   0.163    0.269  ...    11.0      11.0      31.0   1.14
Is_begin  0.791  0.743   0.010    2.003  ...    44.0      24.0      60.0   1.07
Ia_begin  1.495  1.275   0.037    4.159  ...    13.0      14.0      59.0   1.13
E_begin   0.427  0.502   0.005    1.437  ...    32.0      31.0      38.0   1.04

[8 rows x 11 columns]