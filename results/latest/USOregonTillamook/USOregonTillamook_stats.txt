0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -424.61    23.35
p_loo       17.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.269  0.053   0.169    0.348  ...   137.0     131.0      32.0   1.00
pu        0.877  0.021   0.845    0.900  ...    29.0      27.0      30.0   1.06
mu        0.140  0.025   0.104    0.191  ...    39.0      40.0      93.0   1.03
mus       0.166  0.033   0.111    0.217  ...   152.0     152.0      52.0   1.03
gamma     0.186  0.032   0.125    0.241  ...    88.0     104.0      56.0   1.01
Is_begin  0.913  0.772   0.029    2.696  ...    99.0      66.0      61.0   1.00
Ia_begin  2.115  1.832   0.119    5.489  ...   113.0      92.0      68.0   1.04
E_begin   0.981  0.910   0.006    2.601  ...    83.0      69.0      60.0   1.02

[8 rows x 11 columns]