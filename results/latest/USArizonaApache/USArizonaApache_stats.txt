0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1203.91    19.75
p_loo       18.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.229  0.049   0.151    0.305  ...    66.0      72.0      46.0   1.04
pu        0.734  0.025   0.700    0.775  ...   113.0      89.0      37.0   1.01
mu        0.106  0.021   0.078    0.156  ...    25.0      23.0      48.0   1.08
mus       0.158  0.032   0.111    0.210  ...    94.0      85.0      69.0   1.04
gamma     0.197  0.030   0.150    0.258  ...    69.0      68.0      52.0   1.00
Is_begin  1.162  0.975   0.090    2.445  ...    99.0     104.0      91.0   0.98
Ia_begin  0.782  0.652   0.038    1.929  ...   136.0     152.0      93.0   1.00
E_begin   0.998  0.978   0.000    2.547  ...    99.0      72.0      60.0   1.00

[8 rows x 11 columns]