0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -993.38    32.14
p_loo       27.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.056   0.150    0.335  ...    13.0      15.0      22.0   1.16
pu        0.877  0.023   0.840    0.900  ...    70.0      64.0      38.0   1.00
mu        0.124  0.017   0.098    0.162  ...    12.0      13.0      21.0   1.13
mus       0.160  0.031   0.121    0.215  ...   133.0     143.0      81.0   0.99
gamma     0.189  0.041   0.136    0.283  ...   152.0     152.0     100.0   1.01
Is_begin  0.947  0.876   0.005    2.527  ...    72.0      55.0      30.0   1.06
Ia_begin  0.668  0.535   0.099    1.635  ...    94.0      75.0      92.0   1.01
E_begin   0.760  0.799   0.007    2.587  ...    96.0      61.0      60.0   0.99

[8 rows x 11 columns]