0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -918.54    25.75
p_loo       19.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.054   0.161    0.342  ...    36.0      39.0      39.0   1.03
pu        0.775  0.036   0.710    0.826  ...    21.0      25.0      22.0   1.04
mu        0.123  0.021   0.083    0.159  ...    45.0      37.0      80.0   1.04
mus       0.157  0.032   0.099    0.216  ...    71.0      67.0      43.0   1.01
gamma     0.188  0.038   0.131    0.255  ...   152.0     152.0      93.0   1.04
Is_begin  0.814  0.759   0.036    2.232  ...    98.0      92.0      83.0   0.99
Ia_begin  1.893  1.931   0.071    6.119  ...    55.0     101.0      56.0   1.04
E_begin   0.786  0.844   0.003    2.826  ...    96.0      74.0      57.0   1.00

[8 rows x 11 columns]