0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1661.42    39.80
p_loo       32.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      265   89.8%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)        10    3.4%
   (1, Inf)   (very bad)    0    0.0%

             mean       sd   hdi_3%  ...  ess_bulk  ess_tail  r_hat
pa          0.232    0.056    0.151  ...     152.0      46.0   0.99
pu          0.745    0.037    0.701  ...      89.0      59.0   1.01
mu          0.103    0.016    0.078  ...      30.0      60.0   1.02
mus         0.193    0.031    0.148  ...     152.0      83.0   0.98
gamma       0.319    0.044    0.240  ...     111.0      60.0   1.05
Is_begin  190.262  126.577    9.251  ...     111.0      51.0   1.03
Ia_begin  625.077  211.687  224.760  ...     152.0      66.0   1.05
E_begin   820.537  515.103   35.393  ...      78.0      59.0   1.02

[8 rows x 11 columns]