0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -628.99    66.35
p_loo       41.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.224  0.047   0.157    0.312  ...    22.0      41.0      60.0   1.05
pu        0.730  0.017   0.702    0.762  ...    67.0      64.0      60.0   0.99
mu        0.104  0.017   0.074    0.134  ...    48.0      51.0      61.0   1.02
mus       0.213  0.032   0.152    0.264  ...    83.0      86.0      93.0   1.00
gamma     0.351  0.066   0.254    0.470  ...    91.0      93.0      59.0   1.02
Is_begin  0.740  0.710   0.019    2.297  ...    46.0      44.0      59.0   1.01
Ia_begin  0.819  0.836   0.000    2.599  ...   105.0      66.0      60.0   1.00
E_begin   0.360  0.420   0.001    1.302  ...    36.0      39.0      54.0   1.01

[8 rows x 11 columns]