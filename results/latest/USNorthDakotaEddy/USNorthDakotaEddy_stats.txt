0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -604.24    59.22
p_loo       35.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      287   97.3%
 (0.5, 0.7]   (ok)          6    2.0%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.178  0.029   0.150    0.228  ...   148.0     147.0      95.0   0.99
pu        0.707  0.007   0.700    0.719  ...    60.0      37.0      45.0   1.01
mu        0.125  0.024   0.086    0.167  ...    66.0      71.0      51.0   1.01
mus       0.196  0.040   0.136    0.269  ...   116.0     147.0      54.0   1.04
gamma     0.206  0.041   0.150    0.298  ...   136.0     115.0      60.0   0.99
Is_begin  0.614  0.548   0.003    1.586  ...    93.0      91.0      60.0   0.98
Ia_begin  0.951  1.030   0.030    3.608  ...    48.0      48.0      24.0   1.03
E_begin   0.398  0.385   0.014    1.014  ...   105.0      76.0      59.0   1.02

[8 rows x 11 columns]