0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1151.21    26.91
p_loo       22.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.277  0.053   0.183    0.348  ...    93.0      73.0      43.0   1.02
pu        0.882  0.018   0.852    0.900  ...    28.0      37.0      29.0   1.02
mu        0.121  0.018   0.089    0.153  ...    11.0      11.0      59.0   1.19
mus       0.157  0.033   0.103    0.215  ...   118.0     128.0      76.0   1.05
gamma     0.188  0.035   0.130    0.261  ...   152.0     152.0      61.0   1.03
Is_begin  0.999  0.844   0.019    2.378  ...   116.0     138.0     100.0   1.11
Ia_begin  1.698  1.304   0.011    4.102  ...    40.0      37.0      43.0   1.07
E_begin   0.928  0.885   0.034    2.727  ...    34.0      22.0     100.0   1.06

[8 rows x 11 columns]