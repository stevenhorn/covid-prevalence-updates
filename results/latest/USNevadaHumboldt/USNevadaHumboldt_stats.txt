1 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -835.08    43.88
p_loo       31.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      289   98.0%
 (0.5, 0.7]   (ok)          1    0.3%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.052   0.197    0.347  ...    18.0      15.0      16.0   1.13
pu        0.853  0.018   0.820    0.883  ...    36.0      37.0      59.0   1.08
mu        0.117  0.016   0.090    0.144  ...    58.0      74.0      60.0   1.02
mus       0.146  0.020   0.119    0.197  ...    68.0      68.0      55.0   1.00
gamma     0.231  0.039   0.169    0.300  ...    54.0      56.0      60.0   1.06
Is_begin  1.071  0.852   0.050    2.705  ...    62.0      75.0      54.0   1.01
Ia_begin  2.528  1.773   0.194    5.491  ...   118.0     100.0      93.0   1.00
E_begin   1.224  1.317   0.002    4.688  ...   102.0      71.0      30.0   1.01

[8 rows x 11 columns]