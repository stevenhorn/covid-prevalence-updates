0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1104.48    36.48
p_loo       29.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.054   0.156    0.339  ...   103.0      83.0      40.0   0.98
pu        0.833  0.019   0.799    0.868  ...    68.0      64.0      31.0   0.99
mu        0.144  0.030   0.085    0.186  ...     8.0       8.0      49.0   1.21
mus       0.160  0.032   0.104    0.219  ...   152.0     152.0     100.0   1.03
gamma     0.181  0.036   0.115    0.242  ...   110.0     108.0      48.0   1.10
Is_begin  0.794  0.843   0.009    2.357  ...    46.0      81.0      60.0   1.04
Ia_begin  1.753  1.905   0.038    5.570  ...    68.0      77.0      61.0   1.07
E_begin   0.718  0.863   0.032    2.459  ...    63.0      60.0      57.0   0.99

[8 rows x 11 columns]