3 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo   115.19    77.19
p_loo       96.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)          7    2.4%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    4    1.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.274  0.052   0.180    0.348  ...    10.0      11.0      56.0   1.17
pu        0.837  0.062   0.729    0.899  ...     3.0       3.0      14.0   2.27
mu        0.187  0.064   0.096    0.274  ...     3.0       3.0      25.0   1.86
mus       0.321  0.040   0.246    0.386  ...    47.0      43.0      60.0   1.01
gamma     0.334  0.050   0.240    0.420  ...     6.0       6.0      59.0   1.33
Is_begin  0.204  0.236   0.008    0.570  ...    26.0      34.0      57.0   1.09
Ia_begin  1.663  1.403   0.018    3.795  ...     3.0       4.0      15.0   1.82
E_begin   1.140  1.064   0.051    2.959  ...     3.0       4.0      48.0   1.82

[8 rows x 11 columns]