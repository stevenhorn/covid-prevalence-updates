0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -729.53    28.70
p_loo       25.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      263   89.2%
 (0.5, 0.7]   (ok)         27    9.2%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.057   0.164    0.347  ...    96.0      97.0      93.0   1.04
pu        0.883  0.018   0.846    0.900  ...    53.0      63.0      40.0   1.06
mu        0.124  0.026   0.082    0.168  ...    52.0      51.0      60.0   1.03
mus       0.152  0.027   0.103    0.204  ...   152.0     152.0      93.0   1.00
gamma     0.178  0.041   0.120    0.266  ...   107.0     152.0      95.0   0.99
Is_begin  0.912  0.991   0.000    2.666  ...    97.0     109.0      96.0   0.98
Ia_begin  1.735  1.708   0.011    4.378  ...   103.0     108.0      96.0   1.01
E_begin   0.891  1.279   0.001    2.575  ...    96.0      81.0      43.0   1.00

[8 rows x 11 columns]