0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -990.96    47.45
p_loo       28.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.277  0.043   0.210    0.347  ...   100.0     101.0      61.0   1.04
pu        0.884  0.014   0.855    0.900  ...    67.0      50.0      38.0   1.01
mu        0.125  0.022   0.088    0.158  ...     7.0       8.0      61.0   1.22
mus       0.163  0.040   0.109    0.242  ...    83.0      82.0      59.0   1.01
gamma     0.206  0.041   0.136    0.278  ...    64.0      47.0      51.0   1.04
Is_begin  0.929  0.854   0.011    2.987  ...    79.0      59.0      60.0   1.03
Ia_begin  2.439  2.135   0.010    6.928  ...    58.0      41.0      60.0   1.05
E_begin   1.119  1.078   0.015    3.511  ...    46.0      31.0      77.0   1.05

[8 rows x 11 columns]