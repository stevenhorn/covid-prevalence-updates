0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -439.61    57.18
p_loo       38.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.055   0.172    0.349  ...   102.0      99.0      60.0   1.01
pu        0.774  0.026   0.731    0.820  ...    49.0      50.0      52.0   0.99
mu        0.129  0.026   0.091    0.198  ...    32.0      33.0      59.0   1.05
mus       0.186  0.039   0.126    0.269  ...    95.0     142.0      59.0   0.99
gamma     0.209  0.043   0.130    0.270  ...    64.0      92.0      40.0   0.99
Is_begin  0.715  0.775   0.000    2.092  ...    96.0      77.0      75.0   1.01
Ia_begin  1.337  1.412   0.010    3.978  ...    89.0      52.0      56.0   1.01
E_begin   0.566  0.675   0.000    2.050  ...    82.0      23.0      34.0   1.07

[8 rows x 11 columns]