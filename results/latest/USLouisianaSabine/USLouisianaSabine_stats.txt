0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -929.19    29.77
p_loo       24.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.052   0.164    0.325  ...    61.0      55.0      32.0   1.12
pu        0.758  0.024   0.709    0.796  ...    35.0      38.0      40.0   1.07
mu        0.142  0.021   0.109    0.180  ...     9.0       9.0      40.0   1.18
mus       0.175  0.033   0.118    0.234  ...    49.0      48.0      43.0   1.01
gamma     0.216  0.037   0.154    0.274  ...    17.0      15.0      40.0   1.12
Is_begin  0.722  0.788   0.009    2.327  ...    93.0      86.0      59.0   1.00
Ia_begin  1.365  1.127   0.032    3.140  ...    54.0      51.0      54.0   1.03
E_begin   0.539  0.589   0.004    1.722  ...    62.0      45.0      34.0   1.04

[8 rows x 11 columns]