0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -349.77    24.26
p_loo       22.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.290  0.045   0.189    0.350  ...    79.0      68.0      59.0   0.98
pu        0.889  0.011   0.863    0.899  ...    35.0      30.0      59.0   1.06
mu        0.153  0.026   0.105    0.191  ...    14.0      18.0     100.0   1.09
mus       0.218  0.038   0.161    0.287  ...    57.0      58.0      87.0   1.03
gamma     0.277  0.051   0.186    0.367  ...    72.0      70.0      80.0   1.04
Is_begin  0.835  0.982   0.000    2.543  ...    21.0       7.0      16.0   1.26
Ia_begin  1.752  1.634   0.018    5.553  ...    46.0      31.0      59.0   1.07
E_begin   0.850  0.955   0.003    2.803  ...    36.0      17.0      59.0   1.10

[8 rows x 11 columns]