0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -2099.89    22.52
p_loo       16.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.054   0.152    0.322  ...   115.0      99.0      60.0   1.01
pu        0.777  0.043   0.703    0.833  ...    41.0      43.0      38.0   1.00
mu        0.108  0.019   0.076    0.138  ...     8.0      11.0      22.0   1.20
mus       0.159  0.026   0.116    0.201  ...    99.0     104.0      60.0   1.00
gamma     0.236  0.040   0.167    0.306  ...    94.0      96.0      93.0   1.04
Is_begin  3.759  2.795   0.050    9.412  ...    77.0      45.0      30.0   1.03
Ia_begin  9.246  5.097   0.469   16.985  ...   105.0      97.0      85.0   0.99
E_begin   9.568  8.070   0.188   25.430  ...    49.0      31.0      81.0   1.06

[8 rows x 11 columns]