0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -874.74    37.74
p_loo       30.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.048   0.187    0.341  ...    63.0      63.0      48.0   1.03
pu        0.846  0.017   0.817    0.876  ...    28.0      25.0      60.0   1.08
mu        0.124  0.021   0.079    0.155  ...    42.0      44.0      57.0   1.06
mus       0.147  0.033   0.098    0.214  ...    30.0      30.0      60.0   1.05
gamma     0.181  0.034   0.133    0.236  ...    54.0      66.0      95.0   1.08
Is_begin  0.983  0.825   0.001    2.590  ...    81.0      58.0      60.0   1.00
Ia_begin  1.992  1.665   0.020    5.015  ...    89.0      36.0      43.0   1.04
E_begin   0.948  1.018   0.008    2.937  ...    64.0      45.0      60.0   1.03

[8 rows x 11 columns]