0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1083.99    28.63
p_loo       24.25        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.061   0.159    0.347  ...    69.0      60.0      37.0   1.05
pu        0.821  0.021   0.780    0.854  ...    48.0      51.0      69.0   1.03
mu        0.129  0.020   0.095    0.168  ...    36.0      31.0      87.0   1.05
mus       0.159  0.027   0.117    0.205  ...   133.0     152.0      65.0   0.99
gamma     0.170  0.033   0.106    0.230  ...   117.0     132.0      77.0   1.03
Is_begin  0.996  0.763   0.088    2.534  ...   142.0     119.0      40.0   1.02
Ia_begin  0.652  0.611   0.001    1.871  ...    88.0      50.0      40.0   1.00
E_begin   0.558  0.566   0.015    1.563  ...   100.0     152.0      71.0   1.00

[8 rows x 11 columns]