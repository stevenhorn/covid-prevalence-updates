0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo  -874.06    36.86
p_loo       37.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      281   94.9%
 (0.5, 0.7]   (ok)          7    2.4%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.290  0.049   0.189    0.347  ...   152.0     152.0      97.0   0.99
pu        0.881  0.019   0.837    0.899  ...    76.0     135.0      56.0   1.02
mu        0.166  0.027   0.112    0.207  ...     6.0       7.0      24.0   1.39
mus       0.218  0.039   0.151    0.292  ...   101.0     101.0      57.0   1.08
gamma     0.313  0.050   0.196    0.382  ...    79.0      78.0      91.0   1.01
Is_begin  0.629  0.508   0.049    1.364  ...    65.0      63.0      49.0   0.99
Ia_begin  0.929  1.016   0.004    2.836  ...    68.0      25.0      47.0   1.04
E_begin   0.595  0.658   0.003    1.974  ...    63.0      46.0      40.0   0.99

[8 rows x 11 columns]