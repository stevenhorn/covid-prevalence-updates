0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1729.44    30.15
p_loo       18.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.051   0.169    0.345  ...    46.0      36.0      40.0   1.06
pu        0.824  0.024   0.778    0.860  ...    36.0      40.0      59.0   1.03
mu        0.118  0.029   0.068    0.160  ...     6.0       7.0      36.0   1.26
mus       0.155  0.030   0.091    0.199  ...    16.0      19.0      60.0   1.09
gamma     0.189  0.045   0.123    0.288  ...    91.0     103.0      83.0   0.98
Is_begin  0.693  0.638   0.021    2.182  ...    43.0      50.0      37.0   1.02
Ia_begin  2.210  1.623   0.001    5.369  ...    84.0      77.0      57.0   1.01
E_begin   0.969  0.942   0.009    2.856  ...    68.0      47.0      40.0   1.03

[8 rows x 11 columns]