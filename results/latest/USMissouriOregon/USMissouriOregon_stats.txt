0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -612.10    52.28
p_loo       30.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.052   0.173    0.347  ...   152.0     146.0      54.0   1.00
pu        0.864  0.019   0.834    0.899  ...    50.0      33.0      60.0   1.05
mu        0.115  0.020   0.074    0.150  ...    39.0      43.0      24.0   1.03
mus       0.164  0.029   0.106    0.208  ...   142.0     152.0      33.0   1.03
gamma     0.196  0.036   0.141    0.269  ...    99.0      97.0      60.0   1.00
Is_begin  0.642  0.879   0.004    2.787  ...    57.0      62.0      60.0   0.99
Ia_begin  1.223  1.365   0.012    3.809  ...   105.0     137.0      95.0   1.01
E_begin   0.548  0.646   0.000    1.590  ...    48.0      85.0      40.0   1.01

[8 rows x 11 columns]