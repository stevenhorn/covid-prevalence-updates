0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1101.86    25.03
p_loo       19.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.050   0.172    0.340  ...    65.0      67.0      60.0   1.04
pu        0.842  0.043   0.753    0.899  ...    60.0      62.0      58.0   1.00
mu        0.157  0.019   0.118    0.186  ...    19.0      17.0      43.0   1.11
mus       0.174  0.028   0.127    0.218  ...    77.0      82.0      49.0   1.04
gamma     0.236  0.036   0.160    0.296  ...    67.0      77.0      39.0   1.02
Is_begin  0.879  0.642   0.024    2.293  ...    92.0      70.0      22.0   1.02
Ia_begin  1.954  1.251   0.040    4.204  ...    11.0      11.0      42.0   1.16
E_begin   1.335  1.105   0.018    3.713  ...    34.0      32.0      60.0   1.07

[8 rows x 11 columns]