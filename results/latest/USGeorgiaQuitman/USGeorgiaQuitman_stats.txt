0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -389.48    36.99
p_loo       30.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.049   0.185    0.342  ...    75.0      75.0      18.0   1.02
pu        0.884  0.010   0.863    0.897  ...    61.0      60.0      88.0   0.99
mu        0.110  0.020   0.078    0.147  ...    36.0      38.0     100.0   1.06
mus       0.183  0.037   0.124    0.245  ...    63.0      64.0      59.0   1.03
gamma     0.267  0.048   0.190    0.348  ...    83.0      80.0      59.0   1.01
Is_begin  0.966  0.743   0.020    2.500  ...    60.0      52.0      42.0   1.04
Ia_begin  2.134  2.124   0.105    6.937  ...    61.0      90.0      58.0   0.99
E_begin   0.896  0.811   0.023    2.647  ...    43.0      40.0      43.0   1.02

[8 rows x 11 columns]