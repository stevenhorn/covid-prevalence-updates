0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1552.01    40.35
p_loo       28.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.062   0.165    0.350  ...    30.0      31.0      30.0   1.05
pu        0.831  0.043   0.747    0.900  ...    34.0      29.0      31.0   1.05
mu        0.116  0.019   0.079    0.144  ...     6.0       7.0      20.0   1.30
mus       0.171  0.026   0.131    0.223  ...   110.0     101.0      93.0   1.01
gamma     0.207  0.047   0.114    0.275  ...   101.0      92.0      88.0   1.00
Is_begin  1.748  2.020   0.000    5.863  ...    59.0      20.0      22.0   1.07
Ia_begin  5.115  3.415   0.409   12.138  ...   152.0     152.0      57.0   1.02
E_begin   3.596  3.529   0.037   10.628  ...   125.0      85.0      58.0   1.00

[8 rows x 11 columns]