0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -668.88    43.53
p_loo       28.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      281   95.3%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.055   0.157    0.327  ...   104.0      90.0      60.0   1.01
pu        0.840  0.015   0.817    0.868  ...    53.0      51.0      97.0   1.05
mu        0.134  0.020   0.091    0.162  ...    35.0      35.0      45.0   1.04
mus       0.172  0.030   0.114    0.223  ...    64.0      65.0      99.0   1.00
gamma     0.238  0.038   0.182    0.323  ...    86.0     115.0      60.0   0.99
Is_begin  0.664  0.583   0.059    1.726  ...   121.0     152.0      74.0   1.00
Ia_begin  1.291  1.251   0.031    3.566  ...    70.0     104.0      54.0   1.01
E_begin   0.602  0.725   0.007    1.825  ...    67.0      68.0      54.0   1.03

[8 rows x 11 columns]