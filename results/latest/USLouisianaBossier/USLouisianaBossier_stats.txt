0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1310.20    27.63
p_loo       31.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.054   0.165    0.345  ...    63.0      59.0      42.0   1.03
pu        0.762  0.035   0.702    0.819  ...    49.0      49.0      55.0   1.07
mu        0.108  0.019   0.073    0.142  ...     9.0       9.0      40.0   1.20
mus       0.173  0.032   0.125    0.238  ...    71.0      62.0      60.0   1.04
gamma     0.219  0.048   0.154    0.338  ...   117.0     132.0      57.0   1.02
Is_begin  0.862  0.547   0.119    1.753  ...    75.0      84.0      60.0   1.00
Ia_begin  1.593  1.135   0.026    3.781  ...   110.0     109.0      57.0   1.02
E_begin   1.180  1.271   0.046    3.853  ...    74.0      39.0      43.0   1.05

[8 rows x 11 columns]