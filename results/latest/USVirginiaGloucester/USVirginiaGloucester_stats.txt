0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -710.06    28.24
p_loo       28.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.286  0.046   0.194    0.349  ...    63.0      69.0      26.0   1.00
pu        0.888  0.012   0.866    0.900  ...    54.0      60.0      52.0   1.02
mu        0.153  0.026   0.107    0.195  ...    22.0      22.0      14.0   1.00
mus       0.175  0.034   0.124    0.243  ...    31.0      44.0      93.0   1.05
gamma     0.220  0.056   0.126    0.310  ...    65.0      60.0      40.0   1.09
Is_begin  1.178  0.858   0.060    2.757  ...    52.0      50.0      22.0   1.08
Ia_begin  0.663  0.645   0.009    1.666  ...    98.0      65.0      40.0   1.01
E_begin   0.727  0.537   0.034    1.650  ...    83.0      67.0      93.0   1.01

[8 rows x 11 columns]