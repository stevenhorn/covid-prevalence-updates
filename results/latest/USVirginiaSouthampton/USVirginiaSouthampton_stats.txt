2 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -932.85    44.80
p_loo       43.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    4    1.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.213  0.042   0.150    0.287  ...    18.0      26.0      59.0   1.11
pu        0.723  0.019   0.700    0.755  ...    63.0      46.0      40.0   1.03
mu        0.156  0.024   0.122    0.209  ...    12.0      12.0      75.0   1.12
mus       0.223  0.032   0.171    0.282  ...    53.0      61.0      95.0   1.02
gamma     0.361  0.045   0.281    0.444  ...    53.0      53.0      48.0   1.02
Is_begin  0.616  0.679   0.006    1.642  ...    93.0      69.0      15.0   1.01
Ia_begin  1.391  1.334   0.005    3.745  ...    45.0      33.0      43.0   1.07
E_begin   0.760  0.682   0.011    2.035  ...    63.0      54.0      34.0   1.05

[8 rows x 11 columns]