0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -754.02    25.63
p_loo       26.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.042   0.188    0.326  ...    97.0      96.0     100.0   0.99
pu        0.837  0.040   0.762    0.899  ...    11.0      11.0      38.0   1.16
mu        0.108  0.018   0.074    0.134  ...    74.0      74.0      70.0   1.04
mus       0.162  0.028   0.113    0.220  ...   152.0     152.0      96.0   1.04
gamma     0.200  0.044   0.120    0.266  ...    78.0      91.0      57.0   1.02
Is_begin  1.254  0.945   0.003    3.062  ...   122.0      98.0      59.0   1.00
Ia_begin  2.747  2.187   0.064    7.389  ...    95.0      82.0      56.0   1.00
E_begin   1.792  1.883   0.068    5.916  ...    91.0      71.0      91.0   1.00

[8 rows x 11 columns]