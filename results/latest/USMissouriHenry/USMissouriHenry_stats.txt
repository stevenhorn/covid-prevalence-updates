0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -739.90    27.98
p_loo       22.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.053   0.189    0.345  ...    53.0      55.0      40.0   1.05
pu        0.839  0.017   0.811    0.869  ...    33.0      35.0      49.0   1.03
mu        0.141  0.031   0.085    0.191  ...     5.0       5.0      21.0   1.41
mus       0.188  0.034   0.129    0.243  ...    55.0      59.0      59.0   1.03
gamma     0.200  0.043   0.131    0.287  ...   102.0     152.0      62.0   0.99
Is_begin  0.810  0.736   0.047    2.096  ...   102.0     118.0      36.0   1.00
Ia_begin  1.485  1.440   0.046    3.218  ...    81.0      68.0      60.0   0.99
E_begin   0.755  0.982   0.025    2.342  ...    85.0      82.0      60.0   1.04

[8 rows x 11 columns]