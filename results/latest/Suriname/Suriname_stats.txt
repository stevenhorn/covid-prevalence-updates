0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo  -997.42    28.82
p_loo       21.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.6%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.272  0.051   0.186    0.335  ...   105.0     101.0      57.0   1.01
pu        0.872  0.022   0.831    0.898  ...    74.0      62.0      43.0   1.04
mu        0.150  0.022   0.108    0.185  ...     9.0       9.0      42.0   1.18
mus       0.171  0.033   0.126    0.230  ...    90.0     106.0      93.0   1.01
gamma     0.261  0.052   0.175    0.361  ...    36.0      36.0      37.0   1.02
Is_begin  0.569  0.392   0.098    1.213  ...   127.0     104.0      60.0   1.00
Ia_begin  0.945  0.820   0.004    2.257  ...   102.0      67.0      57.0   1.02
E_begin   0.482  0.511   0.003    1.490  ...    96.0      76.0      36.0   1.00

[8 rows x 11 columns]