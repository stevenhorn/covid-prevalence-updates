0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -539.60    37.35
p_loo       32.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.049   0.151    0.317  ...    91.0      95.0      56.0   1.02
pu        0.821  0.023   0.778    0.852  ...    99.0      98.0      77.0   1.01
mu        0.129  0.022   0.090    0.176  ...    31.0      24.0      59.0   1.08
mus       0.180  0.041   0.111    0.256  ...    31.0     142.0      20.0   1.23
gamma     0.207  0.037   0.129    0.269  ...   108.0     118.0      57.0   0.99
Is_begin  0.685  0.616   0.016    2.101  ...    91.0      67.0      59.0   1.00
Ia_begin  1.252  1.013   0.034    3.293  ...    64.0      50.0      56.0   1.04
E_begin   0.565  0.633   0.001    1.716  ...    83.0      52.0      35.0   1.01

[8 rows x 11 columns]