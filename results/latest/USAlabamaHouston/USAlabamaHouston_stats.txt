0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1268.48    49.67
p_loo       33.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.056   0.165    0.337  ...    85.0      72.0      53.0   1.00
pu        0.798  0.027   0.745    0.845  ...    70.0      67.0     100.0   1.02
mu        0.122  0.029   0.074    0.178  ...    30.0      27.0      18.0   1.02
mus       0.166  0.031   0.114    0.212  ...   114.0     143.0      93.0   0.99
gamma     0.179  0.038   0.113    0.260  ...   152.0     152.0      78.0   1.02
Is_begin  0.837  0.716   0.007    2.324  ...    80.0     130.0      54.0   1.06
Ia_begin  1.700  1.458   0.049    4.252  ...    85.0      81.0      93.0   1.00
E_begin   0.818  1.073   0.002    2.554  ...    90.0      82.0      60.0   1.02

[8 rows x 11 columns]