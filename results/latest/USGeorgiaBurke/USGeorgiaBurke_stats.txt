0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -941.44    57.85
p_loo       38.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.057   0.166    0.346  ...    51.0      42.0      17.0   1.02
pu        0.766  0.031   0.716    0.823  ...    44.0      38.0      21.0   1.04
mu        0.139  0.026   0.095    0.175  ...    39.0      47.0      31.0   0.99
mus       0.232  0.035   0.172    0.290  ...    63.0      60.0      59.0   1.05
gamma     0.350  0.061   0.247    0.466  ...   110.0     117.0      59.0   1.01
Is_begin  1.034  1.087   0.061    2.783  ...   109.0      75.0      59.0   0.98
Ia_begin  1.773  1.954   0.004    5.551  ...    35.0      10.0      22.0   1.19
E_begin   1.044  1.128   0.027    3.316  ...    52.0      34.0      68.0   1.02

[8 rows x 11 columns]