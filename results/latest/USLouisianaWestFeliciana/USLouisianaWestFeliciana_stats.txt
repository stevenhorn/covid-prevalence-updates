0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -940.93    30.62
p_loo       35.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.050   0.171    0.329  ...    96.0     103.0      95.0   1.01
pu        0.783  0.036   0.716    0.842  ...    27.0      31.0      24.0   1.06
mu        0.135  0.026   0.094    0.185  ...    55.0      52.0      81.0   1.03
mus       0.167  0.028   0.118    0.210  ...    57.0      65.0      60.0   1.02
gamma     0.219  0.045   0.144    0.300  ...   143.0     152.0      45.0   1.04
Is_begin  1.191  0.990   0.004    2.922  ...   113.0      69.0      57.0   1.03
Ia_begin  2.758  2.489   0.109    7.408  ...    89.0      71.0      60.0   1.03
E_begin   1.974  2.286   0.007    6.913  ...    59.0      31.0      75.0   1.07

[8 rows x 11 columns]