0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -789.57    33.47
p_loo       37.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      283   95.9%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.203  0.033   0.153    0.266  ...    30.0      22.0      22.0   1.10
pu        0.720  0.017   0.700    0.760  ...    21.0      41.0      60.0   1.20
mu        0.154  0.025   0.110    0.193  ...    40.0      38.0      59.0   1.00
mus       0.249  0.032   0.178    0.293  ...    61.0      77.0      59.0   1.02
gamma     0.370  0.060   0.254    0.457  ...    67.0      73.0      88.0   1.03
Is_begin  0.387  0.469   0.002    1.415  ...    27.0      34.0      58.0   1.06
Ia_begin  0.618  0.894   0.008    2.286  ...   101.0      72.0      59.0   1.01
E_begin   0.313  0.398   0.005    0.922  ...    59.0      45.0      77.0   1.03

[8 rows x 11 columns]