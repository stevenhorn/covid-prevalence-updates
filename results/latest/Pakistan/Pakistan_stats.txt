0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -2233.27    36.51
p_loo       23.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      258   87.2%
 (0.5, 0.7]   (ok)         30   10.1%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    1    0.3%

             mean       sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.254    0.052   0.170    0.345  ...    26.0      35.0      19.0   1.20
pu          0.786    0.059   0.707    0.898  ...    42.0      43.0      57.0   1.04
mu          0.121    0.029   0.070    0.164  ...     4.0       4.0      41.0   1.58
mus         0.178    0.035   0.123    0.250  ...    60.0      64.0      60.0   1.03
gamma       0.225    0.046   0.150    0.300  ...   152.0     152.0      60.0   0.98
Is_begin  170.428  106.993  17.513  366.302  ...    39.0      38.0      58.0   1.00
Ia_begin  416.566  224.661  61.070  765.410  ...    67.0      60.0      60.0   0.98
E_begin   305.653  244.792  24.639  802.007  ...   104.0      98.0      91.0   0.99

[8 rows x 11 columns]