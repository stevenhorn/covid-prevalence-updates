0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -885.26    30.66
p_loo       24.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      266   90.2%
 (0.5, 0.7]   (ok)         24    8.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.054   0.177    0.345  ...    89.0      94.0      69.0   1.03
pu        0.830  0.050   0.751    0.899  ...    68.0      53.0      48.0   1.05
mu        0.131  0.022   0.094    0.170  ...    26.0      33.0      60.0   1.05
mus       0.178  0.031   0.118    0.233  ...   140.0     152.0      59.0   1.00
gamma     0.228  0.038   0.153    0.288  ...    71.0      77.0      88.0   1.00
Is_begin  1.289  1.198   0.029    3.237  ...    53.0      36.0      60.0   1.02
Ia_begin  3.443  2.930   0.002    8.748  ...    72.0      55.0      40.0   1.01
E_begin   2.529  2.837   0.001    8.614  ...    91.0      61.0      59.0   1.02

[8 rows x 11 columns]