0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -558.52    34.64
p_loo       25.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      281   95.3%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.284  0.044   0.201    0.346  ...   129.0     120.0      61.0   1.02
pu        0.889  0.010   0.868    0.899  ...    45.0      62.0      38.0   1.02
mu        0.134  0.019   0.100    0.163  ...    25.0      25.0      49.0   1.07
mus       0.156  0.030   0.108    0.205  ...    76.0      72.0      56.0   1.00
gamma     0.219  0.036   0.152    0.277  ...    77.0      79.0      60.0   1.01
Is_begin  0.711  0.684   0.005    2.514  ...   110.0      91.0      59.0   1.00
Ia_begin  1.581  1.605   0.001    4.780  ...    58.0      64.0      60.0   1.04
E_begin   0.761  0.877   0.003    2.302  ...    55.0      50.0      60.0   1.03

[8 rows x 11 columns]