0 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo  -120.65    35.23
p_loo       31.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   90.6%
 (0.5, 0.7]   (ok)         21    6.8%
   (0.7, 1]   (bad)         6    1.9%
   (1, Inf)   (very bad)    2    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.270  0.056   0.179    0.349  ...   152.0     152.0     100.0   1.10
pu        0.880  0.017   0.850    0.900  ...   112.0      78.0      48.0   1.05
mu        0.159  0.023   0.122    0.200  ...    90.0      90.0      76.0   1.04
mus       0.199  0.032   0.144    0.265  ...    79.0      78.0      37.0   1.01
gamma     0.263  0.037   0.204    0.340  ...   152.0     152.0      80.0   0.99
Is_begin  0.729  0.694   0.014    2.148  ...    49.0     124.0      46.0   1.00
Ia_begin  1.162  0.852   0.088    2.949  ...   108.0     136.0      54.0   1.07
E_begin   0.723  0.638   0.031    2.047  ...   103.0      83.0      46.0   1.01

[8 rows x 11 columns]