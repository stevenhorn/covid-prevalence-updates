0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -601.39    48.37
p_loo       35.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.215  0.054   0.151    0.303  ...   152.0     152.0      60.0   1.02
pu        0.723  0.016   0.700    0.749  ...   152.0     152.0      56.0   1.09
mu        0.126  0.020   0.091    0.162  ...    83.0      99.0      59.0   0.99
mus       0.195  0.035   0.124    0.246  ...   115.0     143.0      57.0   0.99
gamma     0.282  0.049   0.198    0.373  ...   152.0     152.0      99.0   1.03
Is_begin  1.014  0.916   0.015    3.066  ...   152.0     149.0      91.0   1.01
Ia_begin  2.562  2.726   0.001    6.876  ...   129.0      82.0      40.0   1.02
E_begin   0.971  1.001   0.002    2.992  ...    58.0      19.0      16.0   1.09

[8 rows x 11 columns]