0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -430.04    22.98
p_loo       19.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.285  0.058   0.165    0.350  ...    45.0      81.0      25.0   1.18
pu        0.891  0.009   0.874    0.900  ...    85.0      60.0      43.0   1.01
mu        0.133  0.024   0.091    0.177  ...    31.0      33.0      38.0   1.00
mus       0.169  0.035   0.093    0.230  ...   152.0     152.0      67.0   0.99
gamma     0.200  0.039   0.152    0.279  ...    79.0      96.0      83.0   1.04
Is_begin  0.794  0.670   0.006    2.229  ...   152.0     142.0      46.0   1.05
Ia_begin  1.362  0.870   0.158    2.818  ...    87.0      99.0      40.0   1.00
E_begin   0.777  0.780   0.019    2.554  ...    94.0      58.0      18.0   1.03

[8 rows x 11 columns]