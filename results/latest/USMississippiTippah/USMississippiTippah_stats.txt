0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -791.71    20.10
p_loo       19.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.053   0.155    0.338  ...    70.0      71.0      45.0   1.00
pu        0.813  0.023   0.766    0.856  ...    29.0      31.0      59.0   1.05
mu        0.119  0.015   0.096    0.147  ...    23.0      25.0      31.0   1.07
mus       0.166  0.027   0.112    0.210  ...    61.0      55.0      41.0   1.02
gamma     0.205  0.045   0.145    0.303  ...    90.0      94.0      39.0   1.02
Is_begin  1.310  1.246   0.016    3.617  ...    83.0      56.0      35.0   1.00
Ia_begin  0.733  0.709   0.000    1.973  ...    70.0      29.0      22.0   1.03
E_begin   0.693  0.813   0.001    2.394  ...    21.0      39.0      23.0   1.06

[8 rows x 11 columns]