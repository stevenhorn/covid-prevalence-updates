0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -330.34    28.96
p_loo       27.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      263   89.2%
 (0.5, 0.7]   (ok)         26    8.8%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.054   0.152    0.325  ...    54.0      51.0      40.0   1.00
pu        0.855  0.014   0.829    0.878  ...    62.0      61.0      54.0   1.03
mu        0.123  0.022   0.083    0.159  ...    57.0      47.0      96.0   1.00
mus       0.182  0.035   0.106    0.235  ...    67.0      65.0      21.0   1.12
gamma     0.222  0.032   0.174    0.288  ...   152.0     152.0      77.0   0.99
Is_begin  0.433  0.414   0.006    1.342  ...    56.0      27.0      42.0   1.06
Ia_begin  0.745  0.762   0.003    2.391  ...    56.0      37.0      54.0   1.01
E_begin   0.362  0.419   0.010    1.030  ...    26.0      11.0      80.0   1.15

[8 rows x 11 columns]