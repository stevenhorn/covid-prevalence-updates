0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1150.19    32.30
p_loo       23.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.045   0.185    0.332  ...    73.0      92.0      60.0   1.14
pu        0.783  0.032   0.735    0.838  ...    46.0      46.0      37.0   1.06
mu        0.105  0.019   0.071    0.136  ...    11.0      12.0      57.0   1.16
mus       0.152  0.035   0.094    0.203  ...    94.0     102.0      96.0   1.00
gamma     0.176  0.033   0.121    0.237  ...   152.0     152.0      79.0   1.01
Is_begin  1.007  1.124   0.001    2.836  ...    92.0      36.0      15.0   1.03
Ia_begin  2.467  2.584   0.063    6.650  ...    57.0      33.0      42.0   1.07
E_begin   1.389  1.450   0.008    3.050  ...    10.0       8.0      22.0   1.24

[8 rows x 11 columns]