0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1415.46    29.77
p_loo       11.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      285   96.6%
 (0.5, 0.7]   (ok)          6    2.0%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.044   0.180    0.329  ...     3.0       3.0      18.0   2.03
pu        0.794  0.014   0.777    0.818  ...     3.0       3.0      22.0   1.98
mu        0.169  0.021   0.142    0.197  ...     3.0       3.0      15.0   2.81
mus       0.163  0.021   0.137    0.196  ...     3.0       3.0      22.0   2.14
gamma     0.143  0.006   0.132    0.154  ...     3.0       3.0      14.0   1.91
Is_begin  1.220  0.584   0.400    2.066  ...     3.0       3.0      20.0   2.06
Ia_begin  1.458  0.641   0.686    2.580  ...     3.0       4.0      36.0   1.83
E_begin   1.565  1.062   0.474    3.981  ...     3.0       3.0      24.0   1.87

[8 rows x 11 columns]