0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -433.72    33.58
p_loo       30.05        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.272  0.058   0.158    0.348  ...   100.0      80.0      30.0   1.12
pu        0.879  0.024   0.840    0.900  ...    75.0      56.0      40.0   1.02
mu        0.171  0.037   0.113    0.242  ...    44.0      44.0      91.0   0.99
mus       0.185  0.031   0.132    0.238  ...   152.0     141.0      83.0   1.01
gamma     0.242  0.045   0.171    0.304  ...   152.0     152.0      99.0   0.99
Is_begin  0.779  0.628   0.048    2.003  ...    38.0     151.0      79.0   1.00
Ia_begin  1.668  2.064   0.003    4.970  ...    94.0      82.0      95.0   1.03
E_begin   0.799  1.137   0.002    2.401  ...    99.0      39.0      60.0   1.04

[8 rows x 11 columns]