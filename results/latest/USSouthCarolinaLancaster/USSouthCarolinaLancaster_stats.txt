0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1035.08    25.81
p_loo       26.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.059   0.158    0.339  ...    40.0      36.0      48.0   1.04
pu        0.826  0.039   0.750    0.890  ...    23.0      24.0      16.0   1.03
mu        0.113  0.023   0.074    0.153  ...    42.0      43.0      56.0   1.05
mus       0.159  0.030   0.108    0.211  ...   152.0     152.0     100.0   1.00
gamma     0.170  0.035   0.102    0.227  ...   152.0     152.0      74.0   1.01
Is_begin  1.494  1.281   0.003    3.882  ...   145.0      88.0      33.0   1.09
Ia_begin  2.876  2.505   0.004    7.851  ...    47.0      22.0      22.0   1.08
E_begin   1.808  1.763   0.010    5.475  ...    96.0      67.0      59.0   1.00

[8 rows x 11 columns]