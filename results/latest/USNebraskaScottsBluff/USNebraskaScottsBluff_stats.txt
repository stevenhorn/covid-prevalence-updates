0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -931.78    52.88
p_loo       35.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      283   95.9%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.055   0.150    0.334  ...    71.0      69.0      59.0   1.00
pu        0.819  0.017   0.781    0.846  ...   152.0     152.0      58.0   1.00
mu        0.107  0.017   0.078    0.139  ...    71.0      81.0      93.0   0.99
mus       0.187  0.044   0.097    0.257  ...   152.0     150.0      60.0   1.00
gamma     0.278  0.046   0.205    0.365  ...    94.0      96.0      60.0   1.00
Is_begin  0.595  0.500   0.009    1.611  ...   120.0     132.0      91.0   0.99
Ia_begin  1.093  1.077   0.003    3.071  ...    97.0      72.0     100.0   1.00
E_begin   0.534  0.586   0.029    1.789  ...   123.0     152.0      86.0   1.01

[8 rows x 11 columns]