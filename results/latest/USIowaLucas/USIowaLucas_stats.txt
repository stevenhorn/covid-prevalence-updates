0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -569.66    23.28
p_loo       22.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.051   0.170    0.345  ...    79.0      80.0      43.0   1.03
pu        0.834  0.031   0.772    0.882  ...    31.0      36.0      39.0   1.04
mu        0.118  0.024   0.080    0.161  ...    69.0      66.0      57.0   0.99
mus       0.174  0.037   0.114    0.222  ...   152.0     152.0      93.0   1.01
gamma     0.203  0.041   0.134    0.275  ...    86.0     152.0      72.0   1.01
Is_begin  0.377  0.497   0.004    1.174  ...    94.0      75.0      93.0   1.00
Ia_begin  0.657  0.782   0.000    2.574  ...   103.0     112.0      77.0   0.99
E_begin   0.349  0.543   0.005    1.180  ...    96.0      66.0      57.0   1.00

[8 rows x 11 columns]