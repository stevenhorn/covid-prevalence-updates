1 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1755.20    53.43
p_loo       28.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         24    8.1%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.245   0.053   0.152    0.322  ...   100.0     106.0      96.0   1.00
pu         0.758   0.045   0.703    0.841  ...    83.0      70.0      60.0   0.99
mu         0.123   0.021   0.091    0.160  ...     7.0       6.0      23.0   1.37
mus        0.198   0.034   0.142    0.276  ...    81.0     152.0      93.0   1.01
gamma      0.283   0.044   0.201    0.359  ...   130.0     142.0      75.0   0.98
Is_begin  23.348  12.827   5.033   42.416  ...    57.0      51.0      59.0   1.04
Ia_begin  54.209  32.967   1.052  108.888  ...    45.0      28.0      22.0   1.07
E_begin   52.999  38.799   0.870  133.068  ...    50.0      38.0      57.0   1.02

[8 rows x 11 columns]