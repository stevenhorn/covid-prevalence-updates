0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1050.14    28.43
p_loo       19.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.057   0.165    0.340  ...   110.0      96.0      58.0   0.98
pu        0.845  0.027   0.782    0.881  ...    24.0      25.0      42.0   1.08
mu        0.118  0.024   0.081    0.151  ...    15.0      18.0      40.0   1.13
mus       0.148  0.027   0.096    0.193  ...   125.0     135.0      48.0   1.00
gamma     0.173  0.034   0.104    0.219  ...   152.0     152.0     100.0   1.02
Is_begin  0.635  0.694   0.002    1.836  ...   118.0      78.0      54.0   1.00
Ia_begin  1.595  1.367   0.014    4.578  ...    63.0      59.0      63.0   1.01
E_begin   0.539  0.531   0.004    1.665  ...    81.0      80.0      59.0   0.98

[8 rows x 11 columns]