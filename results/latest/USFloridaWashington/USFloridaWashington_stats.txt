0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -870.99    30.68
p_loo       31.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      261   88.5%
 (0.5, 0.7]   (ok)         29    9.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.055   0.166    0.348  ...    69.0      66.0      38.0   1.06
pu        0.778  0.047   0.700    0.839  ...     5.0       6.0      17.0   1.31
mu        0.173  0.024   0.140    0.221  ...    46.0      41.0      56.0   1.05
mus       0.221  0.026   0.180    0.278  ...    60.0      63.0      59.0   1.04
gamma     0.289  0.047   0.222    0.386  ...   106.0     112.0      91.0   0.99
Is_begin  0.719  0.668   0.039    1.988  ...   111.0     103.0      54.0   1.02
Ia_begin  1.099  1.130   0.023    3.235  ...    76.0      62.0      60.0   0.99
E_begin   0.495  0.464   0.008    1.437  ...    76.0      68.0      59.0   1.05

[8 rows x 11 columns]