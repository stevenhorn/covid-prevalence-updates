0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -542.40    28.03
p_loo       21.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.044   0.184    0.340  ...   143.0     131.0      60.0   1.00
pu        0.874  0.014   0.848    0.897  ...   101.0     107.0      59.0   0.99
mu        0.140  0.026   0.095    0.188  ...     7.0       8.0      58.0   1.25
mus       0.151  0.024   0.112    0.194  ...    95.0      85.0      60.0   1.02
gamma     0.171  0.032   0.102    0.223  ...    67.0      65.0      60.0   1.02
Is_begin  0.568  0.532   0.004    1.445  ...   108.0      78.0      46.0   1.03
Ia_begin  1.684  1.718   0.003    3.780  ...    56.0      61.0      22.0   1.06
E_begin   0.733  1.052   0.000    2.335  ...    48.0      62.0      59.0   0.99

[8 rows x 11 columns]