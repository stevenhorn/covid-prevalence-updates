0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -679.65    31.11
p_loo       28.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      262   88.8%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.059   0.168    0.350  ...   105.0      91.0      22.0   0.99
pu        0.816  0.027   0.770    0.870  ...   128.0     127.0     100.0   1.02
mu        0.128  0.023   0.095    0.174  ...    45.0      43.0      33.0   1.07
mus       0.173  0.032   0.117    0.223  ...   152.0     152.0      59.0   1.02
gamma     0.203  0.039   0.147    0.282  ...    77.0      79.0      97.0   1.02
Is_begin  0.763  0.731   0.004    2.265  ...    89.0      83.0      88.0   1.04
Ia_begin  1.598  1.577   0.012    4.418  ...    82.0      97.0      49.0   1.00
E_begin   0.775  1.047   0.000    2.805  ...    98.0      60.0      59.0   1.00

[8 rows x 11 columns]