0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo    -0.54    51.69
p_loo       59.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      281   94.9%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.051   0.175    0.348  ...    28.0      28.0      38.0   1.07
pu        0.769  0.042   0.700    0.836  ...    58.0      62.0      59.0   1.01
mu        0.147  0.036   0.092    0.213  ...    63.0      79.0      42.0   1.02
mus       0.238  0.052   0.148    0.324  ...    78.0      72.0      54.0   1.11
gamma     0.258  0.056   0.165    0.354  ...   104.0     115.0      60.0   1.01
Is_begin  0.679  0.595   0.004    1.571  ...    79.0      64.0      60.0   0.99
Ia_begin  1.450  1.371   0.006    3.829  ...    97.0     115.0      66.0   1.00
E_begin   0.628  0.572   0.004    1.642  ...    41.0      23.0      65.0   1.07

[8 rows x 11 columns]