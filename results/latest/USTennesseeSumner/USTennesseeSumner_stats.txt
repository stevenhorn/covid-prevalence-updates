0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1353.38    28.44
p_loo       25.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.228  0.047   0.158    0.305  ...   121.0     115.0      75.0   1.00
pu         0.739  0.030   0.702    0.799  ...    24.0      40.0      60.0   1.05
mu         0.123  0.023   0.088    0.167  ...    22.0      18.0      59.0   1.10
mus        0.195  0.034   0.139    0.251  ...    35.0      34.0     100.0   1.05
gamma      0.291  0.056   0.196    0.389  ...   152.0     152.0     100.0   0.98
Is_begin   3.588  2.250   0.342    6.919  ...    97.0      75.0      57.0   0.99
Ia_begin   7.966  4.365   1.462   15.667  ...    60.0      64.0      40.0   0.99
E_begin   11.025  9.711   0.120   30.925  ...    48.0      51.0      52.0   0.99

[8 rows x 11 columns]