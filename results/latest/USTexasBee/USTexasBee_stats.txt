0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1093.25    40.34
p_loo       35.34        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.061   0.155    0.343  ...    35.0      27.0      33.0   1.06
pu        0.779  0.025   0.728    0.822  ...    11.0      12.0      14.0   1.15
mu        0.136  0.021   0.102    0.179  ...    31.0      36.0      21.0   1.12
mus       0.184  0.031   0.131    0.237  ...   101.0      96.0      57.0   1.02
gamma     0.245  0.040   0.182    0.334  ...    77.0      80.0      88.0   1.00
Is_begin  0.721  0.760   0.003    2.464  ...   114.0      62.0      59.0   1.01
Ia_begin  1.312  1.237   0.013    3.753  ...    73.0      42.0      57.0   1.07
E_begin   0.593  0.747   0.016    1.872  ...    45.0      20.0      60.0   1.10

[8 rows x 11 columns]