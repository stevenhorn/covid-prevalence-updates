0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -821.15    25.39
p_loo       23.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.054   0.166    0.346  ...    39.0      37.0      60.0   1.05
pu        0.878  0.016   0.851    0.899  ...    52.0      35.0      54.0   1.04
mu        0.125  0.027   0.082    0.173  ...    22.0      23.0      22.0   1.05
mus       0.153  0.028   0.103    0.208  ...   152.0     152.0      65.0   1.07
gamma     0.179  0.043   0.120    0.286  ...   152.0     152.0      23.0   1.00
Is_begin  0.727  0.839   0.004    2.224  ...    91.0      91.0      22.0   1.01
Ia_begin  1.760  2.152   0.001    6.437  ...    62.0      64.0      39.0   1.04
E_begin   0.668  0.823   0.009    2.196  ...    68.0      53.0      96.0   1.02

[8 rows x 11 columns]