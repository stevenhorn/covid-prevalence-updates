0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1194.60    57.64
p_loo       37.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.239   0.050   0.162    0.329  ...   152.0     151.0      56.0   1.07
pu         0.762   0.037   0.709    0.836  ...    38.0      27.0      21.0   1.07
mu         0.140   0.025   0.085    0.180  ...    37.0      34.0      60.0   1.08
mus        0.211   0.037   0.153    0.294  ...    83.0      83.0      57.0   1.01
gamma      0.255   0.048   0.189    0.359  ...   119.0     120.0      45.0   1.02
Is_begin  18.148   9.291   1.886   34.423  ...    54.0      53.0      40.0   1.04
Ia_begin  26.793  20.931   0.303   65.294  ...    51.0      34.0      33.0   1.04
E_begin   21.284  21.144   0.584   67.271  ...    60.0      51.0      72.0   1.02

[8 rows x 11 columns]