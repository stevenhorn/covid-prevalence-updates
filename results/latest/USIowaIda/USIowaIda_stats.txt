0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -542.11    26.65
p_loo       23.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.057   0.168    0.341  ...    60.0      70.0      80.0   1.03
pu        0.799  0.023   0.754    0.834  ...    51.0      48.0      87.0   1.02
mu        0.126  0.024   0.082    0.170  ...    35.0      33.0      40.0   1.01
mus       0.191  0.036   0.127    0.261  ...    78.0      43.0      25.0   1.04
gamma     0.234  0.042   0.162    0.324  ...   152.0     152.0      60.0   1.02
Is_begin  0.408  0.389   0.007    1.172  ...    53.0      52.0      69.0   1.01
Ia_begin  0.541  0.588   0.009    2.011  ...    48.0      24.0      57.0   1.06
E_begin   0.252  0.252   0.005    0.827  ...    33.0      41.0      69.0   1.02

[8 rows x 11 columns]