0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -483.93    26.83
p_loo       19.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.287  0.046   0.211    0.350  ...    97.0     104.0      39.0   1.00
pu        0.890  0.010   0.873    0.899  ...    80.0      98.0      59.0   1.02
mu        0.136  0.023   0.098    0.179  ...    67.0      65.0      72.0   1.03
mus       0.149  0.031   0.092    0.210  ...   122.0     100.0      53.0   1.08
gamma     0.176  0.032   0.131    0.243  ...   152.0     152.0      49.0   1.01
Is_begin  1.086  1.031   0.013    3.347  ...    66.0      78.0      38.0   1.00
Ia_begin  2.579  1.635   0.312    5.388  ...   109.0      99.0      96.0   1.01
E_begin   1.592  1.828   0.007    4.691  ...   107.0     121.0      54.0   0.98

[8 rows x 11 columns]