0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -479.54    23.95
p_loo       20.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.055   0.178    0.346  ...    86.0      82.0      51.0   1.01
pu        0.848  0.029   0.801    0.881  ...    21.0      18.0      60.0   1.08
mu        0.124  0.025   0.083    0.165  ...    58.0      47.0      38.0   1.05
mus       0.151  0.028   0.114    0.204  ...   152.0     152.0      61.0   0.99
gamma     0.166  0.034   0.112    0.222  ...   152.0     152.0      55.0   0.99
Is_begin  0.629  0.672   0.028    1.772  ...    92.0     111.0      75.0   1.01
Ia_begin  1.256  1.266   0.016    4.108  ...    86.0      70.0      58.0   1.00
E_begin   0.585  0.674   0.023    2.192  ...    89.0      46.0      27.0   1.02

[8 rows x 11 columns]