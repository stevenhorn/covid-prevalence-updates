0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1423.13    31.09
p_loo       26.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      260   88.1%
 (0.5, 0.7]   (ok)         28    9.5%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.057   0.151    0.331  ...   152.0     152.0      86.0   1.08
pu        0.771  0.049   0.700    0.850  ...    65.0      62.0      29.0   1.06
mu        0.133  0.026   0.091    0.182  ...    30.0      26.0      25.0   1.06
mus       0.192  0.033   0.130    0.244  ...   152.0     152.0      83.0   1.01
gamma     0.295  0.050   0.214    0.389  ...   133.0     131.0     100.0   0.99
Is_begin  2.026  1.449   0.204    4.688  ...   150.0     152.0      88.0   1.06
Ia_begin  0.784  0.657   0.030    1.959  ...    56.0      72.0      69.0   1.02
E_begin   2.299  3.114   0.064    8.059  ...    84.0     105.0      93.0   0.99

[8 rows x 11 columns]