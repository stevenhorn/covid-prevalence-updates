0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -471.04    27.69
p_loo       27.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.060   0.164    0.342  ...    58.0      56.0      59.0   1.04
pu        0.866  0.014   0.844    0.897  ...    70.0      70.0      60.0   1.02
mu        0.118  0.026   0.084    0.185  ...    80.0      72.0      96.0   1.00
mus       0.176  0.029   0.130    0.229  ...    80.0      45.0      46.0   1.05
gamma     0.222  0.037   0.161    0.281  ...    24.0      34.0      86.0   1.07
Is_begin  0.378  0.361   0.004    1.006  ...    74.0      60.0      51.0   0.99
Ia_begin  0.787  1.008   0.006    2.861  ...    83.0      55.0      74.0   0.99
E_begin   0.411  0.423   0.003    1.263  ...    87.0      72.0      95.0   1.02

[8 rows x 11 columns]