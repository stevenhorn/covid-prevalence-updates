0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -467.46    26.97
p_loo       21.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      281   95.3%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.047   0.160    0.327  ...    74.0      74.0      40.0   0.99
pu        0.821  0.021   0.785    0.858  ...    32.0      44.0      59.0   1.04
mu        0.132  0.023   0.094    0.174  ...    64.0      62.0      60.0   1.12
mus       0.173  0.028   0.122    0.227  ...    63.0      67.0      64.0   1.03
gamma     0.199  0.044   0.127    0.280  ...    59.0      92.0      65.0   1.01
Is_begin  0.478  0.536   0.003    1.670  ...    19.0      12.0      15.0   1.12
Ia_begin  0.694  0.695   0.045    2.099  ...    61.0      66.0      60.0   1.00
E_begin   0.427  0.522   0.003    1.341  ...    70.0      38.0      41.0   1.06

[8 rows x 11 columns]