1 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo   -77.79    31.16
p_loo       27.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      260   88.1%
 (0.5, 0.7]   (ok)         28    9.5%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.060   0.164    0.338  ...    65.0      62.0      42.0   1.02
pu        0.860  0.023   0.816    0.898  ...     7.0       7.0      59.0   1.27
mu        0.136  0.024   0.099    0.183  ...    58.0      53.0      53.0   1.06
mus       0.194  0.042   0.130    0.269  ...    60.0      62.0      55.0   1.08
gamma     0.224  0.035   0.171    0.298  ...    40.0      47.0      40.0   1.05
Is_begin  0.261  0.252   0.003    0.705  ...    23.0      20.0      40.0   1.10
Ia_begin  0.483  0.505   0.014    1.496  ...    39.0      46.0      45.0   1.10
E_begin   0.250  0.294   0.003    0.863  ...    36.0      44.0      30.0   1.04

[8 rows x 11 columns]