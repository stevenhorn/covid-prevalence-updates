0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -770.89    57.22
p_loo       39.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.222  0.046   0.159    0.305  ...    72.0      74.0      91.0   1.01
pu        0.738  0.020   0.703    0.768  ...    82.0      76.0      60.0   1.03
mu        0.110  0.019   0.077    0.149  ...    57.0      81.0      92.0   1.02
mus       0.188  0.034   0.135    0.247  ...   126.0     152.0      59.0   0.99
gamma     0.233  0.040   0.166    0.309  ...    43.0      40.0      55.0   1.01
Is_begin  0.169  0.191   0.000    0.574  ...    47.0      40.0      60.0   1.02
Ia_begin  0.305  0.364   0.001    1.178  ...    82.0      51.0      40.0   1.02
E_begin   0.112  0.127   0.001    0.365  ...    67.0      43.0      83.0   1.02

[8 rows x 11 columns]