0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -610.73    25.13
p_loo       17.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.282  0.052   0.185    0.348  ...   152.0     152.0      74.0   0.98
pu        0.883  0.015   0.859    0.899  ...    43.0      62.0      60.0   1.02
mu        0.120  0.024   0.077    0.155  ...    37.0      48.0      39.0   1.04
mus       0.156  0.028   0.116    0.208  ...   115.0     132.0      77.0   1.01
gamma     0.166  0.035   0.114    0.227  ...   102.0      97.0      55.0   1.02
Is_begin  0.665  0.695   0.005    1.984  ...    93.0      80.0      59.0   0.98
Ia_begin  1.402  1.468   0.026    4.490  ...    73.0      58.0      53.0   1.04
E_begin   0.476  0.528   0.000    1.376  ...    56.0      56.0      40.0   1.04

[8 rows x 11 columns]