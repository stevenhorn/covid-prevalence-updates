0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -340.88    28.26
p_loo       27.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.052   0.152    0.321  ...    74.0      72.0      39.0   1.02
pu        0.842  0.032   0.774    0.882  ...    12.0      20.0      22.0   1.10
mu        0.124  0.023   0.094    0.172  ...    20.0      19.0      59.0   1.08
mus       0.165  0.035   0.115    0.233  ...    69.0      71.0      86.0   0.98
gamma     0.188  0.044   0.114    0.269  ...   140.0     145.0      55.0   1.00
Is_begin  0.601  0.588   0.006    1.612  ...    76.0      54.0      95.0   1.08
Ia_begin  0.995  1.025   0.002    2.918  ...    51.0      37.0      43.0   1.02
E_begin   0.626  0.752   0.002    1.704  ...    75.0      58.0      54.0   1.03

[8 rows x 11 columns]