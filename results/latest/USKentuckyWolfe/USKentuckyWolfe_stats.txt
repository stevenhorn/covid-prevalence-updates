0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -457.20    26.41
p_loo       24.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      283   95.9%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.286  0.044   0.205    0.345  ...   152.0     152.0     100.0   0.99
pu        0.882  0.021   0.837    0.900  ...    55.0      64.0      38.0   1.04
mu        0.126  0.022   0.091    0.166  ...    77.0      77.0      57.0   1.00
mus       0.157  0.043   0.081    0.229  ...   131.0     152.0      40.0   1.06
gamma     0.178  0.041   0.102    0.249  ...   152.0     152.0      95.0   1.11
Is_begin  0.474  0.538   0.002    1.687  ...   116.0      92.0      59.0   1.03
Ia_begin  1.071  1.372   0.003    3.925  ...    98.0     105.0      96.0   0.99
E_begin   0.506  0.868   0.000    1.172  ...    80.0      84.0      51.0   1.00

[8 rows x 11 columns]