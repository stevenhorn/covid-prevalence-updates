0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -266.93    53.25
p_loo       55.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      262   88.8%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.053   0.185    0.344  ...   120.0     119.0      60.0   1.00
pu        0.858  0.037   0.785    0.897  ...    69.0      64.0      59.0   1.01
mu        0.131  0.026   0.087    0.173  ...    84.0      79.0      69.0   1.02
mus       0.167  0.035   0.111    0.229  ...    67.0      88.0      75.0   1.00
gamma     0.191  0.039   0.135    0.271  ...   118.0     101.0      36.0   1.03
Is_begin  0.441  0.521   0.001    1.798  ...    21.0      23.0      85.0   1.07
Ia_begin  0.817  0.998   0.009    2.680  ...    38.0      45.0      59.0   1.02
E_begin   0.407  0.472   0.017    1.411  ...    46.0      67.0      88.0   1.00

[8 rows x 11 columns]