0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -507.30    69.67
p_loo       52.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.211  0.048   0.151    0.310  ...   106.0     104.0      60.0   1.01
pu        0.722  0.018   0.700    0.755  ...    74.0      65.0      30.0   1.01
mu        0.138  0.025   0.092    0.172  ...    86.0      79.0      18.0   1.00
mus       0.210  0.030   0.151    0.251  ...    87.0      84.0      28.0   1.02
gamma     0.267  0.059   0.161    0.385  ...    86.0     107.0      81.0   0.99
Is_begin  0.415  0.479   0.001    1.385  ...    64.0      23.0      88.0   1.08
Ia_begin  0.587  0.700   0.000    2.076  ...   105.0      60.0      38.0   1.04
E_begin   0.256  0.357   0.000    1.175  ...    69.0      26.0      59.0   1.06

[8 rows x 11 columns]