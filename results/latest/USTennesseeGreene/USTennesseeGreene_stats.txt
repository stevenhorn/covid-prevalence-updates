0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1049.50    28.86
p_loo       22.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.053   0.154    0.325  ...    82.0      78.0      60.0   1.07
pu        0.824  0.021   0.783    0.856  ...    64.0      63.0      54.0   1.04
mu        0.130  0.025   0.084    0.173  ...    10.0      11.0      22.0   1.16
mus       0.157  0.030   0.088    0.205  ...    55.0      60.0      15.0   1.01
gamma     0.179  0.037   0.114    0.232  ...   116.0     115.0      74.0   1.00
Is_begin  0.770  0.786   0.018    2.517  ...    80.0      42.0      60.0   1.02
Ia_begin  1.689  1.970   0.034    6.034  ...    28.0      15.0      28.0   1.10
E_begin   0.610  0.925   0.003    1.870  ...    88.0      66.0      40.0   1.02

[8 rows x 11 columns]