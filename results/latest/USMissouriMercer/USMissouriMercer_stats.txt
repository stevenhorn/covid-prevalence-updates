0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -336.75    33.15
p_loo       29.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.045   0.193    0.346  ...   122.0     123.0     100.0   1.04
pu        0.865  0.030   0.816    0.899  ...    37.0      44.0      19.0   1.13
mu        0.131  0.023   0.096    0.181  ...    44.0      71.0      57.0   1.06
mus       0.176  0.032   0.114    0.225  ...   115.0     124.0      72.0   1.00
gamma     0.217  0.040   0.152    0.292  ...    80.0     101.0      88.0   1.00
Is_begin  0.505  0.495   0.011    1.424  ...    33.0      44.0      39.0   1.00
Ia_begin  0.851  0.974   0.009    2.740  ...    37.0      22.0      59.0   1.06
E_begin   0.420  0.449   0.006    1.385  ...    41.0      42.0      60.0   1.01

[8 rows x 11 columns]