0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1281.94    25.40
p_loo       21.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      253   85.8%
 (0.5, 0.7]   (ok)         34   11.5%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.059   0.160    0.348  ...    94.0      98.0      59.0   1.07
pu        0.789  0.059   0.700    0.872  ...     5.0       5.0      24.0   1.44
mu        0.109  0.023   0.063    0.143  ...    12.0      13.0      17.0   1.11
mus       0.166  0.026   0.123    0.225  ...   111.0     152.0      88.0   1.00
gamma     0.217  0.042   0.128    0.287  ...    80.0      62.0      80.0   1.05
Is_begin  1.490  1.102   0.033    3.349  ...   136.0      96.0      57.0   0.99
Ia_begin  0.717  0.550   0.011    1.609  ...    21.0      13.0      22.0   1.12
E_begin   1.362  1.331   0.037    4.038  ...    97.0     101.0      96.0   1.00

[8 rows x 11 columns]