0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -627.92    26.89
p_loo       21.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.056   0.155    0.337  ...    82.0      90.0      44.0   1.06
pu        0.761  0.024   0.724    0.807  ...    63.0      62.0     100.0   1.02
mu        0.123  0.027   0.084    0.171  ...    77.0      67.0      40.0   0.99
mus       0.173  0.036   0.110    0.241  ...   118.0     140.0      60.0   0.99
gamma     0.200  0.040   0.137    0.272  ...   152.0     152.0      58.0   1.02
Is_begin  0.582  0.499   0.013    1.606  ...    29.0      26.0     100.0   1.06
Ia_begin  1.034  0.997   0.037    2.927  ...    28.0      14.0      39.0   1.12
E_begin   0.511  0.625   0.006    1.392  ...    79.0      40.0      60.0   1.04

[8 rows x 11 columns]