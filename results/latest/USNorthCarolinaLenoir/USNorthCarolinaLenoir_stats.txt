0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -935.88    28.81
p_loo       21.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      284   96.3%
 (0.5, 0.7]   (ok)          6    2.0%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.053   0.160    0.328  ...   152.0     152.0     100.0   0.98
pu        0.782  0.043   0.701    0.843  ...    47.0      47.0      38.0   1.02
mu        0.112  0.021   0.077    0.152  ...    43.0      44.0      43.0   1.09
mus       0.157  0.029   0.112    0.206  ...   152.0     152.0      66.0   1.03
gamma     0.174  0.042   0.096    0.254  ...   152.0     152.0      43.0   1.01
Is_begin  0.907  0.807   0.013    2.414  ...   131.0     120.0      60.0   1.01
Ia_begin  1.627  1.528   0.066    4.486  ...    97.0      77.0      54.0   0.99
E_begin   1.062  1.097   0.022    2.930  ...    84.0      99.0      57.0   0.98

[8 rows x 11 columns]