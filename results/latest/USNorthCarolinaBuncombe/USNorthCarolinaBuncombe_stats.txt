0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1198.02    40.12
p_loo       27.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      262   88.8%
 (0.5, 0.7]   (ok)         28    9.5%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.044   0.188    0.343  ...    68.0      67.0      43.0   1.02
pu        0.851  0.046   0.755    0.899  ...    21.0      23.0      65.0   1.07
mu        0.114  0.030   0.072    0.164  ...     7.0       7.0      22.0   1.24
mus       0.149  0.023   0.107    0.188  ...    54.0      54.0      91.0   1.04
gamma     0.175  0.035   0.127    0.245  ...   111.0     152.0      60.0   1.00
Is_begin  0.685  0.694   0.004    2.084  ...   121.0      98.0      20.0   1.01
Ia_begin  0.623  0.515   0.020    1.667  ...   149.0      94.0      59.0   1.01
E_begin   0.503  0.632   0.005    1.595  ...   117.0      59.0     100.0   1.03

[8 rows x 11 columns]