0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1148.16    34.83
p_loo       31.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.053   0.156    0.337  ...    58.0      53.0      38.0   1.03
pu        0.767  0.026   0.723    0.810  ...    20.0      32.0      72.0   1.06
mu        0.133  0.024   0.087    0.169  ...     8.0       9.0      40.0   1.19
mus       0.224  0.039   0.162    0.297  ...    68.0      68.0      60.0   1.01
gamma     0.287  0.053   0.190    0.380  ...    59.0      65.0      33.0   1.09
Is_begin  0.905  0.789   0.006    2.724  ...   114.0      99.0      57.0   1.02
Ia_begin  1.386  1.019   0.097    3.610  ...    39.0      37.0      43.0   1.02
E_begin   0.695  0.585   0.013    1.839  ...    33.0      31.0      38.0   1.02

[8 rows x 11 columns]