0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -847.92    28.63
p_loo       27.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.052   0.183    0.346  ...   129.0     121.0      54.0   1.01
pu        0.845  0.029   0.790    0.890  ...    28.0      31.0      59.0   1.05
mu        0.112  0.021   0.072    0.149  ...     5.0       5.0      60.0   1.38
mus       0.160  0.032   0.102    0.211  ...   135.0     123.0      35.0   1.03
gamma     0.189  0.035   0.115    0.239  ...    70.0      72.0      91.0   1.01
Is_begin  0.511  0.666   0.016    1.667  ...    99.0      74.0      59.0   1.01
Ia_begin  1.116  1.449   0.006    4.587  ...   115.0     118.0      96.0   1.04
E_begin   0.481  0.640   0.000    1.951  ...    92.0      71.0     100.0   1.04

[8 rows x 11 columns]