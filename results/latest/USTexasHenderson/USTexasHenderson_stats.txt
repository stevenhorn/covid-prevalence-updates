0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1130.90    53.38
p_loo       32.08        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      283   95.9%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.051   0.167    0.348  ...   125.0     129.0     100.0   1.00
pu        0.878  0.017   0.839    0.900  ...    36.0      28.0      15.0   1.04
mu        0.147  0.021   0.119    0.194  ...     8.0       9.0      16.0   1.18
mus       0.202  0.033   0.145    0.265  ...   116.0     110.0      58.0   1.10
gamma     0.288  0.051   0.209    0.373  ...   119.0     114.0      44.0   1.02
Is_begin  0.836  0.902   0.000    2.613  ...   119.0      53.0      29.0   1.04
Ia_begin  1.930  2.222   0.019    6.970  ...    69.0      77.0      40.0   1.01
E_begin   0.791  0.847   0.002    2.193  ...    69.0      55.0      43.0   1.00

[8 rows x 11 columns]