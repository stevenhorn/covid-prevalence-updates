0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -636.82    59.80
p_loo       70.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.224  0.043   0.154    0.291  ...    87.0      70.0      40.0   1.01
pu        0.733  0.019   0.705    0.776  ...   104.0     111.0      72.0   1.00
mu        0.158  0.026   0.114    0.207  ...   124.0     127.0      91.0   1.02
mus       0.245  0.041   0.182    0.325  ...    17.0      17.0      53.0   1.10
gamma     0.343  0.061   0.254    0.449  ...    27.0      33.0      60.0   1.07
Is_begin  0.495  0.530   0.010    1.868  ...    94.0      69.0      93.0   0.99
Ia_begin  0.652  0.745   0.006    2.241  ...    78.0      76.0      76.0   1.00
E_begin   0.393  0.491   0.002    1.123  ...    39.0      67.0      16.0   1.00

[8 rows x 11 columns]