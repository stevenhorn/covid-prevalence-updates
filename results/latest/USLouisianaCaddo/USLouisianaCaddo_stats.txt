0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1555.12    32.64
p_loo       27.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.227   0.052   0.153    0.317  ...    65.0      81.0      58.0   1.01
pu         0.732   0.025   0.701    0.772  ...    89.0      83.0      39.0   1.00
mu         0.111   0.020   0.078    0.145  ...    27.0      23.0      40.0   1.14
mus        0.197   0.032   0.145    0.261  ...    95.0      89.0      88.0   0.99
gamma      0.297   0.049   0.216    0.384  ...   137.0     133.0      59.0   0.99
Is_begin   4.626   3.074   0.453    9.829  ...   152.0     124.0      24.0   0.99
Ia_begin  12.250   5.870   3.963   22.598  ...   147.0     114.0      75.0   1.00
E_begin   17.463  13.475   0.519   41.225  ...    99.0      80.0      57.0   1.02

[8 rows x 11 columns]