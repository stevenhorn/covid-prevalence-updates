0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -208.31    26.69
p_loo       29.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.059   0.155    0.342  ...    77.0      81.0      41.0   1.00
pu        0.851  0.022   0.814    0.893  ...    61.0      63.0      58.0   0.99
mu        0.140  0.031   0.093    0.194  ...    29.0      29.0      31.0   1.05
mus       0.170  0.025   0.127    0.213  ...   152.0     142.0      86.0   1.01
gamma     0.209  0.048   0.132    0.287  ...   107.0     152.0      73.0   1.11
Is_begin  0.515  0.584   0.016    1.603  ...   120.0      74.0      54.0   1.02
Ia_begin  0.917  1.068   0.004    2.670  ...    54.0      93.0      55.0   1.03
E_begin   0.477  0.522   0.005    1.588  ...   127.0     101.0      93.0   1.02

[8 rows x 11 columns]