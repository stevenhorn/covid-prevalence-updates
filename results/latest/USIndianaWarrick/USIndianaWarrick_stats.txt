0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -922.95    22.31
p_loo       18.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.051   0.151    0.324  ...    57.0      58.0      49.0   1.01
pu        0.779  0.029   0.726    0.837  ...    46.0      47.0      43.0   1.00
mu        0.129  0.021   0.093    0.159  ...     9.0       9.0      40.0   1.20
mus       0.171  0.031   0.117    0.231  ...    59.0      68.0      19.0   1.03
gamma     0.233  0.044   0.169    0.320  ...    88.0      80.0      60.0   1.01
Is_begin  1.042  0.869   0.012    2.793  ...    70.0      44.0      22.0   1.03
Ia_begin  2.905  2.399   0.068    7.833  ...    84.0      45.0      22.0   1.04
E_begin   1.846  1.782   0.023    6.264  ...   101.0      69.0      39.0   1.01

[8 rows x 11 columns]