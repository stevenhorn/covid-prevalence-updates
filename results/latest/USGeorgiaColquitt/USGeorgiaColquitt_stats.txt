0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1149.74    49.22
p_loo       34.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.052   0.178    0.342  ...    74.0      81.0      83.0   1.03
pu        0.762  0.033   0.705    0.811  ...    55.0      55.0      59.0   1.00
mu        0.135  0.021   0.103    0.169  ...     6.0       6.0      80.0   1.34
mus       0.233  0.042   0.161    0.315  ...   134.0     118.0      47.0   1.13
gamma     0.320  0.056   0.219    0.405  ...   112.0     109.0      93.0   0.99
Is_begin  1.073  0.952   0.057    2.442  ...    91.0      69.0      39.0   1.03
Ia_begin  2.526  2.602   0.003    7.088  ...    55.0      45.0      22.0   1.08
E_begin   1.354  1.620   0.011    3.707  ...    67.0      58.0     100.0   1.03

[8 rows x 11 columns]