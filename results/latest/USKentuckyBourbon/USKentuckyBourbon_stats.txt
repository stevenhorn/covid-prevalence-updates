0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -659.82    29.40
p_loo       24.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.054   0.154    0.335  ...   102.0      88.0      40.0   0.98
pu        0.824  0.029   0.779    0.875  ...    65.0      65.0      60.0   1.07
mu        0.118  0.026   0.070    0.156  ...     7.0       7.0      42.0   1.27
mus       0.155  0.034   0.102    0.214  ...   131.0     152.0     100.0   1.00
gamma     0.170  0.027   0.120    0.219  ...    51.0      91.0      76.0   1.02
Is_begin  0.899  0.784   0.025    1.984  ...    88.0     145.0      61.0   1.01
Ia_begin  0.681  0.519   0.009    1.720  ...   121.0      96.0      57.0   1.02
E_begin   0.523  0.503   0.022    1.586  ...   105.0      92.0      87.0   1.01

[8 rows x 11 columns]