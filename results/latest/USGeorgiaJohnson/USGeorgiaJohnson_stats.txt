0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -787.05    50.51
p_loo       38.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.052   0.153    0.323  ...   152.0     152.0      59.0   1.02
pu        0.742  0.025   0.705    0.788  ...    80.0      58.0      46.0   1.02
mu        0.153  0.026   0.115    0.208  ...    11.0      12.0      58.0   1.14
mus       0.213  0.042   0.142    0.271  ...    73.0      83.0      19.0   1.01
gamma     0.304  0.070   0.191    0.431  ...   134.0     152.0      60.0   1.01
Is_begin  0.886  0.697   0.000    2.272  ...    97.0     118.0      40.0   1.01
Ia_begin  1.748  1.561   0.091    5.198  ...   109.0      85.0      93.0   0.98
E_begin   1.056  1.104   0.035    2.968  ...   112.0     128.0      61.0   1.00

[8 rows x 11 columns]