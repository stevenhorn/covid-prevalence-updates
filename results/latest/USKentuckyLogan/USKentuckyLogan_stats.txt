0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -859.31    37.16
p_loo       30.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.056   0.151    0.324  ...    55.0      53.0      64.0   1.01
pu        0.813  0.042   0.712    0.864  ...     9.0      13.0      26.0   1.12
mu        0.118  0.020   0.085    0.152  ...     9.0       9.0      40.0   1.22
mus       0.156  0.022   0.125    0.208  ...    94.0      94.0      74.0   1.01
gamma     0.181  0.044   0.119    0.265  ...    65.0      99.0      60.0   1.04
Is_begin  0.617  0.454   0.015    1.511  ...    49.0      44.0      40.0   1.02
Ia_begin  1.269  1.075   0.015    2.906  ...    67.0      59.0      40.0   1.06
E_begin   0.751  0.665   0.021    2.166  ...    90.0      66.0      27.0   1.02

[8 rows x 11 columns]