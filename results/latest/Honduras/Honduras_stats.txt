0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -1929.24    22.00
p_loo       15.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.6%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.266  0.050   0.166    0.334  ...    94.0     109.0      93.0   1.03
pu        0.838  0.050   0.744    0.898  ...   152.0     152.0      80.0   1.00
mu        0.110  0.022   0.063    0.148  ...     9.0       9.0      14.0   1.22
mus       0.157  0.033   0.103    0.219  ...   148.0     152.0      60.0   1.00
gamma     0.179  0.039   0.118    0.263  ...   152.0     152.0      59.0   1.07
Is_begin  3.329  2.711   0.099    7.762  ...   152.0     152.0      69.0   0.99
Ia_begin  6.611  5.715   0.179   16.034  ...    82.0      87.0      88.0   1.00
E_begin   5.022  5.458   0.018   15.926  ...    90.0      99.0      59.0   1.02

[8 rows x 11 columns]