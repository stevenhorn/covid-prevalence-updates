0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo  -996.39    19.60
p_loo       20.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.6%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.053   0.171    0.339  ...   105.0     116.0      59.0   1.07
pu        0.816  0.055   0.736    0.899  ...    83.0      80.0      59.0   1.03
mu        0.122  0.015   0.098    0.150  ...    45.0      49.0      60.0   1.00
mus       0.173  0.036   0.106    0.222  ...   148.0     152.0      88.0   0.98
gamma     0.197  0.044   0.131    0.302  ...   152.0     152.0      91.0   1.00
Is_begin  1.611  1.112   0.026    3.173  ...   118.0     145.0      60.0   0.99
Ia_begin  2.998  2.701   0.022    8.142  ...    58.0      27.0      56.0   1.08
E_begin   2.172  2.110   0.030    6.283  ...    71.0      65.0      91.0   1.01

[8 rows x 11 columns]