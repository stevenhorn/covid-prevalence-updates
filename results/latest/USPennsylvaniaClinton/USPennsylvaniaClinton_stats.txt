0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -742.54    38.05
p_loo       29.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.283  0.047   0.203    0.350  ...    64.0      63.0      57.0   1.04
pu        0.889  0.008   0.874    0.900  ...    33.0      22.0      60.0   1.08
mu        0.145  0.032   0.105    0.220  ...    17.0      17.0      60.0   1.07
mus       0.153  0.024   0.112    0.202  ...   107.0     134.0      59.0   1.00
gamma     0.164  0.041   0.112    0.261  ...    63.0      68.0      45.0   1.03
Is_begin  0.959  0.920   0.007    2.301  ...   121.0     100.0      59.0   1.01
Ia_begin  2.312  2.010   0.053    6.658  ...   101.0      84.0      59.0   1.02
E_begin   1.262  1.023   0.021    3.239  ...    82.0      93.0      86.0   1.00

[8 rows x 11 columns]