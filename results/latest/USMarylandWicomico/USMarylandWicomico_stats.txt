0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1023.86    38.02
p_loo       30.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.048   0.187    0.342  ...    72.0      69.0      60.0   1.03
pu        0.838  0.048   0.757    0.897  ...    43.0      33.0      15.0   1.04
mu        0.131  0.030   0.076    0.180  ...    21.0      22.0      54.0   1.08
mus       0.186  0.036   0.137    0.270  ...   152.0     152.0      91.0   1.03
gamma     0.250  0.042   0.181    0.325  ...   152.0     152.0      99.0   0.99
Is_begin  1.024  0.770   0.036    2.349  ...    64.0      64.0      60.0   1.05
Ia_begin  0.684  0.549   0.002    1.902  ...    83.0      65.0      19.0   1.03
E_begin   0.889  0.771   0.048    2.496  ...    68.0      66.0      96.0   1.02

[8 rows x 11 columns]