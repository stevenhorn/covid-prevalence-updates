0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -740.42    24.69
p_loo       21.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.275  0.054   0.185    0.349  ...    67.0      63.0      60.0   0.99
pu        0.839  0.029   0.789    0.888  ...    58.0      59.0      51.0   1.06
mu        0.121  0.023   0.087    0.168  ...    15.0      13.0      40.0   1.12
mus       0.155  0.026   0.104    0.199  ...   118.0     119.0      88.0   1.01
gamma     0.167  0.040   0.109    0.237  ...   133.0     152.0      74.0   1.06
Is_begin  0.743  0.601   0.015    1.967  ...    47.0      63.0      60.0   1.03
Ia_begin  1.230  0.996   0.038    2.822  ...    46.0      38.0      97.0   1.03
E_begin   0.708  0.575   0.005    1.845  ...    55.0      29.0      17.0   1.05

[8 rows x 11 columns]