0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1743.24    28.79
p_loo       25.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      266   90.2%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.055   0.152    0.340  ...   101.0     102.0      44.0   1.01
pu        0.753  0.039   0.700    0.818  ...    67.0      58.0      40.0   1.04
mu        0.120  0.026   0.086    0.178  ...    45.0      44.0      48.0   1.02
mus       0.194  0.035   0.133    0.257  ...   152.0     152.0      80.0   1.05
gamma     0.265  0.049   0.196    0.356  ...   141.0     152.0      96.0   1.01
Is_begin  1.768  1.297   0.019    4.291  ...    69.0      26.0      57.0   1.07
Ia_begin  4.287  2.548   0.165    8.299  ...    25.0      18.0      36.0   1.11
E_begin   3.901  3.169   0.028    9.749  ...    76.0      65.0      58.0   1.05

[8 rows x 11 columns]