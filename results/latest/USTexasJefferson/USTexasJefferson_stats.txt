10 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1445.36    38.71
p_loo       35.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      263   89.2%
 (0.5, 0.7]   (ok)         27    9.2%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.266  0.062   0.153    0.339  ...     3.0       4.0      32.0   1.81
pu        0.831  0.021   0.795    0.865  ...     4.0       5.0      21.0   1.48
mu        0.160  0.020   0.133    0.205  ...     3.0       3.0      24.0   2.21
mus       0.153  0.028   0.116    0.216  ...    25.0      29.0      45.0   1.04
gamma     0.168  0.026   0.112    0.209  ...     7.0       8.0      18.0   1.21
Is_begin  1.420  1.320   0.045    4.234  ...    16.0      13.0      48.0   1.12
Ia_begin  2.985  2.432   0.044    7.379  ...     5.0       4.0      23.0   1.64
E_begin   1.939  1.584   0.132    5.323  ...    10.0      10.0      42.0   1.21

[8 rows x 11 columns]