0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -658.36    34.29
p_loo       25.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      285   96.6%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.045   0.184    0.331  ...    58.0      57.0      60.0   1.03
pu        0.800  0.024   0.761    0.838  ...    26.0      27.0      59.0   1.04
mu        0.131  0.021   0.100    0.174  ...    12.0      12.0      38.0   1.16
mus       0.229  0.039   0.166    0.302  ...    34.0      46.0      22.0   1.04
gamma     0.332  0.052   0.245    0.448  ...    80.0     111.0      43.0   1.02
Is_begin  0.719  0.686   0.065    2.351  ...    92.0     105.0      61.0   1.00
Ia_begin  1.261  1.466   0.005    4.471  ...    50.0      56.0      44.0   1.03
E_begin   0.554  0.557   0.000    1.566  ...    73.0      47.0      29.0   1.02

[8 rows x 11 columns]