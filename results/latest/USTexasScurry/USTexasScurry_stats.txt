0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1066.82    61.05
p_loo       33.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      286   96.9%
 (0.5, 0.7]   (ok)          6    2.0%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.169  0.010   0.153    0.191  ...    22.0      16.0      39.0   1.11
pu        0.716  0.006   0.707    0.729  ...    17.0      16.0      54.0   1.13
mu        0.193  0.022   0.164    0.233  ...     3.0       3.0      15.0   2.00
mus       0.197  0.017   0.168    0.230  ...    10.0      11.0      19.0   1.14
gamma     0.218  0.036   0.167    0.283  ...     4.0       4.0      36.0   1.63
Is_begin  0.488  0.509   0.037    1.611  ...     5.0       4.0      32.0   1.80
Ia_begin  0.579  0.644   0.011    2.149  ...     6.0       4.0      15.0   1.72
E_begin   0.279  0.360   0.004    1.091  ...     6.0       6.0      22.0   1.37

[8 rows x 11 columns]