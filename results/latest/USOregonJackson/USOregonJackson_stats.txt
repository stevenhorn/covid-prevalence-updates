3 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -975.45    30.34
p_loo       22.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.278  0.049   0.189    0.346  ...   152.0     152.0      46.0   1.07
pu        0.874  0.031   0.829    0.899  ...    34.0      74.0      60.0   1.08
mu        0.135  0.027   0.094    0.185  ...    12.0      19.0      40.0   1.14
mus       0.164  0.040   0.101    0.240  ...    65.0      92.0      69.0   1.02
gamma     0.199  0.050   0.113    0.287  ...   141.0     127.0      91.0   1.02
Is_begin  1.435  1.073   0.018    3.363  ...   135.0      88.0      60.0   1.03
Ia_begin  3.552  2.247   0.218    7.759  ...    61.0      57.0      58.0   0.99
E_begin   2.285  1.975   0.009    6.101  ...    68.0      60.0      60.0   0.98

[8 rows x 11 columns]