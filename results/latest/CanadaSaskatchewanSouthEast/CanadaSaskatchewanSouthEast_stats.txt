0 Divergences 
Passed validation 
Computed from 80 by 166 log-likelihood matrix

         Estimate       SE
elpd_loo  -393.01    17.70
p_loo       16.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      146   88.0%
 (0.5, 0.7]   (ok)         17   10.2%
   (0.7, 1]   (bad)         1    0.6%
   (1, Inf)   (very bad)    2    1.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.276  0.040   0.204    0.346  ...    66.0      51.0      31.0   1.03
pu        0.880  0.019   0.836    0.899  ...   140.0     115.0      59.0   1.02
mu        0.130  0.019   0.098    0.167  ...    39.0      38.0      60.0   1.03
mus       0.176  0.036   0.106    0.231  ...    66.0      76.0      91.0   1.00
gamma     0.215  0.039   0.147    0.289  ...   152.0     132.0      59.0   1.02
Is_begin  0.956  1.092   0.017    3.006  ...    89.0      55.0      87.0   1.00
Ia_begin  1.076  0.979   0.004    3.102  ...   113.0      82.0      57.0   1.02
E_begin   0.640  0.640   0.003    1.974  ...    99.0      64.0      60.0   1.02

[8 rows x 11 columns]