0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -962.71    31.72
p_loo       27.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.043   0.169    0.329  ...   152.0     152.0      44.0   1.02
pu        0.836  0.031   0.781    0.891  ...    58.0      62.0      60.0   1.02
mu        0.116  0.019   0.082    0.143  ...    53.0      55.0      43.0   1.01
mus       0.157  0.034   0.093    0.217  ...   152.0     152.0      67.0   1.00
gamma     0.174  0.042   0.109    0.242  ...    77.0      68.0      80.0   1.02
Is_begin  0.637  0.628   0.000    1.792  ...   105.0      74.0      14.0   1.01
Ia_begin  1.453  1.513   0.023    4.410  ...   119.0     152.0      59.0   1.01
E_begin   0.746  0.805   0.013    2.399  ...    97.0     116.0     100.0   1.02

[8 rows x 11 columns]