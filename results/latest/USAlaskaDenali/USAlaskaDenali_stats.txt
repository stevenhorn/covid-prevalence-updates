0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -225.67    40.91
p_loo       33.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.234  0.049   0.151    0.316  ...   131.0     152.0      40.0   1.04
pu        0.755  0.034   0.703    0.805  ...    66.0      70.0      39.0   1.03
mu        0.131  0.025   0.093    0.186  ...    59.0      58.0      56.0   1.03
mus       0.216  0.040   0.154    0.299  ...   152.0     152.0      75.0   0.99
gamma     0.235  0.040   0.178    0.306  ...   119.0     146.0      59.0   1.00
Is_begin  0.296  0.333   0.000    1.088  ...    62.0      66.0      46.0   1.00
Ia_begin  0.515  0.667   0.000    2.276  ...    86.0      65.0      96.0   1.02
E_begin   0.264  0.373   0.001    1.158  ...    86.0      71.0      87.0   1.03

[8 rows x 11 columns]