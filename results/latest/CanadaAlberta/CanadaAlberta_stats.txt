0 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo -1986.65    32.56
p_loo       25.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   90.3%
 (0.5, 0.7]   (ok)         23    7.4%
   (0.7, 1]   (bad)         6    1.9%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.250   0.055   0.150    0.335  ...    64.0      62.0      32.0   1.14
pu         0.773   0.058   0.700    0.872  ...    90.0      68.0      22.0   1.00
mu         0.136   0.024   0.094    0.177  ...    21.0      20.0      36.0   1.08
mus        0.189   0.033   0.146    0.260  ...    99.0     128.0      86.0   1.00
gamma      0.268   0.052   0.185    0.361  ...    85.0      79.0      97.0   1.01
Is_begin  24.847  19.613   1.451   53.968  ...    68.0      50.0      48.0   1.01
Ia_begin  64.787  36.268   2.494  119.016  ...    36.0      34.0      57.0   1.06
E_begin   49.987  46.085   0.070  145.730  ...    42.0      26.0      38.0   1.07

[8 rows x 11 columns]