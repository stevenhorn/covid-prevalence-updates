0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -415.29    32.04
p_loo       23.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.045   0.183    0.341  ...    60.0      74.0      38.0   1.02
pu        0.882  0.010   0.865    0.898  ...    33.0      41.0     100.0   1.04
mu        0.128  0.019   0.098    0.168  ...    60.0      63.0      59.0   1.04
mus       0.157  0.028   0.115    0.212  ...    65.0      62.0      95.0   1.02
gamma     0.196  0.041   0.136    0.276  ...    44.0      55.0      60.0   1.01
Is_begin  0.523  0.473   0.018    1.610  ...   107.0      93.0      96.0   1.01
Ia_begin  1.015  1.801   0.012    3.442  ...    68.0      84.0      60.0   1.09
E_begin   0.480  0.690   0.004    1.424  ...    46.0      29.0      33.0   1.03

[8 rows x 11 columns]