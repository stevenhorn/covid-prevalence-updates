0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1027.08    30.58
p_loo       26.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.055   0.163    0.349  ...    59.0      56.0      43.0   1.03
pu        0.836  0.036   0.777    0.898  ...    23.0      21.0      22.0   1.09
mu        0.109  0.024   0.069    0.151  ...    13.0      11.0      34.0   1.15
mus       0.147  0.032   0.101    0.207  ...    37.0      44.0     100.0   1.05
gamma     0.151  0.034   0.105    0.233  ...   152.0     152.0     100.0   0.98
Is_begin  0.586  0.727   0.001    2.412  ...    89.0      40.0      39.0   1.06
Ia_begin  1.465  1.409   0.018    4.025  ...    62.0      36.0      60.0   1.05
E_begin   0.612  0.720   0.006    1.987  ...    43.0      21.0      46.0   1.08

[8 rows x 11 columns]