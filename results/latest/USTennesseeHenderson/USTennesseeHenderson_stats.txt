0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -894.67    25.77
p_loo       22.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.057   0.153    0.341  ...    55.0      55.0      55.0   1.03
pu        0.795  0.023   0.753    0.834  ...    22.0      23.0      22.0   1.07
mu        0.128  0.018   0.093    0.162  ...    32.0      32.0      37.0   1.04
mus       0.156  0.028   0.104    0.209  ...    89.0     102.0      46.0   1.10
gamma     0.189  0.039   0.131    0.264  ...    66.0      54.0      59.0   1.02
Is_begin  0.407  0.444   0.004    1.369  ...    58.0      48.0      43.0   1.03
Ia_begin  0.706  0.627   0.000    1.908  ...    55.0      40.0      29.0   1.01
E_begin   0.391  0.414   0.014    1.080  ...    47.0      33.0      59.0   1.07

[8 rows x 11 columns]