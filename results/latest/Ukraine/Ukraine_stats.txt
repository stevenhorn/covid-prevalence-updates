1 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -2021.73    29.23
p_loo       30.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      256   86.5%
 (0.5, 0.7]   (ok)         31   10.5%
   (0.7, 1]   (bad)         9    3.0%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.221   0.048   0.152    0.315  ...   128.0     138.0      60.0   1.00
pu         0.741   0.034   0.703    0.818  ...     4.0       4.0      14.0   1.61
mu         0.105   0.028   0.067    0.163  ...     7.0       9.0      36.0   1.23
mus        0.154   0.029   0.116    0.206  ...    39.0      65.0      87.0   1.04
gamma      0.272   0.045   0.191    0.356  ...    66.0      73.0      37.0   1.02
Is_begin   8.116   5.336   0.802   17.798  ...     5.0       5.0      33.0   1.40
Ia_begin  26.706   9.296   9.242   43.900  ...    32.0      27.0      53.0   1.11
E_begin   56.234  29.638  12.213  108.728  ...    44.0      42.0      60.0   1.03

[8 rows x 11 columns]