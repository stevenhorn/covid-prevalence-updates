0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -874.09    24.94
p_loo       21.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.045   0.166    0.331  ...    41.0      42.0      22.0   1.03
pu        0.826  0.024   0.782    0.864  ...    23.0      24.0      40.0   1.08
mu        0.131  0.023   0.096    0.190  ...    28.0      28.0      40.0   1.01
mus       0.159  0.029   0.109    0.210  ...    32.0      35.0      76.0   1.05
gamma     0.181  0.040   0.117    0.250  ...   124.0     142.0      49.0   0.99
Is_begin  0.704  0.571   0.052    1.985  ...   110.0      60.0      56.0   1.07
Ia_begin  1.664  1.336   0.032    4.795  ...    61.0      65.0      43.0   1.00
E_begin   0.778  0.765   0.029    2.197  ...    51.0      40.0      43.0   1.04

[8 rows x 11 columns]