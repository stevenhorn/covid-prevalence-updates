0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1044.31    26.85
p_loo       22.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.056   0.172    0.349  ...    38.0      39.0      40.0   1.04
pu        0.792  0.040   0.716    0.855  ...    30.0      32.0      22.0   1.13
mu        0.101  0.019   0.068    0.132  ...     9.0      10.0      40.0   1.17
mus       0.140  0.024   0.101    0.187  ...    42.0      40.0      49.0   1.05
gamma     0.166  0.034   0.100    0.226  ...   152.0     152.0      21.0   1.04
Is_begin  0.879  0.751   0.058    2.769  ...    87.0      72.0      91.0   0.99
Ia_begin  1.580  1.674   0.000    4.015  ...    21.0      10.0      15.0   1.13
E_begin   0.862  1.049   0.030    2.703  ...    55.0      46.0      60.0   1.03

[8 rows x 11 columns]