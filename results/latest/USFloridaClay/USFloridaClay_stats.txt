0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1245.72    30.09
p_loo       30.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.058   0.166    0.350  ...    19.0      30.0      22.0   1.14
pu        0.762  0.041   0.701    0.829  ...    32.0      28.0      43.0   1.06
mu        0.133  0.022   0.103    0.191  ...    50.0      44.0      59.0   1.02
mus       0.188  0.034   0.126    0.251  ...    45.0      41.0      41.0   1.00
gamma     0.271  0.037   0.205    0.350  ...   112.0     130.0      77.0   1.08
Is_begin  0.778  0.544   0.001    1.809  ...    87.0      62.0      38.0   1.03
Ia_begin  1.765  1.005   0.039    3.363  ...    92.0     111.0      65.0   0.99
E_begin   1.651  1.633   0.016    4.871  ...   111.0      82.0      60.0   1.00

[8 rows x 11 columns]