0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -298.71    31.89
p_loo       27.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.057   0.150    0.332  ...    46.0      52.0      33.0   1.06
pu        0.812  0.020   0.780    0.849  ...    41.0      42.0      97.0   1.03
mu        0.118  0.022   0.087    0.158  ...    66.0      67.0      72.0   1.02
mus       0.180  0.026   0.141    0.226  ...   123.0     108.0     100.0   1.00
gamma     0.231  0.043   0.164    0.306  ...    99.0     100.0      39.0   0.99
Is_begin  0.507  0.466   0.017    1.406  ...    89.0      96.0      54.0   1.00
Ia_begin  0.639  0.754   0.002    1.850  ...   112.0     129.0      83.0   0.98
E_begin   0.360  0.375   0.007    1.071  ...   115.0      88.0      38.0   1.03

[8 rows x 11 columns]