0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1320.40    58.35
p_loo       36.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.063   0.162    0.349  ...    52.0      52.0      46.0   1.16
pu        0.804  0.038   0.733    0.870  ...    70.0      72.0      57.0   0.99
mu        0.137  0.022   0.102    0.181  ...    54.0      54.0      81.0   1.00
mus       0.222  0.038   0.168    0.292  ...   152.0     152.0      61.0   1.03
gamma     0.338  0.060   0.213    0.438  ...    79.0      79.0      76.0   1.00
Is_begin  0.579  0.544   0.017    1.862  ...    72.0      29.0      53.0   1.06
Ia_begin  1.682  1.445   0.051    4.517  ...    90.0      77.0      59.0   1.03
E_begin   0.650  0.720   0.011    1.990  ...   103.0      49.0      60.0   1.00

[8 rows x 11 columns]