0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1149.01    43.56
p_loo       33.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.057   0.161    0.348  ...    51.0      57.0      60.0   0.99
pu        0.858  0.019   0.817    0.885  ...    51.0      54.0      40.0   1.01
mu        0.159  0.023   0.126    0.204  ...    46.0      44.0      58.0   1.00
mus       0.162  0.033   0.105    0.212  ...    58.0      59.0      54.0   1.02
gamma     0.208  0.049   0.119    0.293  ...    12.0      12.0      22.0   1.12
Is_begin  0.726  0.515   0.057    1.694  ...    70.0      70.0      95.0   1.03
Ia_begin  1.812  1.814   0.039    5.153  ...    64.0      77.0      30.0   1.00
E_begin   0.687  0.796   0.003    2.097  ...    44.0      86.0      18.0   1.02

[8 rows x 11 columns]