4 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1608.47    40.03
p_loo       32.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.058   0.158    0.341  ...   152.0     152.0      55.0   1.03
pu        0.755  0.044   0.701    0.835  ...    67.0      59.0      36.0   1.10
mu        0.109  0.022   0.076    0.155  ...    32.0      37.0      83.0   1.09
mus       0.185  0.031   0.128    0.238  ...    92.0      87.0      60.0   0.99
gamma     0.258  0.046   0.189    0.356  ...    17.0      17.0      88.0   1.11
Is_begin  1.850  1.258   0.082    4.128  ...    42.0      33.0      40.0   1.02
Ia_begin  3.833  2.826   0.009    8.846  ...     8.0       7.0      16.0   1.24
E_begin   3.687  3.708   0.001   10.219  ...    18.0      11.0      40.0   1.17

[8 rows x 11 columns]