0 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo -1586.50    37.52
p_loo       28.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      287   92.9%
 (0.5, 0.7]   (ok)         17    5.5%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.6%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.250   0.048   0.167    0.324  ...    59.0      56.0      35.0   1.02
pu         0.776   0.051   0.701    0.868  ...    76.0      49.0      38.0   1.04
mu         0.124   0.020   0.096    0.158  ...     8.0       8.0      52.0   1.26
mus        0.190   0.028   0.147    0.240  ...   133.0     135.0      60.0   0.99
gamma      0.268   0.045   0.196    0.364  ...   152.0     140.0     100.0   0.99
Is_begin   7.950   6.904   0.126   21.326  ...    65.0      14.0      19.0   1.10
Ia_begin  59.514  25.603  10.501  100.643  ...    81.0      84.0      60.0   0.99
E_begin   48.979  32.261   0.625  116.880  ...    76.0      66.0      39.0   1.03

[8 rows x 11 columns]