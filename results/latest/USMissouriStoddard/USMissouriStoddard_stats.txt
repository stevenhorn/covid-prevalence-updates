0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -856.81    29.69
p_loo       25.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.051   0.179    0.345  ...    55.0      60.0      99.0   1.00
pu        0.874  0.016   0.846    0.896  ...    21.0      16.0      40.0   1.10
mu        0.113  0.021   0.078    0.142  ...    15.0      15.0      19.0   1.09
mus       0.155  0.030   0.106    0.200  ...    75.0      70.0     100.0   1.00
gamma     0.169  0.027   0.122    0.219  ...    18.0      17.0      80.0   1.09
Is_begin  0.972  0.877   0.028    2.425  ...   101.0      71.0     100.0   1.00
Ia_begin  2.299  2.888   0.007    8.492  ...   117.0      84.0      40.0   1.03
E_begin   0.855  0.954   0.004    2.871  ...    41.0      10.0      57.0   1.18

[8 rows x 11 columns]