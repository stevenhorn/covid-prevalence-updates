0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -846.73    34.31
p_loo       21.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.059   0.153    0.330  ...   140.0     123.0      60.0   0.99
pu        0.825  0.025   0.779    0.858  ...    53.0      71.0      22.0   1.00
mu        0.131  0.030   0.083    0.184  ...     9.0       8.0      69.0   1.27
mus       0.154  0.034   0.097    0.211  ...    83.0      88.0      60.0   1.02
gamma     0.169  0.037   0.106    0.244  ...    26.0      18.0      24.0   1.09
Is_begin  1.011  0.974   0.012    2.448  ...   116.0     103.0      86.0   1.00
Ia_begin  2.232  2.069   0.123    5.708  ...    95.0      88.0      39.0   1.02
E_begin   1.021  1.074   0.019    3.076  ...    62.0      58.0      42.0   1.05

[8 rows x 11 columns]