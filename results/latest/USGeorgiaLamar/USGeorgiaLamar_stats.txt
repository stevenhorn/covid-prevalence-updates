0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -794.69    55.72
p_loo       31.31        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      281   95.3%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.050   0.169    0.337  ...    84.0      84.0      55.0   1.01
pu        0.793  0.036   0.720    0.845  ...    14.0      18.0      44.0   1.09
mu        0.153  0.028   0.100    0.200  ...     7.0       7.0      49.0   1.27
mus       0.227  0.036   0.154    0.278  ...    15.0      14.0      56.0   1.13
gamma     0.311  0.046   0.224    0.383  ...    71.0      78.0      88.0   1.02
Is_begin  0.858  0.794   0.004    2.390  ...    73.0      55.0      40.0   1.00
Ia_begin  2.169  2.356   0.077    7.297  ...   102.0      93.0      48.0   1.00
E_begin   0.875  0.866   0.035    2.549  ...   119.0     110.0      83.0   1.00

[8 rows x 11 columns]