0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -693.00    19.33
p_loo       18.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.273  0.045   0.192    0.348  ...    45.0      41.0      32.0   1.05
pu        0.852  0.040   0.771    0.899  ...    33.0      40.0      39.0   0.99
mu        0.111  0.020   0.076    0.156  ...    10.0      10.0      14.0   1.14
mus       0.141  0.031   0.084    0.204  ...    68.0      54.0      61.0   1.04
gamma     0.157  0.030   0.093    0.203  ...    66.0      68.0      14.0   1.01
Is_begin  0.567  0.741   0.003    2.353  ...    66.0      41.0      59.0   1.03
Ia_begin  1.409  1.488   0.011    4.499  ...    46.0      32.0      60.0   1.06
E_begin   0.725  0.924   0.001    3.082  ...    52.0      51.0      40.0   1.04

[8 rows x 11 columns]