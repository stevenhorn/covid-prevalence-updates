0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1359.47    33.18
p_loo       27.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.052   0.166    0.350  ...   152.0     152.0      66.0   1.01
pu        0.784  0.053   0.713    0.886  ...    57.0      59.0      60.0   1.02
mu        0.119  0.016   0.091    0.150  ...    33.0      33.0      53.0   1.02
mus       0.172  0.035   0.119    0.240  ...   108.0     147.0      59.0   0.99
gamma     0.251  0.042   0.169    0.331  ...   152.0     152.0      88.0   0.98
Is_begin  1.973  1.241   0.160    4.456  ...   116.0      88.0      60.0   1.06
Ia_begin  6.149  3.809   0.330   13.404  ...    80.0      78.0      93.0   0.99
E_begin   5.374  5.696   0.384   13.416  ...    24.0      27.0      60.0   1.09

[8 rows x 11 columns]