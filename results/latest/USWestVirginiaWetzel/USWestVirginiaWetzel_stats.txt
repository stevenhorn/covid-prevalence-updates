0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -593.27    28.12
p_loo       27.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.058   0.151    0.341  ...    23.0      22.0      38.0   1.08
pu        0.862  0.022   0.824    0.897  ...    16.0      17.0      17.0   1.10
mu        0.144  0.025   0.097    0.180  ...    21.0      17.0      60.0   1.12
mus       0.190  0.035   0.134    0.257  ...   152.0     152.0     100.0   0.99
gamma     0.245  0.034   0.195    0.313  ...   152.0     152.0      88.0   1.00
Is_begin  0.666  0.539   0.002    1.697  ...    63.0      55.0      33.0   1.00
Ia_begin  1.773  2.283   0.043    5.170  ...    89.0     105.0      69.0   1.02
E_begin   0.744  0.844   0.001    1.946  ...    94.0     111.0      70.0   1.01

[8 rows x 11 columns]