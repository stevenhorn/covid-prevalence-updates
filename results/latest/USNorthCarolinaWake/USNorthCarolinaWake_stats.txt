7 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1608.60    30.73
p_loo       23.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.249  0.053   0.168    0.343  ...    99.0     104.0      60.0   1.04
pu         0.764  0.050   0.702    0.856  ...    40.0      30.0      24.0   1.05
mu         0.106  0.018   0.066    0.132  ...    39.0      41.0      40.0   1.10
mus        0.156  0.027   0.104    0.206  ...    82.0      72.0      95.0   0.99
gamma      0.181  0.030   0.125    0.225  ...   103.0     110.0      86.0   1.00
Is_begin   4.900  3.719   0.108   10.023  ...    60.0      35.0      43.0   1.04
Ia_begin  14.191  8.208   0.760   28.795  ...   113.0     101.0      57.0   0.98
E_begin   12.095  9.295   1.136   35.991  ...    80.0      72.0      88.0   1.02

[8 rows x 11 columns]