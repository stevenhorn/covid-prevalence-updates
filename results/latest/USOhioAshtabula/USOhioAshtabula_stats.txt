0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -905.81    28.64
p_loo       23.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      263   89.2%
 (0.5, 0.7]   (ok)         24    8.1%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.267  0.051   0.179    0.347  ...   128.0     133.0      60.0   1.00
pu        0.877  0.022   0.839    0.900  ...    62.0      61.0      96.0   1.02
mu        0.133  0.021   0.098    0.177  ...    36.0      33.0      43.0   1.07
mus       0.163  0.032   0.120    0.228  ...   108.0     112.0      95.0   1.00
gamma     0.231  0.042   0.170    0.323  ...    46.0      49.0      52.0   1.03
Is_begin  1.068  0.884   0.011    2.786  ...    88.0      65.0      49.0   1.02
Ia_begin  2.293  2.099   0.030    6.396  ...    89.0      66.0      59.0   0.99
E_begin   1.510  1.575   0.046    3.825  ...    58.0      28.0      39.0   1.07

[8 rows x 11 columns]