0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1787.12    25.92
p_loo       28.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      261   88.5%
 (0.5, 0.7]   (ok)         27    9.2%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    0    0.0%

              mean       sd   hdi_3%  ...  ess_bulk  ess_tail  r_hat
pa           0.255    0.051    0.152  ...      27.0      49.0   1.46
pu           0.833    0.067    0.725  ...       4.0      24.0   1.82
mu           0.171    0.055    0.088  ...       3.0      38.0   1.94
mus          0.161    0.030    0.116  ...       6.0      27.0   1.33
gamma        0.247    0.046    0.174  ...       9.0      42.0   1.23
Is_begin   154.098   74.521   14.326  ...       4.0      24.0   1.86
Ia_begin   387.653  161.921  161.446  ...       4.0      36.0   1.75
E_begin   1026.052  711.664   82.257  ...       3.0      38.0   1.96

[8 rows x 11 columns]