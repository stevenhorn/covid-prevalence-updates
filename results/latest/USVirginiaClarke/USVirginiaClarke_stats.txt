0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -470.86    21.31
p_loo       18.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.280  0.049   0.191    0.348  ...   122.0     129.0      72.0   1.03
pu        0.884  0.017   0.856    0.900  ...    55.0      73.0      15.0   1.14
mu        0.129  0.025   0.077    0.165  ...    17.0      18.0      65.0   1.08
mus       0.146  0.030   0.096    0.198  ...   140.0     152.0      56.0   1.08
gamma     0.164  0.032   0.108    0.227  ...    57.0      79.0      40.0   1.01
Is_begin  0.809  0.661   0.047    1.746  ...   122.0     104.0      60.0   0.98
Ia_begin  1.680  1.657   0.008    5.184  ...   104.0      84.0      60.0   0.99
E_begin   0.840  0.866   0.017    2.594  ...   106.0      83.0      74.0   0.99

[8 rows x 11 columns]