0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -525.54    28.79
p_loo       23.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.059   0.159    0.348  ...    95.0      80.0      42.0   1.05
pu        0.813  0.017   0.787    0.846  ...   105.0     108.0      42.0   1.03
mu        0.127  0.020   0.088    0.163  ...    25.0      32.0      60.0   1.06
mus       0.173  0.034   0.123    0.232  ...   152.0     152.0      93.0   1.01
gamma     0.214  0.033   0.169    0.282  ...   152.0     152.0      79.0   1.01
Is_begin  0.477  0.492   0.021    1.648  ...   104.0     152.0      54.0   1.09
Ia_begin  0.936  0.964   0.007    2.802  ...   106.0      93.0      59.0   1.01
E_begin   0.501  0.559   0.004    1.742  ...   121.0     152.0      65.0   0.98

[8 rows x 11 columns]