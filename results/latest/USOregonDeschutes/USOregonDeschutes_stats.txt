0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -931.93    31.54
p_loo       25.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.059   0.165    0.347  ...    66.0      79.0      80.0   1.02
pu        0.864  0.035   0.791    0.899  ...    84.0      91.0     100.0   1.00
mu        0.146  0.040   0.074    0.206  ...     9.0      10.0      31.0   1.15
mus       0.187  0.030   0.138    0.242  ...   118.0     101.0      42.0   1.02
gamma     0.254  0.045   0.181    0.342  ...   109.0     107.0      93.0   0.99
Is_begin  0.759  0.567   0.001    1.866  ...   114.0      66.0      43.0   1.02
Ia_begin  2.153  1.503   0.039    4.443  ...   113.0     106.0      91.0   0.98
E_begin   1.402  1.800   0.008    5.902  ...   110.0      91.0      60.0   1.04

[8 rows x 11 columns]