0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -717.73    31.71
p_loo       24.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         9    3.1%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.058   0.158    0.338  ...     9.0      13.0      39.0   1.24
pu        0.878  0.016   0.846    0.899  ...    22.0      19.0      60.0   1.09
mu        0.148  0.022   0.112    0.191  ...    19.0      21.0      59.0   1.09
mus       0.152  0.028   0.118    0.205  ...    82.0      82.0      54.0   1.02
gamma     0.195  0.029   0.142    0.246  ...    61.0      58.0      96.0   1.01
Is_begin  0.902  0.658   0.011    2.062  ...    38.0      33.0      77.0   1.06
Ia_begin  2.690  2.010   0.073    6.964  ...    62.0      53.0      59.0   1.09
E_begin   1.322  1.726   0.026    3.768  ...    40.0      31.0      38.0   1.05

[8 rows x 11 columns]