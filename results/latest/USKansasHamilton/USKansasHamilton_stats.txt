0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -476.93    27.68
p_loo       24.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.053   0.153    0.317  ...    25.0      23.0      39.0   1.09
pu        0.844  0.019   0.815    0.882  ...    60.0      66.0      72.0   1.00
mu        0.122  0.021   0.088    0.156  ...    61.0      52.0      69.0   1.01
mus       0.171  0.024   0.134    0.217  ...    62.0      64.0      60.0   1.00
gamma     0.219  0.037   0.148    0.271  ...    65.0      63.0      80.0   0.99
Is_begin  0.584  0.535   0.003    1.627  ...    50.0      43.0      40.0   1.03
Ia_begin  1.336  1.488   0.047    4.823  ...    48.0      45.0      60.0   1.01
E_begin   0.580  0.599   0.022    1.716  ...    44.0      27.0      72.0   1.04

[8 rows x 11 columns]