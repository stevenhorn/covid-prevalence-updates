0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -797.50    20.02
p_loo       18.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      264   89.5%
 (0.5, 0.7]   (ok)         25    8.5%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.057   0.160    0.341  ...    53.0      51.0      48.0   1.07
pu        0.778  0.030   0.712    0.820  ...    12.0      15.0      38.0   1.12
mu        0.108  0.023   0.078    0.157  ...     5.0       6.0      17.0   1.37
mus       0.148  0.030   0.095    0.196  ...    66.0      68.0      60.0   1.02
gamma     0.167  0.032   0.106    0.225  ...   108.0     102.0      53.0   1.02
Is_begin  0.817  0.800   0.011    2.533  ...   100.0      88.0      60.0   1.01
Ia_begin  1.749  1.533   0.018    4.467  ...    95.0      78.0      60.0   1.02
E_begin   0.890  1.198   0.001    3.505  ...    92.0      51.0      59.0   1.05

[8 rows x 11 columns]