0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1683.85    28.06
p_loo       23.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      263   89.2%
 (0.5, 0.7]   (ok)         29    9.8%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.244   0.048   0.156    0.321  ...   152.0     152.0      56.0   1.03
pu         0.754   0.034   0.706    0.811  ...   103.0      99.0      88.0   0.98
mu         0.107   0.018   0.077    0.140  ...    54.0      53.0      60.0   0.98
mus        0.172   0.026   0.128    0.214  ...   122.0     151.0      65.0   0.99
gamma      0.226   0.037   0.165    0.304  ...   116.0     118.0      56.0   0.99
Is_begin   6.121   3.231   0.301   10.657  ...   117.0     107.0      18.0   1.04
Ia_begin  14.481   7.891   2.041   28.084  ...    66.0      38.0      56.0   1.04
E_begin   15.651  14.812   0.045   40.133  ...    98.0      75.0      43.0   1.02

[8 rows x 11 columns]