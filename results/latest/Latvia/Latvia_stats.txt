0 Divergences 
Passed validation 
Computed from 80 by 275 log-likelihood matrix

         Estimate       SE
elpd_loo -1061.95    27.95
p_loo       29.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      247   89.8%
 (0.5, 0.7]   (ok)         22    8.0%
   (0.7, 1]   (bad)         5    1.8%
   (1, Inf)   (very bad)    1    0.4%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.211   0.056   0.150    0.329  ...   152.0     152.0      24.0   1.01
pu         0.721   0.016   0.701    0.748  ...   101.0      81.0      81.0   0.99
mu         0.132   0.023   0.097    0.179  ...    10.0      12.0      48.0   1.12
mus        0.212   0.034   0.154    0.275  ...   135.0     146.0      42.0   1.04
gamma      0.345   0.051   0.260    0.451  ...    74.0      75.0      60.0   1.02
Is_begin  12.613   4.001   4.980   18.403  ...   103.0      70.0      63.0   1.03
Ia_begin   9.136   4.018   1.546   16.263  ...    98.0      95.0      37.0   1.00
E_begin   38.952  16.273   5.199   60.742  ...    38.0      31.0      53.0   1.06

[8 rows x 11 columns]