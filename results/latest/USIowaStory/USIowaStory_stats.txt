0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1125.37    38.79
p_loo       27.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         24    8.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.053   0.174    0.350  ...    54.0      58.0      56.0   1.03
pu        0.793  0.033   0.715    0.838  ...    53.0      58.0      29.0   1.02
mu        0.148  0.020   0.102    0.180  ...    34.0      37.0      75.0   1.02
mus       0.228  0.031   0.170    0.279  ...    69.0      64.0      59.0   1.04
gamma     0.360  0.058   0.265    0.480  ...    59.0      67.0      55.0   1.06
Is_begin  0.998  0.895   0.002    2.774  ...    66.0     152.0      15.0   1.06
Ia_begin  1.845  1.422   0.030    4.623  ...   101.0     105.0      83.0   0.99
E_begin   0.739  0.784   0.015    2.462  ...    70.0      64.0      59.0   1.04

[8 rows x 11 columns]