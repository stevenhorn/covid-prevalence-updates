0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -616.20    20.11
p_loo       18.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.055   0.151    0.332  ...   126.0     128.0      60.0   1.09
pu        0.811  0.058   0.719    0.897  ...    11.0      12.0      57.0   1.16
mu        0.116  0.028   0.076    0.176  ...    21.0      20.0      59.0   1.08
mus       0.156  0.030   0.103    0.215  ...   107.0     133.0      88.0   1.03
gamma     0.191  0.035   0.125    0.255  ...   152.0     152.0      48.0   1.10
Is_begin  1.091  1.027   0.020    3.260  ...   111.0     103.0     100.0   1.00
Ia_begin  2.405  2.260   0.055    6.918  ...    40.0      29.0      57.0   1.07
E_begin   1.273  1.594   0.015    4.664  ...    44.0      18.0      72.0   1.09

[8 rows x 11 columns]