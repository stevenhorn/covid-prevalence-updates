19 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -521.59    23.93
p_loo       19.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      251   85.1%
 (0.5, 0.7]   (ok)         31   10.5%
   (0.7, 1]   (bad)        10    3.4%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.301  0.037   0.237    0.350  ...    22.0      25.0      43.0   1.09
pu        0.884  0.010   0.864    0.897  ...    12.0      13.0      24.0   1.15
mu        0.122  0.017   0.091    0.154  ...    16.0      18.0      22.0   1.12
mus       0.156  0.028   0.108    0.195  ...    33.0      39.0      53.0   1.23
gamma     0.185  0.037   0.124    0.233  ...    14.0      10.0      58.0   1.19
Is_begin  0.423  0.517   0.012    1.466  ...    11.0       6.0      59.0   1.35
Ia_begin  0.974  1.057   0.054    3.012  ...    20.0      10.0      19.0   1.16
E_begin   0.520  0.546   0.032    1.466  ...    42.0      35.0      59.0   1.14

[8 rows x 11 columns]