0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1034.93    34.02
p_loo       25.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.062   0.156    0.339  ...   152.0     152.0      96.0   1.01
pu        0.781  0.055   0.708    0.885  ...   106.0     152.0      81.0   0.98
mu        0.135  0.027   0.088    0.178  ...    40.0      39.0      96.0   1.03
mus       0.186  0.035   0.132    0.263  ...   152.0     152.0      53.0   1.04
gamma     0.254  0.042   0.184    0.330  ...    75.0      80.0      25.0   1.04
Is_begin  1.868  1.105   0.045    3.824  ...    93.0      83.0      43.0   1.04
Ia_begin  5.356  2.532   1.002    9.721  ...    90.0      85.0     100.0   0.99
E_begin   4.578  3.097   0.177   10.105  ...    70.0      64.0      60.0   0.99

[8 rows x 11 columns]