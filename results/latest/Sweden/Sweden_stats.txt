7 Divergences 
Failed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -2358.55    28.86
p_loo       23.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.6%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

             mean       sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.260    0.056   0.170    0.346  ...    55.0      58.0      43.0   1.07
pu          0.780    0.047   0.708    0.869  ...    26.0      25.0      59.0   1.05
mu          0.115    0.024   0.077    0.161  ...     7.0       6.0      20.0   1.32
mus         0.155    0.028   0.113    0.207  ...    40.0      41.0      40.0   1.05
gamma       0.209    0.035   0.142    0.274  ...    77.0      78.0      88.0   1.03
Is_begin  156.339  126.281   1.185  377.256  ...     7.0       5.0      24.0   1.48
Ia_begin  486.536  292.093  66.229  975.022  ...    35.0      32.0      48.0   1.05
E_begin   372.638  247.523  10.893  739.622  ...    63.0      65.0      60.0   1.06

[8 rows x 11 columns]