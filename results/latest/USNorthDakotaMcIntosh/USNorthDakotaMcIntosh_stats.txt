0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -593.67    57.50
p_loo       38.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      288   97.6%
 (0.5, 0.7]   (ok)          5    1.7%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.049   0.159    0.320  ...    75.0      84.0      31.0   1.07
pu        0.730  0.019   0.701    0.761  ...    74.0      59.0      31.0   1.02
mu        0.120  0.022   0.087    0.162  ...    41.0      41.0      60.0   1.01
mus       0.199  0.037   0.128    0.254  ...    25.0      24.0      36.0   1.06
gamma     0.262  0.055   0.183    0.355  ...    59.0      94.0      34.0   1.00
Is_begin  0.410  0.355   0.006    1.100  ...    91.0      63.0      46.0   1.02
Ia_begin  0.810  0.945   0.002    2.515  ...   106.0      16.0      38.0   1.13
E_begin   0.337  0.387   0.001    1.061  ...    95.0      26.0      80.0   1.07

[8 rows x 11 columns]