0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -577.83    26.63
p_loo       22.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         24    8.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.271  0.043   0.207    0.349  ...    20.0      22.0      17.0   1.06
pu        0.862  0.022   0.820    0.899  ...    20.0      24.0      59.0   1.05
mu        0.122  0.021   0.090    0.160  ...     7.0       7.0      57.0   1.24
mus       0.164  0.032   0.116    0.235  ...    62.0      66.0      22.0   1.09
gamma     0.177  0.030   0.134    0.230  ...    60.0      58.0      60.0   1.03
Is_begin  0.919  0.584   0.137    2.059  ...    77.0      80.0      36.0   1.07
Ia_begin  1.877  1.831   0.018    6.308  ...    53.0      39.0      31.0   1.19
E_begin   1.088  0.967   0.059    3.230  ...    43.0      35.0      47.0   1.06

[8 rows x 11 columns]