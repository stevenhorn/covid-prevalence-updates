0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1059.13    24.45
p_loo       21.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      262   88.8%
 (0.5, 0.7]   (ok)         27    9.2%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.054   0.166    0.344  ...   105.0      95.0      58.0   1.01
pu        0.776  0.031   0.720    0.830  ...    82.0      81.0      59.0   0.99
mu        0.117  0.018   0.088    0.154  ...    22.0      19.0      54.0   1.08
mus       0.156  0.031   0.107    0.200  ...   112.0     109.0      38.0   0.99
gamma     0.199  0.038   0.130    0.274  ...   152.0     152.0      73.0   0.99
Is_begin  0.638  0.593   0.033    1.844  ...    70.0      84.0      93.0   1.01
Ia_begin  1.000  1.116   0.019    3.335  ...    84.0      74.0      40.0   1.05
E_begin   0.579  0.702   0.026    1.791  ...    67.0     100.0      45.0   0.99

[8 rows x 11 columns]