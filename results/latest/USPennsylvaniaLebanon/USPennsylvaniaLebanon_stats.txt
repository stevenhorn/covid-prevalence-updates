1 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1086.18    26.59
p_loo       24.23        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.059   0.154    0.337  ...    70.0      86.0      39.0   1.06
pu        0.752  0.036   0.701    0.810  ...    82.0      69.0      58.0   1.01
mu        0.114  0.017   0.082    0.145  ...    32.0      33.0      59.0   1.03
mus       0.183  0.030   0.129    0.226  ...    75.0      79.0      60.0   1.02
gamma     0.266  0.035   0.208    0.330  ...   152.0     152.0      69.0   1.00
Is_begin  1.871  0.997   0.425    3.575  ...    66.0      81.0     100.0   1.03
Ia_begin  0.840  0.519   0.086    1.773  ...    69.0      51.0      59.0   1.07
E_begin   2.156  1.641   0.146    5.268  ...    58.0      43.0      80.0   1.04

[8 rows x 11 columns]