0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -944.37    19.48
p_loo       16.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.055   0.157    0.336  ...    75.0      82.0      86.0   1.02
pu        0.847  0.024   0.801    0.881  ...    55.0      60.0      59.0   1.02
mu        0.105  0.017   0.081    0.139  ...    31.0      38.0      46.0   1.09
mus       0.146  0.032   0.093    0.209  ...    42.0      50.0      25.0   1.10
gamma     0.170  0.032   0.108    0.222  ...   152.0     152.0      91.0   0.99
Is_begin  0.877  0.838   0.004    2.355  ...   152.0     152.0      60.0   1.00
Ia_begin  1.978  1.851   0.133    6.248  ...    41.0      29.0      73.0   1.07
E_begin   1.081  1.092   0.016    3.400  ...    50.0      68.0      29.0   1.06

[8 rows x 11 columns]