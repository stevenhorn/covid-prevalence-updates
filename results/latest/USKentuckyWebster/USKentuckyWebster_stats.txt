0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -648.45    36.37
p_loo       29.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.057   0.154    0.335  ...    52.0      45.0      51.0   1.07
pu        0.833  0.036   0.770    0.895  ...    18.0      21.0      22.0   1.08
mu        0.125  0.026   0.086    0.174  ...    37.0      38.0      60.0   1.00
mus       0.173  0.031   0.121    0.228  ...    61.0      66.0      56.0   1.07
gamma     0.210  0.050   0.122    0.297  ...    88.0     104.0      59.0   1.18
Is_begin  0.896  0.802   0.025    2.123  ...   108.0      89.0      65.0   1.02
Ia_begin  1.793  1.628   0.039    5.033  ...    86.0      74.0      57.0   0.98
E_begin   0.955  0.995   0.016    2.983  ...    57.0      69.0      60.0   0.98

[8 rows x 11 columns]