0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1817.16    55.28
p_loo       28.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         24    8.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.250   0.058   0.152    0.335  ...    87.0      75.0      34.0   1.05
pu         0.762   0.038   0.702    0.830  ...    84.0      90.0      59.0   1.01
mu         0.113   0.023   0.073    0.153  ...    41.0      43.0      58.0   1.00
mus        0.199   0.037   0.146    0.273  ...    52.0      45.0      21.0   1.05
gamma      0.265   0.050   0.190    0.370  ...   152.0     152.0      56.0   1.00
Is_begin  32.888  18.075   3.540   66.610  ...   116.0      99.0      59.0   1.00
Ia_begin  88.168  43.621  18.551  174.577  ...   108.0      93.0      17.0   1.03
E_begin   81.669  52.562   8.868  195.219  ...   122.0     121.0      88.0   0.98

[8 rows x 11 columns]