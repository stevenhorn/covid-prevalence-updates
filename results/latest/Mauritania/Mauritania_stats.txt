0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -1127.72    23.25
p_loo       21.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      265   89.5%
 (0.5, 0.7]   (ok)         25    8.4%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.281  0.046   0.201    0.345  ...    78.0      76.0      59.0   1.07
pu        0.879  0.022   0.832    0.900  ...   117.0      97.0      60.0   1.00
mu        0.134  0.020   0.101    0.166  ...    13.0      13.0      42.0   1.10
mus       0.189  0.032   0.137    0.243  ...    62.0      63.0      64.0   1.03
gamma     0.254  0.040   0.181    0.318  ...   131.0     117.0      88.0   0.99
Is_begin  0.685  0.586   0.032    1.671  ...    60.0      65.0      60.0   1.01
Ia_begin  0.615  0.503   0.028    1.484  ...    91.0     100.0      60.0   0.98
E_begin   0.411  0.449   0.007    1.351  ...    49.0      38.0      86.0   1.05

[8 rows x 11 columns]