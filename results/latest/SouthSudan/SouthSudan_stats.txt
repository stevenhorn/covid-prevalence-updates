0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -1245.54    34.83
p_loo       31.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.6%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    4    1.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.052   0.171    0.350  ...   132.0     116.0      58.0   1.02
pu        0.866  0.033   0.801    0.900  ...    13.0       8.0      17.0   1.25
mu        0.133  0.022   0.095    0.177  ...     8.0       9.0      48.0   1.17
mus       0.173  0.028   0.128    0.231  ...   107.0     107.0      49.0   1.02
gamma     0.231  0.029   0.179    0.276  ...    67.0      69.0      93.0   1.06
Is_begin  0.546  0.515   0.008    1.774  ...    49.0      51.0      21.0   1.02
Ia_begin  1.091  1.255   0.009    3.505  ...    69.0      51.0      40.0   1.04
E_begin   0.683  1.034   0.003    2.372  ...    80.0      63.0      65.0   1.04

[8 rows x 11 columns]