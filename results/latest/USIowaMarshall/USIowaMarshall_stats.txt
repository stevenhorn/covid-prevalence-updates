0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -987.67    24.92
p_loo       28.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.054   0.184    0.347  ...    50.0      47.0      34.0   1.01
pu        0.780  0.025   0.734    0.818  ...    33.0      34.0      60.0   1.03
mu        0.147  0.018   0.116    0.180  ...    21.0      21.0      24.0   1.06
mus       0.212  0.029   0.162    0.268  ...   136.0     120.0      70.0   1.02
gamma     0.347  0.046   0.277    0.434  ...    36.0      38.0      72.0   1.05
Is_begin  0.657  0.598   0.012    1.828  ...    38.0      55.0      40.0   1.04
Ia_begin  1.146  1.150   0.047    3.386  ...    81.0      71.0      59.0   1.01
E_begin   0.634  0.716   0.005    1.961  ...    76.0      63.0      31.0   0.99

[8 rows x 11 columns]