0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -977.50    53.67
p_loo       38.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.050   0.154    0.322  ...    46.0      44.0      33.0   1.06
pu        0.733  0.018   0.700    0.760  ...    66.0      65.0      59.0   1.00
mu        0.178  0.022   0.143    0.224  ...     8.0       8.0      38.0   1.20
mus       0.281  0.032   0.225    0.332  ...    74.0      81.0      96.0   1.01
gamma     0.405  0.069   0.300    0.528  ...    29.0      24.0      59.0   1.09
Is_begin  0.992  0.767   0.087    2.577  ...    67.0     152.0      54.0   1.01
Ia_begin  1.757  1.565   0.096    3.775  ...    59.0      98.0      76.0   1.04
E_begin   0.906  1.013   0.002    2.428  ...    69.0     143.0      54.0   1.00

[8 rows x 11 columns]