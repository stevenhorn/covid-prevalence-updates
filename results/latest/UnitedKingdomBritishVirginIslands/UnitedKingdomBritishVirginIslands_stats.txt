0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo  -342.65    37.63
p_loo       35.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.6%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.053   0.175    0.348  ...   152.0     152.0      91.0   1.05
pu        0.828  0.038   0.770    0.896  ...    22.0      24.0      38.0   1.06
mu        0.212  0.041   0.153    0.302  ...     7.0       7.0      19.0   1.26
mus       0.261  0.047   0.183    0.335  ...   100.0      94.0      79.0   1.02
gamma     0.351  0.061   0.255    0.474  ...    76.0      68.0      88.0   1.03
Is_begin  0.969  0.645   0.078    2.167  ...   126.0     105.0      69.0   0.99
Ia_begin  2.498  1.801   0.125    6.148  ...   103.0      87.0      67.0   1.00
E_begin   1.165  1.194   0.029    4.425  ...    74.0      54.0      60.0   1.01

[8 rows x 11 columns]