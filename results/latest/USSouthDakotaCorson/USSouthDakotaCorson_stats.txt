0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -523.06    32.80
p_loo       25.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.049   0.166    0.330  ...    56.0      57.0      60.0   1.04
pu        0.779  0.022   0.746    0.823  ...    21.0      20.0      43.0   1.09
mu        0.123  0.029   0.074    0.175  ...     6.0       7.0      19.0   1.27
mus       0.158  0.033   0.104    0.218  ...    39.0      34.0      20.0   1.06
gamma     0.165  0.044   0.090    0.249  ...    11.0      10.0      59.0   1.20
Is_begin  0.526  0.484   0.000    1.478  ...    66.0      33.0      37.0   1.04
Ia_begin  1.189  1.186   0.015    3.290  ...    59.0      85.0      61.0   1.04
E_begin   0.410  0.414   0.016    1.405  ...    68.0      58.0      40.0   1.02

[8 rows x 11 columns]