0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -571.53    56.16
p_loo       41.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.048   0.177    0.339  ...   106.0     124.0      17.0   1.06
pu        0.865  0.033   0.790    0.900  ...    26.0      41.0      56.0   1.05
mu        0.175  0.028   0.130    0.223  ...    14.0      13.0      60.0   1.14
mus       0.231  0.031   0.185    0.284  ...    55.0      57.0      60.0   0.98
gamma     0.361  0.061   0.268    0.471  ...   103.0     118.0      58.0   1.01
Is_begin  1.944  1.262   0.063    4.269  ...   117.0      82.0      39.0   1.00
Ia_begin  0.809  0.615   0.151    2.356  ...   136.0     106.0      59.0   1.02
E_begin   1.679  2.073   0.023    5.279  ...   112.0      47.0      91.0   1.03

[8 rows x 11 columns]