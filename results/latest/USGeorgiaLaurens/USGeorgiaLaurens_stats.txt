0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1075.86    59.79
p_loo       34.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.050   0.160    0.322  ...    92.0     118.0      59.0   1.11
pu        0.749  0.024   0.702    0.783  ...    70.0      65.0      36.0   1.03
mu        0.148  0.021   0.119    0.196  ...    42.0      48.0      72.0   1.14
mus       0.275  0.040   0.218    0.373  ...    54.0      61.0      54.0   1.03
gamma     0.371  0.074   0.242    0.503  ...    64.0      80.0      38.0   1.01
Is_begin  1.029  1.015   0.014    2.824  ...    97.0      62.0      42.0   1.01
Ia_begin  0.746  0.552   0.008    1.674  ...   152.0     152.0      47.0   1.02
E_begin   0.665  0.598   0.018    1.654  ...    91.0      74.0      87.0   1.01

[8 rows x 11 columns]