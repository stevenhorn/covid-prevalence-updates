0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1185.90    21.08
p_loo       20.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         24    8.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.052   0.172    0.342  ...    20.0      27.0      57.0   1.06
pu        0.772  0.047   0.703    0.855  ...    16.0      17.0      59.0   1.13
mu        0.109  0.022   0.072    0.145  ...    11.0      11.0      54.0   1.17
mus       0.166  0.032   0.119    0.238  ...   152.0     152.0      30.0   1.09
gamma     0.235  0.035   0.168    0.295  ...    70.0      75.0      60.0   1.00
Is_begin  0.785  0.632   0.067    1.964  ...   109.0      62.0      57.0   1.03
Ia_begin  1.922  1.194   0.087    3.952  ...    99.0      84.0      40.0   1.02
E_begin   1.817  1.688   0.017    5.666  ...    23.0      19.0      39.0   1.10

[8 rows x 11 columns]