0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -813.14    23.95
p_loo       24.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.050   0.151    0.320  ...    41.0      39.0      58.0   1.04
pu        0.825  0.018   0.790    0.859  ...    28.0      27.0      43.0   1.06
mu        0.131  0.021   0.101    0.172  ...    10.0      10.0      67.0   1.17
mus       0.161  0.032   0.108    0.230  ...    82.0      92.0     100.0   1.03
gamma     0.203  0.042   0.130    0.281  ...   121.0     148.0      84.0   1.06
Is_begin  0.903  0.963   0.016    2.807  ...    78.0      88.0      60.0   0.99
Ia_begin  1.719  1.606   0.034    4.942  ...    66.0      22.0      19.0   1.08
E_begin   0.816  0.945   0.005    2.731  ...    96.0      54.0      60.0   1.00

[8 rows x 11 columns]