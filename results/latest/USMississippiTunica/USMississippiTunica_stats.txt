0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -653.18    26.97
p_loo       26.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.050   0.172    0.337  ...    47.0      46.0      59.0   1.13
pu        0.834  0.025   0.795    0.875  ...    42.0      50.0      17.0   1.08
mu        0.146  0.023   0.113    0.193  ...    40.0      40.0      24.0   1.05
mus       0.181  0.025   0.135    0.223  ...    67.0      68.0      73.0   0.99
gamma     0.224  0.039   0.164    0.300  ...   128.0     147.0      60.0   0.99
Is_begin  1.339  0.968   0.068    3.217  ...    78.0      80.0      40.0   1.02
Ia_begin  2.962  2.218   0.270    7.403  ...    95.0      74.0      59.0   1.04
E_begin   1.377  1.452   0.059    4.342  ...    79.0      63.0     100.0   1.00

[8 rows x 11 columns]