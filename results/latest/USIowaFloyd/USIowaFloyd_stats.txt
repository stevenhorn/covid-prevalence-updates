0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -695.29    31.81
p_loo       31.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         24    8.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.060   0.172    0.350  ...    18.0      21.0      34.0   1.07
pu        0.828  0.023   0.786    0.866  ...    20.0      21.0      40.0   1.09
mu        0.123  0.021   0.090    0.161  ...    29.0      30.0      40.0   1.02
mus       0.189  0.031   0.129    0.235  ...    55.0      60.0      57.0   1.01
gamma     0.254  0.042   0.186    0.333  ...    80.0      75.0      59.0   0.99
Is_begin  0.363  0.450   0.006    1.244  ...    71.0      58.0      19.0   1.04
Ia_begin  0.479  0.535   0.024    1.277  ...    55.0      43.0      86.0   1.08
E_begin   0.284  0.428   0.002    1.035  ...    99.0      62.0      60.0   1.02

[8 rows x 11 columns]