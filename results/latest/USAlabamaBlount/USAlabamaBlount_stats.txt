0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -928.59    22.52
p_loo       16.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.047   0.175    0.336  ...    88.0     105.0      34.0   1.02
pu        0.836  0.016   0.804    0.861  ...    28.0      31.0      57.0   1.05
mu        0.111  0.019   0.079    0.146  ...     7.0       7.0      42.0   1.25
mus       0.163  0.030   0.114    0.222  ...    87.0      80.0      57.0   1.01
gamma     0.203  0.038   0.130    0.256  ...   152.0     152.0     100.0   1.00
Is_begin  0.807  0.719   0.041    2.163  ...    84.0     109.0      58.0   1.10
Ia_begin  1.712  1.811   0.022    5.025  ...    23.0      68.0      25.0   1.06
E_begin   0.740  0.871   0.007    2.174  ...    26.0      27.0      25.0   1.18

[8 rows x 11 columns]