0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -896.81    22.70
p_loo       20.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.276  0.054   0.179    0.350  ...   127.0     134.0      83.0   1.01
pu        0.872  0.027   0.821    0.900  ...    91.0     103.0      93.0   1.02
mu        0.176  0.029   0.128    0.233  ...     9.0       8.0      28.0   1.24
mus       0.186  0.031   0.126    0.243  ...    50.0      50.0      86.0   1.03
gamma     0.267  0.050   0.172    0.353  ...    42.0      32.0      39.0   1.06
Is_begin  0.908  0.562   0.032    1.877  ...   152.0     152.0      88.0   1.01
Ia_begin  2.018  1.877   0.044    5.444  ...   100.0      50.0      69.0   1.03
E_begin   1.354  1.489   0.031    4.564  ...    98.0      54.0      60.0   1.02

[8 rows x 11 columns]