0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -866.88    28.69
p_loo       22.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.274  0.053   0.174    0.347  ...    50.0      44.0      47.0   1.08
pu        0.885  0.015   0.862    0.900  ...    91.0      23.0      60.0   1.08
mu        0.125  0.025   0.088    0.174  ...     6.0       6.0      17.0   1.37
mus       0.156  0.027   0.120    0.221  ...    74.0      62.0      88.0   1.05
gamma     0.167  0.029   0.126    0.231  ...   152.0     152.0      70.0   0.98
Is_begin  0.940  0.858   0.070    2.520  ...   102.0      89.0      99.0   1.01
Ia_begin  0.755  0.504   0.044    1.688  ...    83.0      69.0      59.0   1.02
E_begin   0.711  0.664   0.014    1.977  ...   116.0      71.0      26.0   1.02

[8 rows x 11 columns]