0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -636.89    31.88
p_loo       29.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.054   0.164    0.336  ...    82.0      84.0      49.0   1.16
pu        0.836  0.025   0.791    0.880  ...    30.0      27.0      43.0   1.05
mu        0.136  0.028   0.088    0.187  ...    19.0      18.0      25.0   1.09
mus       0.169  0.030   0.106    0.219  ...   109.0     119.0      58.0   0.99
gamma     0.185  0.036   0.118    0.246  ...    60.0      67.0      46.0   1.01
Is_begin  0.672  0.793   0.002    2.151  ...   123.0     152.0      63.0   1.01
Ia_begin  1.442  1.309   0.032    4.193  ...    63.0      55.0      87.0   1.04
E_begin   0.573  0.620   0.008    2.143  ...   101.0      55.0      35.0   1.03

[8 rows x 11 columns]