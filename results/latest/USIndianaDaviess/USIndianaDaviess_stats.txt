0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -788.09    28.00
p_loo       22.41        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.056   0.159    0.340  ...   112.0      99.0      39.0   1.14
pu        0.805  0.044   0.709    0.860  ...    32.0      41.0      38.0   1.06
mu        0.127  0.025   0.084    0.170  ...    27.0      37.0      38.0   1.04
mus       0.180  0.034   0.122    0.241  ...   152.0     152.0      72.0   0.98
gamma     0.222  0.048   0.139    0.293  ...   152.0     152.0      93.0   1.00
Is_begin  1.077  1.039   0.011    3.265  ...   152.0     152.0      88.0   0.99
Ia_begin  2.299  1.725   0.152    5.601  ...   103.0      89.0      40.0   1.01
E_begin   1.065  1.020   0.084    2.657  ...    81.0      47.0      65.0   1.04

[8 rows x 11 columns]