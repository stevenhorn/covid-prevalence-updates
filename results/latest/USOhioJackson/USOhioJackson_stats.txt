0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -719.61    28.93
p_loo       20.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.065   0.150    0.338  ...    12.0      10.0      15.0   1.18
pu        0.854  0.018   0.816    0.881  ...    67.0      68.0      57.0   0.99
mu        0.121  0.024   0.077    0.156  ...    38.0      38.0      60.0   1.03
mus       0.176  0.031   0.125    0.230  ...   152.0     152.0      67.0   1.09
gamma     0.199  0.029   0.147    0.247  ...   102.0     102.0      91.0   0.99
Is_begin  0.643  0.490   0.005    1.606  ...   108.0     108.0      83.0   0.99
Ia_begin  1.227  1.287   0.024    3.834  ...    74.0      52.0      65.0   1.02
E_begin   0.699  0.774   0.005    1.987  ...   103.0      76.0      88.0   1.02

[8 rows x 11 columns]