0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1425.34    26.55
p_loo       24.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    5    1.7%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.251   0.057   0.161    0.340  ...   152.0     152.0     100.0   1.07
pu         0.764   0.039   0.705    0.829  ...    68.0      75.0      59.0   1.03
mu         0.118   0.020   0.083    0.148  ...    55.0      46.0      60.0   1.04
mus        0.170   0.027   0.123    0.225  ...   152.0     150.0      91.0   1.00
gamma      0.215   0.041   0.138    0.295  ...   152.0     152.0      91.0   1.03
Is_begin   6.614   4.794   0.133   15.160  ...    67.0      55.0      59.0   1.02
Ia_begin  14.022   9.588   2.745   33.808  ...   111.0      90.0      60.0   0.99
E_begin   13.667  10.601   1.055   35.548  ...    73.0      78.0      88.0   1.02

[8 rows x 11 columns]