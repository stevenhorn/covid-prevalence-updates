0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -643.42    48.60
p_loo       32.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.056   0.178    0.343  ...    86.0      75.0      24.0   1.07
pu        0.827  0.053   0.719    0.896  ...     6.0       8.0      57.0   1.22
mu        0.118  0.026   0.079    0.171  ...    37.0      40.0      51.0   1.07
mus       0.173  0.034   0.130    0.236  ...    50.0      53.0      40.0   1.03
gamma     0.207  0.034   0.139    0.265  ...    30.0      33.0      83.0   1.02
Is_begin  0.866  0.811   0.013    2.607  ...    81.0      73.0      58.0   1.02
Ia_begin  1.492  1.463   0.001    4.281  ...    38.0      16.0      22.0   1.12
E_begin   0.869  0.957   0.007    3.009  ...    86.0      53.0      49.0   1.03

[8 rows x 11 columns]