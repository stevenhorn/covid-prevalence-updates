0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -939.68    42.51
p_loo       29.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.050   0.181    0.346  ...    79.0      95.0     100.0   1.02
pu        0.822  0.031   0.771    0.873  ...    50.0      52.0      58.0   1.03
mu        0.134  0.029   0.087    0.192  ...     9.0       9.0      22.0   1.19
mus       0.173  0.026   0.130    0.226  ...    84.0      79.0      97.0   0.99
gamma     0.200  0.038   0.133    0.270  ...   152.0     152.0      91.0   1.04
Is_begin  1.218  0.850   0.022    2.721  ...    72.0      66.0      80.0   1.01
Ia_begin  2.755  1.993   0.186    6.062  ...   152.0     146.0     100.0   1.02
E_begin   1.436  1.612   0.017    4.310  ...   101.0     101.0      58.0   1.01

[8 rows x 11 columns]