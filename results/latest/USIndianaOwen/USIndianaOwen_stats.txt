0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -657.37    30.43
p_loo       26.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.053   0.175    0.339  ...    83.0      95.0      95.0   1.03
pu        0.854  0.022   0.807    0.882  ...    45.0      44.0      58.0   1.02
mu        0.138  0.025   0.093    0.182  ...    34.0      43.0      24.0   1.10
mus       0.183  0.037   0.120    0.247  ...    74.0      64.0      59.0   1.01
gamma     0.233  0.050   0.155    0.327  ...   152.0     152.0      95.0   1.02
Is_begin  1.127  1.093   0.000    3.452  ...    98.0      67.0      33.0   0.99
Ia_begin  2.350  2.123   0.002    5.997  ...   108.0     108.0      60.0   1.02
E_begin   0.927  0.921   0.008    2.600  ...    82.0      75.0      80.0   0.98

[8 rows x 11 columns]