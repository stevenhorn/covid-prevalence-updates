0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -540.48    29.78
p_loo       23.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.056   0.172    0.343  ...    58.0      58.0      59.0   1.01
pu        0.876  0.013   0.858    0.895  ...    39.0      34.0      59.0   1.07
mu        0.140  0.032   0.091    0.201  ...    11.0      10.0      39.0   1.16
mus       0.168  0.028   0.109    0.214  ...    69.0      79.0      30.0   1.01
gamma     0.210  0.035   0.159    0.278  ...    76.0      74.0      48.0   0.99
Is_begin  0.826  0.792   0.002    2.149  ...    61.0      69.0      51.0   1.04
Ia_begin  1.543  1.847   0.020    4.591  ...   104.0      80.0     100.0   1.00
E_begin   0.717  0.956   0.003    2.563  ...    88.0      34.0      16.0   1.03

[8 rows x 11 columns]