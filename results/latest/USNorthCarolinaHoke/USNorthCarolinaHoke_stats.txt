0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -905.29    25.86
p_loo       22.41        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      263   89.2%
 (0.5, 0.7]   (ok)         28    9.5%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.049   0.158    0.324  ...    63.0      60.0      36.0   1.02
pu        0.845  0.032   0.781    0.890  ...    44.0      44.0      86.0   1.06
mu        0.113  0.020   0.082    0.156  ...    30.0      21.0      48.0   1.10
mus       0.151  0.027   0.111    0.200  ...   139.0     152.0     100.0   0.99
gamma     0.165  0.035   0.093    0.227  ...   152.0     152.0      45.0   1.01
Is_begin  0.949  0.801   0.081    2.677  ...   152.0     152.0      56.0   1.03
Ia_begin  2.215  1.753   0.085    5.998  ...    87.0      95.0      60.0   1.00
E_begin   1.151  1.099   0.005    3.224  ...    84.0      67.0      60.0   1.02

[8 rows x 11 columns]