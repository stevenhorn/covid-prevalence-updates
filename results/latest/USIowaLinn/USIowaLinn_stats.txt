2 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1252.48    27.86
p_loo       30.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      263   89.2%
 (0.5, 0.7]   (ok)         26    8.8%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.048   0.158    0.326  ...    81.0      83.0      43.0   1.04
pu        0.778  0.046   0.703    0.854  ...    24.0      24.0      32.0   1.05
mu        0.136  0.015   0.112    0.166  ...    41.0      42.0      60.0   1.04
mus       0.236  0.042   0.175    0.321  ...   133.0     137.0      88.0   1.00
gamma     0.344  0.048   0.260    0.422  ...    28.0      28.0      75.0   1.06
Is_begin  1.832  1.236   0.055    3.891  ...    89.0      79.0      58.0   1.01
Ia_begin  0.829  0.593   0.034    1.844  ...   144.0     103.0      55.0   0.98
E_begin   1.944  1.579   0.180    5.205  ...   138.0     125.0     100.0   1.01

[8 rows x 11 columns]