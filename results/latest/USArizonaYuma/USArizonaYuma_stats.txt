0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1505.32    28.85
p_loo       18.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.053   0.152    0.325  ...    83.0      92.0      74.0   1.02
pu        0.730  0.020   0.701    0.764  ...   101.0      88.0      59.0   1.04
mu        0.135  0.019   0.107    0.173  ...    29.0      24.0      69.0   1.06
mus       0.157  0.028   0.114    0.206  ...    96.0      88.0      69.0   0.99
gamma     0.174  0.031   0.126    0.237  ...    70.0      38.0     100.0   1.05
Is_begin  0.706  0.613   0.010    1.916  ...   147.0     152.0      96.0   1.00
Ia_begin  1.267  1.409   0.004    4.451  ...   114.0     152.0      61.0   1.01
E_begin   0.517  0.521   0.014    1.440  ...   100.0      92.0      40.0   1.01

[8 rows x 11 columns]