0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1033.58    32.65
p_loo       27.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.047   0.171    0.321  ...   152.0     152.0      91.0   1.01
pu        0.838  0.021   0.794    0.872  ...    56.0      48.0      60.0   1.04
mu        0.114  0.023   0.084    0.166  ...    14.0      13.0      54.0   1.16
mus       0.168  0.035   0.092    0.218  ...    78.0      82.0      59.0   1.02
gamma     0.211  0.039   0.144    0.291  ...   152.0     152.0      65.0   0.98
Is_begin  0.647  0.784   0.012    1.811  ...   114.0     134.0      58.0   0.99
Ia_begin  1.352  1.547   0.018    4.878  ...    85.0      95.0      59.0   0.99
E_begin   0.571  0.747   0.001    1.991  ...    53.0      62.0      42.0   0.99

[8 rows x 11 columns]