0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -981.66    42.31
p_loo       38.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.057   0.154    0.329  ...   109.0      96.0      60.0   1.00
pu        0.740  0.031   0.701    0.794  ...    42.0      42.0      43.0   1.01
mu        0.122  0.023   0.091    0.178  ...    13.0      16.0      56.0   1.11
mus       0.170  0.032   0.117    0.229  ...    43.0      53.0      61.0   1.03
gamma     0.194  0.033   0.147    0.260  ...    39.0      40.0      31.0   1.03
Is_begin  0.391  0.393   0.001    1.122  ...    91.0      76.0      59.0   0.99
Ia_begin  0.624  0.610   0.000    1.623  ...    57.0      54.0      57.0   1.05
E_begin   0.361  0.352   0.002    1.035  ...    94.0      70.0      60.0   1.00

[8 rows x 11 columns]