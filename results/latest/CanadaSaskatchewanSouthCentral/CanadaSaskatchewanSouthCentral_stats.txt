0 Divergences 
Passed validation 
Computed from 80 by 166 log-likelihood matrix

         Estimate       SE
elpd_loo  -400.14    20.88
p_loo       20.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      147   88.6%
 (0.5, 0.7]   (ok)         16    9.6%
   (0.7, 1]   (bad)         1    0.6%
   (1, Inf)   (very bad)    2    1.2%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.244  0.055   0.151    0.332  ...    20.0      21.0      33.0   1.09
pu         0.820  0.060   0.725    0.897  ...    19.0      20.0      75.0   1.05
mu         0.162  0.034   0.121    0.233  ...    17.0      21.0      40.0   1.07
mus        0.213  0.038   0.145    0.278  ...    39.0      42.0      59.0   1.02
gamma      0.290  0.062   0.203    0.412  ...    50.0      43.0      69.0   1.00
Is_begin  13.869  4.784   5.716   21.823  ...    21.0      23.0      39.0   1.07
Ia_begin   2.532  2.032   0.039    6.684  ...    78.0      65.0      66.0   1.01
E_begin    6.667  4.670   0.243   15.673  ...    95.0      81.0      66.0   1.01

[8 rows x 11 columns]