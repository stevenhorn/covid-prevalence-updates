0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -633.15    25.58
p_loo       20.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      256   86.8%
 (0.5, 0.7]   (ok)         36   12.2%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.049   0.176    0.342  ...   104.0     101.0     100.0   0.99
pu        0.849  0.020   0.821    0.886  ...    19.0      18.0      38.0   1.10
mu        0.124  0.025   0.077    0.165  ...    14.0      19.0      17.0   1.09
mus       0.164  0.033   0.111    0.222  ...   152.0     152.0      60.0   1.00
gamma     0.191  0.039   0.130    0.255  ...    46.0      87.0      66.0   1.02
Is_begin  0.435  0.484   0.004    1.474  ...    52.0      28.0      55.0   1.08
Ia_begin  0.824  0.881   0.004    2.889  ...    99.0      51.0      57.0   1.02
E_begin   0.339  0.408   0.009    0.904  ...    97.0      71.0      58.0   1.01

[8 rows x 11 columns]