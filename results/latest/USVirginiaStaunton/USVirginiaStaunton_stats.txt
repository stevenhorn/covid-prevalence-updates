0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -866.46    58.50
p_loo       34.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.049   0.178    0.342  ...    64.0      64.0      35.0   1.00
pu        0.851  0.023   0.807    0.887  ...    57.0      53.0      60.0   1.07
mu        0.122  0.020   0.083    0.156  ...    71.0      70.0      60.0   0.99
mus       0.171  0.035   0.113    0.242  ...   119.0     100.0      75.0   0.99
gamma     0.217  0.048   0.150    0.305  ...    93.0      89.0      59.0   1.00
Is_begin  0.579  0.577   0.000    1.758  ...    84.0      78.0      69.0   1.01
Ia_begin  1.587  1.564   0.017    4.946  ...    70.0      91.0      59.0   0.99
E_begin   0.597  0.540   0.004    1.754  ...    81.0      76.0      88.0   1.03

[8 rows x 11 columns]