0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -644.11    32.30
p_loo       24.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.057   0.168    0.347  ...    67.0      68.0      38.0   1.06
pu        0.815  0.025   0.773    0.852  ...    48.0      52.0      96.0   0.99
mu        0.119  0.020   0.082    0.157  ...    34.0      39.0      40.0   1.06
mus       0.164  0.027   0.124    0.222  ...   152.0     142.0      76.0   1.00
gamma     0.207  0.039   0.143    0.278  ...   152.0     152.0      60.0   0.99
Is_begin  0.737  0.834   0.014    2.441  ...    83.0      54.0      59.0   1.03
Ia_begin  1.132  1.073   0.011    3.174  ...    86.0      50.0      34.0   1.03
E_begin   0.511  0.494   0.009    1.409  ...    44.0      41.0      74.0   1.07

[8 rows x 11 columns]