0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -862.42    29.94
p_loo       24.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      259   87.8%
 (0.5, 0.7]   (ok)         29    9.8%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.269  0.049   0.187    0.340  ...   122.0      96.0      59.0   1.03
pu        0.880  0.022   0.842    0.900  ...    64.0      54.0      93.0   1.03
mu        0.118  0.025   0.072    0.159  ...    22.0      22.0      53.0   1.04
mus       0.153  0.035   0.092    0.219  ...    81.0      81.0      91.0   1.01
gamma     0.170  0.036   0.110    0.233  ...   152.0     152.0      59.0   0.99
Is_begin  0.987  1.001   0.009    2.865  ...   106.0      89.0      58.0   1.02
Ia_begin  2.540  2.475   0.011    6.783  ...    79.0      69.0      60.0   1.00
E_begin   1.362  1.463   0.009    4.393  ...    83.0      69.0      54.0   0.99

[8 rows x 11 columns]