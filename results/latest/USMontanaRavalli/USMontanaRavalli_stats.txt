0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -846.46    46.09
p_loo       26.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.050   0.158    0.330  ...    35.0      38.0      43.0   1.04
pu        0.876  0.020   0.831    0.900  ...    12.0       7.0      17.0   1.24
mu        0.125  0.020   0.087    0.162  ...    11.0      10.0      39.0   1.18
mus       0.192  0.043   0.121    0.271  ...   105.0     132.0      77.0   1.01
gamma     0.217  0.047   0.156    0.311  ...   102.0     105.0      58.0   0.99
Is_begin  0.993  0.705   0.067    2.264  ...    57.0      42.0      59.0   1.04
Ia_begin  1.987  1.927   0.159    6.060  ...    48.0      77.0      60.0   1.03
E_begin   1.051  1.153   0.006    3.373  ...    53.0      52.0      33.0   1.02

[8 rows x 11 columns]