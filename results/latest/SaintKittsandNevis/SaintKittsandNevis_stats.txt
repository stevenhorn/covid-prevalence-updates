0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo  -170.96    50.53
p_loo       85.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      265   89.5%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.051   0.175    0.349  ...    61.0      64.0      59.0   1.02
pu        0.816  0.044   0.741    0.900  ...    42.0      42.0      47.0   1.04
mu        0.154  0.028   0.111    0.202  ...    42.0      49.0      40.0   1.05
mus       0.226  0.041   0.158    0.298  ...    62.0      63.0      59.0   1.00
gamma     0.261  0.047   0.174    0.322  ...    40.0      40.0      40.0   1.04
Is_begin  1.028  0.925   0.043    2.494  ...    63.0      24.0      31.0   1.07
Ia_begin  2.336  2.056   0.097    5.991  ...    96.0      73.0      69.0   1.05
E_begin   1.028  1.112   0.032    2.704  ...   101.0      78.0     100.0   1.01

[8 rows x 11 columns]