0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -965.17    25.13
p_loo       22.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.046   0.165    0.326  ...    56.0      58.0     100.0   1.01
pu        0.823  0.026   0.767    0.868  ...    13.0      10.0      20.0   1.16
mu        0.139  0.016   0.115    0.166  ...    23.0      21.0      72.0   1.06
mus       0.174  0.037   0.115    0.229  ...    11.0      11.0      43.0   1.15
gamma     0.217  0.037   0.143    0.286  ...    18.0      13.0      38.0   1.13
Is_begin  1.009  0.945   0.013    2.691  ...    87.0      66.0      57.0   1.01
Ia_begin  2.146  1.983   0.110    6.472  ...    77.0      51.0      60.0   1.03
E_begin   1.063  0.958   0.011    2.802  ...    76.0      45.0     100.0   1.00

[8 rows x 11 columns]