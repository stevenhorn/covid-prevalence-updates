0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -596.75    29.79
p_loo       30.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.279  0.051   0.189    0.349  ...    77.0      59.0      37.0   1.03
pu        0.890  0.010   0.865    0.900  ...    83.0      47.0      60.0   0.99
mu        0.162  0.024   0.112    0.195  ...    41.0      39.0      60.0   0.99
mus       0.216  0.046   0.151    0.301  ...    33.0      48.0      60.0   1.04
gamma     0.324  0.055   0.217    0.406  ...   152.0     152.0      97.0   1.00
Is_begin  1.682  1.189   0.028    3.627  ...    75.0      61.0      74.0   1.02
Ia_begin  3.726  2.414   0.318    7.315  ...    86.0      73.0      54.0   1.01
E_begin   2.055  1.828   0.075    4.957  ...    54.0     116.0      45.0   1.01

[8 rows x 11 columns]