0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1509.45    33.40
p_loo       25.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.241   0.047   0.163    0.329  ...   135.0     141.0      44.0   1.07
pu         0.772   0.043   0.700    0.841  ...    76.0      88.0      58.0   1.00
mu         0.131   0.023   0.097    0.180  ...    67.0      62.0      88.0   1.02
mus        0.198   0.033   0.135    0.253  ...    80.0      87.0      79.0   1.02
gamma      0.272   0.044   0.201    0.344  ...   144.0     115.0     100.0   0.98
Is_begin   5.957   4.120   0.130   13.163  ...    40.0      31.0      22.0   1.05
Ia_begin  16.376  12.228   0.360   37.068  ...    69.0      54.0      20.0   1.11
E_begin   15.413  11.872   0.274   36.465  ...    64.0      99.0      79.0   1.04

[8 rows x 11 columns]