0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -723.91    21.10
p_loo       23.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.049   0.173    0.340  ...    59.0      64.0      69.0   1.09
pu        0.871  0.019   0.835    0.898  ...    31.0      31.0      42.0   1.06
mu        0.128  0.024   0.089    0.178  ...    10.0      10.0      44.0   1.13
mus       0.183  0.036   0.131    0.247  ...    60.0      54.0      59.0   1.00
gamma     0.211  0.039   0.154    0.283  ...    68.0      73.0      96.0   1.00
Is_begin  0.926  0.712   0.013    2.330  ...    87.0      79.0      56.0   1.02
Ia_begin  1.796  1.750   0.043    4.934  ...   121.0     139.0      96.0   1.01
E_begin   0.880  0.841   0.051    2.431  ...    66.0      52.0      60.0   1.03

[8 rows x 11 columns]