0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -927.98    25.85
p_loo       22.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.225  0.054   0.154    0.342  ...    76.0     129.0      36.0   1.03
pu        0.727  0.025   0.701    0.772  ...    94.0      68.0      68.0   1.03
mu        0.137  0.024   0.103    0.184  ...    17.0      16.0      73.0   1.10
mus       0.207  0.037   0.140    0.268  ...   118.0     152.0      40.0   0.99
gamma     0.300  0.051   0.211    0.393  ...   152.0     152.0      62.0   1.02
Is_begin  2.513  1.422   0.294    5.198  ...   121.0      97.0      33.0   1.06
Ia_begin  0.883  0.617   0.027    2.089  ...   123.0     100.0      83.0   1.00
E_begin   2.992  2.932   0.007    8.655  ...    76.0      60.0      55.0   1.06

[8 rows x 11 columns]