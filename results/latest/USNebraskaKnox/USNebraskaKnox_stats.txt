0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -600.30    36.77
p_loo       29.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.048   0.174    0.342  ...   152.0     152.0      86.0   0.98
pu        0.813  0.023   0.762    0.842  ...    91.0      85.0      79.0   1.02
mu        0.114  0.022   0.083    0.164  ...    18.0      14.0      56.0   1.11
mus       0.183  0.037   0.130    0.252  ...   125.0     152.0      35.0   1.00
gamma     0.221  0.035   0.159    0.283  ...    89.0      82.0      91.0   1.03
Is_begin  0.820  0.810   0.004    2.243  ...   124.0     135.0      60.0   0.99
Ia_begin  1.103  1.249   0.001    3.209  ...    54.0      16.0      24.0   1.11
E_begin   0.610  1.104   0.002    1.756  ...    76.0      77.0      60.0   1.02

[8 rows x 11 columns]