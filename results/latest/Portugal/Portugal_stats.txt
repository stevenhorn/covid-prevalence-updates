34 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -2060.61    35.65
p_loo       31.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      252   85.1%
 (0.5, 0.7]   (ok)         31   10.5%
   (0.7, 1]   (bad)         9    3.0%
   (1, Inf)   (very bad)    4    1.4%

              mean       sd   hdi_3%  ...  ess_bulk  ess_tail  r_hat
pa           0.214    0.053    0.151  ...       9.0      51.0   1.16
pu           0.754    0.035    0.702  ...       6.0      50.0   1.30
mu           0.139    0.024    0.098  ...       4.0      38.0   1.69
mus          0.187    0.022    0.154  ...      46.0      33.0   1.04
gamma        0.274    0.042    0.199  ...      90.0      40.0   0.99
Is_begin   378.908  168.934   50.555  ...      18.0      60.0   1.10
Ia_begin   881.193  290.714  272.192  ...      61.0      29.0   1.04
E_begin   1291.869  637.701  132.902  ...      57.0      47.0   1.17

[8 rows x 11 columns]