0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -612.66    33.80
p_loo       24.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.051   0.179    0.350  ...    58.0      60.0      40.0   1.04
pu        0.853  0.035   0.788    0.896  ...    13.0      19.0      56.0   1.09
mu        0.120  0.024   0.081    0.155  ...    18.0      17.0      46.0   1.09
mus       0.158  0.030   0.114    0.216  ...   152.0     152.0      97.0   1.00
gamma     0.185  0.038   0.122    0.248  ...   114.0     152.0      68.0   1.01
Is_begin  0.433  0.399   0.049    1.258  ...    36.0      31.0      45.0   1.04
Ia_begin  0.633  0.939   0.017    1.692  ...    92.0      61.0      57.0   1.02
E_begin   0.363  0.347   0.005    0.979  ...    48.0      44.0      60.0   1.02

[8 rows x 11 columns]