1 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo  -697.70    30.29
p_loo       31.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      284   91.9%
 (0.5, 0.7]   (ok)         20    6.5%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.6%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.279   0.046   0.187    0.341  ...    52.0      33.0      69.0   1.05
pu         0.881   0.017   0.852    0.900  ...    62.0      43.0      43.0   1.04
mu         0.150   0.025   0.105    0.187  ...     8.0       8.0      53.0   1.24
mus        0.184   0.026   0.130    0.226  ...    21.0      23.0      59.0   1.08
gamma      0.267   0.040   0.210    0.345  ...    94.0     109.0      55.0   1.01
Is_begin   7.121   5.479   0.440   17.716  ...    99.0      95.0      57.0   1.00
Ia_begin  62.302  27.566  17.627  112.020  ...    57.0      57.0      97.0   1.13
E_begin   37.644  25.426   1.231   84.715  ...   106.0      91.0      96.0   1.00

[8 rows x 11 columns]