0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -914.19    47.18
p_loo       32.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.232  0.042   0.151    0.309  ...    38.0      37.0      40.0   1.02
pu        0.753  0.039   0.701    0.827  ...    29.0      37.0      14.0   1.05
mu        0.134  0.026   0.092    0.179  ...    14.0      15.0      59.0   1.14
mus       0.224  0.033   0.164    0.275  ...    24.0      26.0      58.0   1.06
gamma     0.294  0.055   0.191    0.391  ...   152.0     152.0      96.0   0.99
Is_begin  1.269  1.037   0.082    3.469  ...    50.0      48.0      52.0   1.05
Ia_begin  2.374  2.351   0.016    6.647  ...    39.0      17.0      16.0   1.07
E_begin   2.030  2.123   0.047    6.076  ...    48.0      55.0      59.0   1.01

[8 rows x 11 columns]