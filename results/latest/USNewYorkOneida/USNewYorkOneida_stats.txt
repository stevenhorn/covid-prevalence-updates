0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1118.78    24.90
p_loo       23.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)          7    2.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.058   0.162    0.340  ...    58.0      53.0      57.0   1.05
pu        0.761  0.039   0.702    0.823  ...   102.0      97.0      43.0   1.00
mu        0.117  0.020   0.083    0.146  ...    25.0      24.0      54.0   1.06
mus       0.164  0.035   0.109    0.224  ...    99.0     127.0      60.0   1.19
gamma     0.225  0.041   0.170    0.308  ...   106.0     137.0      86.0   0.98
Is_begin  0.873  0.583   0.011    1.913  ...   152.0     140.0      55.0   0.99
Ia_begin  1.648  1.244   0.003    4.206  ...    46.0      43.0      84.0   1.02
E_begin   1.641  1.804   0.046    5.008  ...    64.0      51.0      58.0   1.04

[8 rows x 11 columns]