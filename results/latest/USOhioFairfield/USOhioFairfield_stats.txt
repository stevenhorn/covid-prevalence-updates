1 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1122.76    32.19
p_loo       26.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.055   0.159    0.341  ...    64.0      54.0      32.0   1.07
pu        0.810  0.046   0.727    0.878  ...    37.0      42.0      24.0   1.05
mu        0.107  0.020   0.075    0.146  ...     8.0       9.0      33.0   1.20
mus       0.158  0.034   0.097    0.222  ...    48.0      49.0      60.0   1.02
gamma     0.181  0.031   0.139    0.233  ...    99.0      85.0      97.0   1.05
Is_begin  1.117  0.919   0.012    2.912  ...    39.0      16.0      59.0   1.12
Ia_begin  3.558  2.460   0.421    7.142  ...    61.0      57.0      59.0   1.00
E_begin   1.594  1.570   0.003    5.065  ...    50.0      29.0      14.0   1.07

[8 rows x 11 columns]