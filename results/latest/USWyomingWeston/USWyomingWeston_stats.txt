0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -639.85    56.55
p_loo       37.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.056   0.162    0.340  ...    89.0      90.0      59.0   1.02
pu        0.805  0.027   0.761    0.864  ...    25.0      23.0      69.0   1.08
mu        0.117  0.022   0.083    0.156  ...    62.0      64.0      59.0   1.00
mus       0.202  0.035   0.149    0.269  ...    60.0      60.0      39.0   1.14
gamma     0.260  0.039   0.199    0.331  ...    90.0      89.0      60.0   0.99
Is_begin  0.445  0.592   0.004    1.820  ...    83.0      39.0      60.0   1.06
Ia_begin  0.808  0.936   0.025    2.759  ...    53.0      23.0      91.0   1.08
E_begin   0.357  0.505   0.006    1.165  ...    50.0      36.0      51.0   1.05

[8 rows x 11 columns]