0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -1718.04    19.73
p_loo       25.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   90.9%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.242   0.059   0.159    0.350  ...   141.0     152.0      24.0   1.18
pu         0.763   0.048   0.700    0.854  ...    28.0      58.0      43.0   1.09
mu         0.104   0.018   0.075    0.129  ...    29.0      25.0      59.0   1.09
mus        0.177   0.031   0.125    0.247  ...   152.0     152.0      64.0   1.09
gamma      0.262   0.041   0.209    0.356  ...   103.0     148.0      88.0   1.02
Is_begin  18.568  10.084   1.995   38.258  ...   152.0     150.0      55.0   1.04
Ia_begin  44.855  19.439  13.384   80.091  ...    93.0      83.0      41.0   0.98
E_begin   60.805  40.175   6.602  149.040  ...   136.0     152.0      95.0   1.00

[8 rows x 11 columns]