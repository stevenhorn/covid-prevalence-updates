0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1596.85    30.44
p_loo       22.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

             mean      sd   hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.216   0.045    0.152    0.295  ...   149.0     152.0      81.0   1.01
pu          0.728   0.021    0.702    0.767  ...   124.0     129.0      80.0   0.98
mu          0.118   0.019    0.092    0.164  ...    67.0      68.0      60.0   1.00
mus         0.186   0.026    0.153    0.238  ...   152.0     152.0      47.0   1.00
gamma       0.303   0.046    0.233    0.397  ...    79.0      77.0      57.0   1.00
Is_begin   36.231  18.066    0.231   65.127  ...    72.0      73.0      19.0   1.02
Ia_begin  113.183  30.670   60.069  160.075  ...   100.0      83.0      83.0   0.99
E_begin   284.255  97.609  108.253  436.724  ...    80.0      78.0      96.0   1.04

[8 rows x 11 columns]