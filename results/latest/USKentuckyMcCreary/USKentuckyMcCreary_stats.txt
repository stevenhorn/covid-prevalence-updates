0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -679.65    31.78
p_loo       21.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         25    8.5%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.047   0.181    0.333  ...    46.0      49.0      84.0   1.03
pu        0.883  0.010   0.863    0.897  ...    17.0      18.0      57.0   1.16
mu        0.112  0.021   0.078    0.148  ...    10.0      11.0      39.0   1.15
mus       0.176  0.030   0.135    0.240  ...    46.0      49.0      59.0   1.06
gamma     0.247  0.046   0.167    0.330  ...    54.0      47.0      49.0   1.03
Is_begin  0.887  0.899   0.022    2.517  ...    61.0      51.0      39.0   1.01
Ia_begin  1.403  1.524   0.047    4.058  ...    22.0      16.0      16.0   1.12
E_begin   0.627  0.706   0.029    1.942  ...    43.0      39.0      43.0   1.02

[8 rows x 11 columns]