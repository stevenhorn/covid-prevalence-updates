0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -873.23    26.30
p_loo       25.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.287  0.048   0.205    0.349  ...    79.0      77.0      40.0   1.01
pu        0.891  0.009   0.869    0.900  ...    89.0      70.0      36.0   1.02
mu        0.167  0.024   0.123    0.202  ...    11.0      10.0      40.0   1.20
mus       0.173  0.028   0.127    0.226  ...   136.0     138.0      88.0   1.03
gamma     0.232  0.040   0.177    0.319  ...   104.0     107.0      84.0   0.99
Is_begin  0.948  0.860   0.000    2.603  ...    74.0      67.0      40.0   1.00
Ia_begin  1.733  1.792   0.003    6.179  ...    73.0      45.0      59.0   1.06
E_begin   0.811  0.894   0.019    2.947  ...    64.0      16.0      88.0   1.12

[8 rows x 11 columns]