0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -836.68    29.77
p_loo       26.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      285   96.6%
 (0.5, 0.7]   (ok)          6    2.0%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.203  0.037   0.157    0.281  ...     4.0       5.0      24.0   1.52
pu        0.715  0.017   0.701    0.745  ...     4.0       4.0      22.0   1.79
mu        0.174  0.025   0.123    0.206  ...     3.0       3.0      21.0   2.37
mus       0.144  0.022   0.116    0.188  ...     5.0       6.0      24.0   1.32
gamma     0.141  0.031   0.080    0.186  ...     3.0       3.0      14.0   2.07
Is_begin  0.378  0.269   0.051    0.898  ...     8.0       9.0      38.0   1.18
Ia_begin  0.825  0.736   0.063    2.383  ...     9.0       6.0      24.0   1.36
E_begin   0.364  0.269   0.016    0.764  ...    25.0      17.0      37.0   1.09

[8 rows x 11 columns]