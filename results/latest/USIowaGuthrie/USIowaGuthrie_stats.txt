0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -656.71    22.81
p_loo       18.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.054   0.164    0.331  ...    97.0      89.0      51.0   1.00
pu        0.813  0.032   0.746    0.864  ...    23.0      34.0      25.0   1.06
mu        0.124  0.024   0.074    0.163  ...    62.0      62.0      48.0   1.03
mus       0.175  0.031   0.117    0.230  ...   152.0     152.0      77.0   0.98
gamma     0.204  0.040   0.136    0.266  ...   100.0     102.0      93.0   1.00
Is_begin  0.633  0.583   0.014    1.666  ...   106.0      71.0      59.0   1.01
Ia_begin  1.145  1.360   0.030    3.317  ...   105.0     152.0      88.0   1.00
E_begin   0.646  1.093   0.003    1.896  ...    94.0      81.0      59.0   1.03

[8 rows x 11 columns]