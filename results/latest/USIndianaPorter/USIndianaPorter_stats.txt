0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1115.52    23.57
p_loo       26.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      262   88.8%
 (0.5, 0.7]   (ok)         25    8.5%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.046   0.180    0.342  ...    97.0     101.0      59.0   1.10
pu        0.774  0.042   0.709    0.846  ...    59.0      62.0      59.0   0.99
mu        0.112  0.020   0.084    0.154  ...     8.0       7.0      60.0   1.28
mus       0.176  0.029   0.116    0.224  ...    74.0      81.0      58.0   1.04
gamma     0.219  0.036   0.161    0.281  ...   116.0     120.0      99.0   1.00
Is_begin  1.593  1.400   0.035    4.062  ...    91.0      66.0      65.0   1.01
Ia_begin  4.644  2.818   0.138    9.132  ...    69.0      64.0      43.0   1.02
E_begin   3.661  2.918   0.038    9.713  ...    75.0      54.0      59.0   1.04

[8 rows x 11 columns]