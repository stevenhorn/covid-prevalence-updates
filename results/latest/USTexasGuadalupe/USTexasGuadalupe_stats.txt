0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1467.77    55.84
p_loo       48.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    4    1.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.053   0.174    0.347  ...   103.0      97.0      43.0   1.08
pu        0.844  0.027   0.790    0.888  ...    45.0      60.0      42.0   1.10
mu        0.149  0.019   0.115    0.188  ...    12.0      12.0      56.0   1.15
mus       0.240  0.042   0.167    0.315  ...   115.0     102.0      80.0   1.02
gamma     0.419  0.077   0.302    0.580  ...    62.0      58.0      60.0   1.01
Is_begin  1.059  0.904   0.063    2.741  ...    93.0      98.0      84.0   1.02
Ia_begin  2.307  2.079   0.104    6.543  ...   143.0     108.0      59.0   1.00
E_begin   0.900  0.763   0.002    2.302  ...   113.0      79.0      42.0   1.03

[8 rows x 11 columns]