0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1621.26    39.01
p_loo       27.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.056   0.166    0.339  ...    64.0      54.0      38.0   1.04
pu        0.774  0.036   0.702    0.819  ...    37.0      40.0      22.0   1.01
mu        0.100  0.016   0.075    0.129  ...     7.0       7.0      43.0   1.29
mus       0.154  0.025   0.108    0.208  ...   125.0     124.0      81.0   0.99
gamma     0.201  0.031   0.126    0.247  ...   152.0     152.0      83.0   0.99
Is_begin  1.139  0.828   0.111    2.838  ...    95.0      73.0      60.0   0.98
Ia_begin  2.882  2.558   0.200    7.993  ...    67.0      70.0      43.0   1.02
E_begin   1.812  1.525   0.031    5.224  ...    84.0      75.0      60.0   1.04

[8 rows x 11 columns]