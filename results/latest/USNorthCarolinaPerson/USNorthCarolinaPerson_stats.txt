0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -767.26    27.17
p_loo       24.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.057   0.170    0.350  ...   150.0     152.0      57.0   1.08
pu        0.850  0.039   0.776    0.895  ...    34.0      49.0      20.0   1.05
mu        0.104  0.018   0.070    0.134  ...    10.0      11.0      66.0   1.17
mus       0.156  0.035   0.100    0.243  ...    49.0     122.0      45.0   1.10
gamma     0.167  0.036   0.110    0.229  ...   152.0     152.0      97.0   0.99
Is_begin  0.755  0.682   0.013    2.252  ...   152.0     135.0      72.0   0.99
Ia_begin  1.469  1.597   0.004    5.210  ...    70.0      62.0      59.0   0.99
E_begin   0.659  0.835   0.006    2.261  ...   104.0      72.0      91.0   1.00

[8 rows x 11 columns]