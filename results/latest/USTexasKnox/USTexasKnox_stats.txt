0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -605.79    64.22
p_loo       62.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.6%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    4    1.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.051   0.180    0.350  ...    61.0      66.0      35.0   1.04
pu        0.744  0.030   0.702    0.792  ...    15.0      12.0      43.0   1.16
mu        0.187  0.037   0.110    0.239  ...     8.0       8.0      60.0   1.25
mus       0.255  0.054   0.168    0.358  ...     9.0       9.0      56.0   1.20
gamma     0.355  0.062   0.252    0.480  ...    15.0      16.0      66.0   1.11
Is_begin  0.757  0.778   0.023    2.137  ...    87.0      75.0      59.0   0.98
Ia_begin  1.336  1.182   0.003    3.329  ...    75.0      64.0      15.0   1.00
E_begin   0.635  0.709   0.013    1.672  ...    77.0      63.0      58.0   1.01

[8 rows x 11 columns]