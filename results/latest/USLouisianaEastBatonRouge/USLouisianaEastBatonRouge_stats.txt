0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1657.93    33.84
p_loo       32.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    3    1.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.241   0.050   0.173    0.343  ...    31.0      30.0      22.0   1.05
pu         0.764   0.049   0.700    0.857  ...    63.0      68.0      15.0   1.09
mu         0.120   0.019   0.083    0.148  ...    10.0      10.0      57.0   1.18
mus        0.192   0.035   0.141    0.269  ...    32.0      30.0      55.0   1.03
gamma      0.287   0.043   0.222    0.362  ...   117.0     102.0      84.0   1.02
Is_begin   4.498   3.252   0.012   10.227  ...    31.0      17.0      14.0   1.09
Ia_begin  15.347   6.427   4.273   26.512  ...    47.0      47.0      57.0   1.05
E_begin   17.305  13.456   0.246   46.550  ...    49.0      41.0      40.0   1.03

[8 rows x 11 columns]