0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -1409.10    55.05
p_loo       88.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      288   97.3%
 (0.5, 0.7]   (ok)          4    1.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.289  0.052   0.200    0.338  ...     3.0       3.0      14.0   1.99
pu        0.890  0.005   0.885    0.898  ...     3.0       3.0      17.0   2.37
mu        0.224  0.003   0.220    0.231  ...     5.0       6.0      31.0   1.60
mus       0.263  0.015   0.231    0.287  ...     6.0       6.0      27.0   1.28
gamma     0.234  0.015   0.216    0.266  ...     3.0       4.0      33.0   1.71
Is_begin  1.135  0.546   0.266    1.917  ...     3.0       3.0      14.0   1.94
Ia_begin  0.889  0.175   0.608    1.195  ...     6.0       8.0      32.0   1.27
E_begin   1.046  0.581   0.230    1.988  ...     4.0       5.0      15.0   1.45

[8 rows x 11 columns]