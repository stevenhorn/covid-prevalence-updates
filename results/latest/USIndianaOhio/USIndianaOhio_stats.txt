0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -473.50    25.00
p_loo       24.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      284   96.3%
 (0.5, 0.7]   (ok)          9    3.1%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.044   0.181    0.332  ...   111.0     112.0      60.0   1.00
pu        0.834  0.033   0.767    0.881  ...    24.0      23.0      58.0   1.03
mu        0.159  0.031   0.112    0.231  ...    37.0      34.0      60.0   1.05
mus       0.200  0.041   0.123    0.267  ...    66.0      69.0      86.0   1.00
gamma     0.258  0.051   0.173    0.342  ...   110.0     110.0      86.0   1.00
Is_begin  0.674  0.724   0.008    2.209  ...    77.0     101.0      60.0   1.01
Ia_begin  1.376  1.682   0.017    3.412  ...    81.0      79.0      47.0   0.98
E_begin   0.634  0.664   0.002    2.155  ...   137.0      87.0      59.0   1.00

[8 rows x 11 columns]