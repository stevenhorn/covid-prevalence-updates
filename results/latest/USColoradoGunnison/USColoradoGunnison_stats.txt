0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -722.80    36.26
p_loo       34.75        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         24    8.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    2    0.7%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.260  0.053   0.180    0.349  ...    12.0      14.0      59.0   1.12
pu         0.802  0.065   0.705    0.888  ...     3.0       4.0      53.0   1.74
mu         0.188  0.037   0.120    0.249  ...     8.0       8.0      46.0   1.20
mus        0.233  0.040   0.184    0.331  ...    34.0      39.0      20.0   1.05
gamma      0.351  0.069   0.248    0.448  ...    20.0      16.0      56.0   1.11
Is_begin   3.336  2.351   0.091    7.953  ...    22.0      21.0      40.0   1.10
Ia_begin   8.353  4.726   0.447   17.303  ...    25.0      23.0      59.0   1.07
E_begin   10.552  8.461   0.288   26.748  ...     9.0       8.0      39.0   1.24

[8 rows x 11 columns]