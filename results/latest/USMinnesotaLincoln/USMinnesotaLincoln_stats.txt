0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -492.50    25.45
p_loo       26.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.052   0.164    0.334  ...   143.0     119.0      42.0   1.00
pu        0.842  0.018   0.812    0.873  ...    67.0      66.0      96.0   1.01
mu        0.155  0.025   0.116    0.207  ...    43.0      44.0      74.0   1.00
mus       0.202  0.043   0.138    0.298  ...   104.0     133.0      38.0   0.98
gamma     0.262  0.053   0.180    0.369  ...   152.0     152.0      83.0   1.01
Is_begin  0.808  0.627   0.006    2.089  ...    97.0      99.0      39.0   0.99
Ia_begin  1.464  1.515   0.016    4.148  ...    95.0     152.0      57.0   1.04
E_begin   0.625  0.551   0.007    1.980  ...    98.0      88.0      75.0   1.02

[8 rows x 11 columns]