0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1876.08    22.67
p_loo       20.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      260   88.1%
 (0.5, 0.7]   (ok)         30   10.2%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

             mean       sd   hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.212    0.049    0.151    0.308  ...   152.0     135.0      77.0   1.01
pu          0.723    0.022    0.700    0.774  ...   139.0     148.0      40.0   0.99
mu          0.111    0.023    0.069    0.152  ...    15.0      15.0      38.0   1.12
mus         0.198    0.038    0.138    0.282  ...   100.0     152.0      60.0   1.00
gamma       0.370    0.056    0.276    0.465  ...    81.0      88.0      57.0   0.98
Is_begin   45.295   20.218   12.773   87.552  ...    41.0      40.0      22.0   1.04
Ia_begin  133.859   41.061   83.819  235.882  ...   113.0     111.0      60.0   1.02
E_begin   374.048  134.893  119.713  605.957  ...    74.0     105.0      60.0   1.00

[8 rows x 11 columns]