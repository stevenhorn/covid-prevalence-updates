0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -903.09    29.39
p_loo       23.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.056   0.176    0.347  ...    89.0      85.0      42.0   1.01
pu        0.858  0.025   0.815    0.896  ...    18.0      33.0      59.0   1.05
mu        0.135  0.023   0.086    0.162  ...     8.0       8.0      88.0   1.24
mus       0.171  0.028   0.116    0.216  ...   152.0     152.0      96.0   1.05
gamma     0.208  0.046   0.142    0.299  ...   152.0     152.0      80.0   0.99
Is_begin  0.776  0.622   0.010    1.974  ...   102.0      89.0     100.0   0.99
Ia_begin  2.414  2.370   0.067    5.784  ...   106.0     147.0     100.0   1.01
E_begin   1.225  1.248   0.003    3.282  ...    78.0      84.0      69.0   1.01

[8 rows x 11 columns]