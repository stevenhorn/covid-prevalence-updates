0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1018.00    23.37
p_loo       21.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.052   0.160    0.322  ...    38.0      36.0      56.0   1.03
pu        0.837  0.025   0.783    0.878  ...    32.0      23.0      59.0   1.07
mu        0.138  0.021   0.105    0.178  ...     7.0       7.0      30.0   1.23
mus       0.163  0.030   0.120    0.222  ...    54.0      53.0      57.0   0.99
gamma     0.188  0.036   0.123    0.251  ...   100.0      96.0      97.0   1.00
Is_begin  1.376  1.018   0.039    3.505  ...    93.0      70.0      59.0   1.00
Ia_begin  2.911  2.615   0.014    7.529  ...    69.0      51.0      15.0   1.02
E_begin   1.501  1.487   0.021    3.982  ...    91.0      95.0      69.0   0.99

[8 rows x 11 columns]