0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1003.83    31.85
p_loo       22.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      266   90.2%
 (0.5, 0.7]   (ok)         27    9.2%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.061   0.165    0.343  ...    69.0      84.0      56.0   1.08
pu        0.875  0.021   0.832    0.898  ...    12.0      10.0      56.0   1.19
mu        0.137  0.023   0.098    0.173  ...    27.0      28.0      42.0   1.05
mus       0.172  0.035   0.129    0.254  ...   139.0     152.0      48.0   1.08
gamma     0.214  0.042   0.148    0.292  ...   152.0     152.0      96.0   1.02
Is_begin  0.739  0.534   0.041    1.637  ...   115.0      88.0      82.0   1.02
Ia_begin  1.892  1.508   0.013    4.170  ...   107.0      47.0      14.0   1.01
E_begin   1.022  1.036   0.011    3.010  ...    99.0      53.0      38.0   1.01

[8 rows x 11 columns]