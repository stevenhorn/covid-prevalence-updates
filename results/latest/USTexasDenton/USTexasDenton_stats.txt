0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1614.82    45.37
p_loo       29.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.242  0.052   0.156    0.337  ...    71.0      65.0      15.0   1.15
pu         0.803  0.060   0.701    0.883  ...    26.0      26.0      33.0   1.03
mu         0.114  0.018   0.089    0.154  ...     6.0       7.0      46.0   1.28
mus        0.181  0.037   0.115    0.244  ...   106.0      99.0      53.0   1.00
gamma      0.242  0.039   0.180    0.320  ...   123.0     128.0      86.0   0.98
Is_begin   5.122  3.516   0.299   12.091  ...   106.0      79.0      59.0   0.99
Ia_begin  15.808  8.875   0.749   32.523  ...    37.0      32.0      22.0   1.01
E_begin   13.004  9.466   0.477   32.222  ...    43.0      36.0      58.0   1.03

[8 rows x 11 columns]