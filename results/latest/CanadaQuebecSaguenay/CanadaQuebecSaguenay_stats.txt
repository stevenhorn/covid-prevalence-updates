0 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo -1044.48    45.54
p_loo       28.75        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      293   94.8%
 (0.5, 0.7]   (ok)         11    3.6%
   (0.7, 1]   (bad)         4    1.3%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.285   0.057   0.181    0.350  ...    70.0      63.0      39.0   1.02
pu         0.884   0.019   0.845    0.900  ...    71.0      12.0      56.0   1.14
mu         0.162   0.030   0.122    0.224  ...    11.0      11.0      15.0   1.16
mus        0.209   0.028   0.165    0.263  ...   101.0      98.0     100.0   1.06
gamma      0.302   0.044   0.237    0.408  ...   139.0     125.0      79.0   0.99
Is_begin   5.655   4.605   0.028   14.761  ...   125.0      83.0      60.0   1.00
Ia_begin  29.232  22.368   0.376   69.317  ...    70.0      61.0      60.0   1.00
E_begin   16.081  13.014   0.121   40.924  ...    42.0      73.0      97.0   1.03

[8 rows x 11 columns]