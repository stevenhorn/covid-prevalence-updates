0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -545.70    24.19
p_loo       22.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.052   0.173    0.347  ...    96.0     107.0      40.0   1.03
pu        0.820  0.027   0.774    0.862  ...    80.0     113.0      24.0   1.03
mu        0.127  0.033   0.079    0.183  ...    71.0      80.0      79.0   0.99
mus       0.161  0.030   0.116    0.223  ...   152.0     152.0      59.0   1.02
gamma     0.177  0.034   0.130    0.247  ...   152.0     152.0      87.0   0.98
Is_begin  0.635  0.606   0.001    1.709  ...   114.0      99.0      59.0   1.01
Ia_begin  1.328  1.196   0.023    4.102  ...    50.0      85.0      88.0   1.03
E_begin   0.578  0.618   0.004    2.068  ...    84.0      79.0      96.0   1.06

[8 rows x 11 columns]