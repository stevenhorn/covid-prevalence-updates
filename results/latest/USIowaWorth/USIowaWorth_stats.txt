0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -484.66    22.26
p_loo       21.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      263   89.2%
 (0.5, 0.7]   (ok)         24    8.1%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.050   0.181    0.348  ...   103.0     109.0      93.0   0.99
pu        0.836  0.027   0.779    0.875  ...    17.0      20.0      19.0   1.06
mu        0.134  0.034   0.085    0.204  ...    14.0      14.0      56.0   1.12
mus       0.180  0.036   0.111    0.240  ...   152.0     146.0      42.0   1.03
gamma     0.205  0.040   0.124    0.262  ...   105.0      96.0      40.0   1.05
Is_begin  0.579  0.572   0.013    1.607  ...    79.0      90.0      59.0   0.99
Ia_begin  0.758  0.888   0.001    2.623  ...    85.0      55.0      60.0   1.00
E_begin   0.447  0.591   0.001    1.817  ...   101.0      88.0      70.0   0.99

[8 rows x 11 columns]