0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -330.94    26.39
p_loo       25.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.279  0.048   0.182    0.348  ...    18.0      17.0      27.0   1.12
pu        0.867  0.029   0.804    0.899  ...    21.0      16.0      47.0   1.12
mu        0.124  0.022   0.091    0.161  ...    25.0      30.0      59.0   1.06
mus       0.169  0.032   0.115    0.229  ...    70.0      68.0      39.0   0.99
gamma     0.194  0.036   0.129    0.250  ...   102.0     102.0      49.0   1.09
Is_begin  0.359  0.315   0.011    0.884  ...    52.0      54.0      56.0   1.01
Ia_begin  0.599  0.720   0.012    2.198  ...    54.0      49.0      62.0   1.03
E_begin   0.315  0.424   0.006    0.939  ...    82.0      54.0      60.0   1.03

[8 rows x 11 columns]