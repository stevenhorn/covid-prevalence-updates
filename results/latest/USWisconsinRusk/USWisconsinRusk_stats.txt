0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -654.91    33.58
p_loo       25.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      290   98.3%
 (0.5, 0.7]   (ok)          2    0.7%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.047   0.185    0.347  ...    81.0      82.0      60.0   1.01
pu        0.860  0.012   0.836    0.880  ...    49.0      59.0      80.0   1.04
mu        0.127  0.023   0.086    0.164  ...    28.0      30.0      29.0   1.08
mus       0.159  0.034   0.110    0.226  ...    43.0      42.0      47.0   1.02
gamma     0.176  0.040   0.105    0.240  ...    17.0      19.0      57.0   1.10
Is_begin  0.692  0.723   0.006    2.139  ...   126.0      68.0      44.0   1.04
Ia_begin  1.591  1.612   0.032    4.475  ...   115.0      75.0      40.0   1.02
E_begin   0.888  1.052   0.018    3.613  ...    56.0      52.0      60.0   1.00

[8 rows x 11 columns]