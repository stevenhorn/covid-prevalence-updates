0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -814.37    35.01
p_loo       24.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.290  0.053   0.175    0.350  ...   108.0      91.0      40.0   1.01
pu        0.885  0.013   0.855    0.900  ...    79.0      83.0      59.0   0.99
mu        0.122  0.025   0.078    0.169  ...    10.0      10.0      47.0   1.20
mus       0.157  0.032   0.111    0.220  ...   118.0     152.0      58.0   1.02
gamma     0.186  0.032   0.135    0.247  ...    50.0      41.0      60.0   1.04
Is_begin  0.928  0.673   0.022    2.285  ...   138.0     117.0      86.0   0.99
Ia_begin  2.100  1.832   0.016    5.041  ...    88.0     127.0      60.0   1.03
E_begin   0.944  0.971   0.012    2.923  ...    79.0     132.0      66.0   1.02

[8 rows x 11 columns]