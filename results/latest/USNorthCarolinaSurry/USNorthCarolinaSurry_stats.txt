0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -996.60    28.96
p_loo       21.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.050   0.167    0.324  ...    99.0      85.0      60.0   0.98
pu        0.821  0.029   0.767    0.862  ...    66.0      70.0      60.0   1.01
mu        0.117  0.024   0.078    0.154  ...    47.0      47.0      81.0   1.01
mus       0.148  0.028   0.096    0.203  ...   100.0      91.0      59.0   1.00
gamma     0.164  0.032   0.105    0.225  ...   107.0     125.0      96.0   1.15
Is_begin  0.598  0.652   0.006    1.902  ...   105.0      53.0      40.0   1.02
Ia_begin  1.231  1.303   0.032    3.328  ...    67.0      68.0      53.0   1.03
E_begin   0.643  0.716   0.014    2.496  ...    78.0      54.0      25.0   1.04

[8 rows x 11 columns]