0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -894.94    24.97
p_loo       21.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.058   0.150    0.337  ...    46.0      49.0      43.0   1.10
pu        0.816  0.019   0.789    0.849  ...    38.0      46.0      56.0   1.03
mu        0.118  0.016   0.094    0.153  ...    14.0      14.0      22.0   1.14
mus       0.154  0.034   0.100    0.217  ...   127.0     126.0      74.0   0.98
gamma     0.173  0.043   0.115    0.261  ...   108.0      92.0      40.0   1.04
Is_begin  0.589  0.568   0.005    1.629  ...   120.0      60.0      60.0   1.03
Ia_begin  1.007  1.151   0.004    3.177  ...   111.0      76.0      43.0   1.01
E_begin   0.528  0.528   0.005    1.505  ...    72.0      50.0      38.0   1.02

[8 rows x 11 columns]