0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1633.57    61.53
p_loo       36.23        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.051   0.160    0.326  ...   152.0     152.0      40.0   1.02
pu        0.753  0.034   0.701    0.818  ...   124.0     119.0      69.0   0.99
mu        0.140  0.020   0.102    0.174  ...    43.0      38.0      49.0   1.02
mus       0.260  0.050   0.164    0.337  ...    95.0      81.0      59.0   1.01
gamma     0.388  0.056   0.278    0.488  ...   100.0     105.0     100.0   0.99
Is_begin  1.291  1.007   0.009    3.250  ...    65.0      52.0      29.0   1.01
Ia_begin  0.740  0.617   0.018    1.888  ...    89.0      37.0      22.0   1.04
E_begin   1.128  1.447   0.008    4.136  ...   103.0      75.0      93.0   1.00

[8 rows x 11 columns]