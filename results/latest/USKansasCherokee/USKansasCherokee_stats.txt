0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -918.37    26.43
p_loo       23.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.054   0.163    0.342  ...   111.0      99.0      24.0   1.03
pu        0.749  0.025   0.710    0.796  ...    78.0      71.0      40.0   1.06
mu        0.123  0.025   0.083    0.159  ...    54.0      54.0      60.0   1.02
mus       0.165  0.040   0.107    0.241  ...   111.0     134.0      97.0   1.00
gamma     0.186  0.049   0.110    0.292  ...   114.0     127.0      52.0   1.06
Is_begin  0.915  0.972   0.017    2.532  ...   106.0      69.0      88.0   1.04
Ia_begin  1.970  1.972   0.066    5.456  ...   107.0      91.0      99.0   1.00
E_begin   0.766  0.963   0.008    3.024  ...    79.0      82.0      34.0   0.99

[8 rows x 11 columns]