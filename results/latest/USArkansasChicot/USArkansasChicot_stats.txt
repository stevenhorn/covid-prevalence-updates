0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -903.81    46.80
p_loo       38.34        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.170  0.020   0.151    0.202  ...   108.0      51.0      93.0   1.04
pu        0.707  0.008   0.700    0.722  ...   152.0     152.0      46.0   1.03
mu        0.142  0.025   0.096    0.183  ...    37.0      46.0      59.0   1.04
mus       0.210  0.025   0.163    0.251  ...   152.0     142.0      93.0   1.01
gamma     0.309  0.050   0.228    0.395  ...   152.0     152.0      60.0   0.98
Is_begin  0.687  0.587   0.013    1.809  ...    70.0      70.0      81.0   1.02
Ia_begin  1.257  1.300   0.067    3.609  ...    35.0      32.0      37.0   1.07
E_begin   0.481  0.450   0.033    1.391  ...    85.0      87.0      93.0   0.99

[8 rows x 11 columns]