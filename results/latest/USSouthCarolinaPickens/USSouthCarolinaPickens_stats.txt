0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1230.46    39.06
p_loo       29.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.5%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    4    1.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.054   0.151    0.328  ...   100.0      89.0      37.0   1.03
pu        0.757  0.028   0.708    0.798  ...   124.0     126.0      35.0   1.00
mu        0.121  0.026   0.085    0.168  ...     6.0       7.0      91.0   1.31
mus       0.173  0.041   0.106    0.242  ...    82.0     146.0      29.0   1.09
gamma     0.186  0.034   0.134    0.249  ...   152.0     123.0      61.0   1.04
Is_begin  0.805  0.736   0.012    2.020  ...   106.0     117.0      60.0   1.01
Ia_begin  1.508  1.431   0.010    3.757  ...    56.0      77.0      60.0   1.01
E_begin   0.626  0.626   0.013    1.503  ...    74.0      45.0      88.0   1.05

[8 rows x 11 columns]