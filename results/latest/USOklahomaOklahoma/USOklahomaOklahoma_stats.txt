0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1647.01    30.32
p_loo       25.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      273   92.5%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.244  0.047   0.160    0.321  ...   152.0     152.0      80.0   1.03
pu         0.759  0.037   0.701    0.819  ...   152.0     152.0      49.0   1.06
mu         0.120  0.020   0.096    0.167  ...    25.0      25.0      93.0   1.08
mus        0.171  0.028   0.126    0.229  ...   152.0     152.0      73.0   1.00
gamma      0.226  0.038   0.158    0.284  ...   126.0     124.0     100.0   1.03
Is_begin   4.139  2.595   0.104    8.946  ...   149.0     130.0      86.0   0.99
Ia_begin  10.481  6.749   1.175   22.735  ...   152.0     152.0      46.0   1.00
E_begin    7.783  7.594   0.142   19.367  ...   105.0      83.0      60.0   1.01

[8 rows x 11 columns]