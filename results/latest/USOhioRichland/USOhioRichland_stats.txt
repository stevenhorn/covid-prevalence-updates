0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1057.65    50.48
p_loo       32.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      265   89.8%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.267  0.055   0.168    0.349  ...   107.0     110.0      49.0   1.07
pu        0.866  0.021   0.824    0.898  ...    68.0      68.0      59.0   1.01
mu        0.118  0.022   0.079    0.155  ...    19.0      14.0      25.0   1.14
mus       0.161  0.033   0.112    0.223  ...   107.0     102.0      60.0   0.99
gamma     0.183  0.045   0.124    0.267  ...   152.0     152.0      80.0   1.02
Is_begin  1.071  0.959   0.004    2.846  ...    92.0      78.0      60.0   1.00
Ia_begin  2.549  1.954   0.112    5.944  ...    40.0      46.0      60.0   1.02
E_begin   1.455  1.532   0.019    3.674  ...    76.0      64.0      69.0   1.00

[8 rows x 11 columns]