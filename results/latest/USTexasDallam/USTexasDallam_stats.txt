0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -625.13    24.73
p_loo       25.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.054   0.155    0.326  ...    93.0      58.0      20.0   1.03
pu        0.838  0.020   0.803    0.866  ...    30.0      34.0      36.0   1.10
mu        0.126  0.021   0.091    0.169  ...    18.0      15.0      59.0   1.10
mus       0.156  0.035   0.104    0.225  ...   102.0      89.0      72.0   1.00
gamma     0.208  0.042   0.134    0.283  ...    62.0      61.0      60.0   1.00
Is_begin  0.547  0.432   0.002    1.351  ...    83.0      62.0      58.0   1.00
Ia_begin  1.100  1.172   0.006    3.028  ...   102.0     119.0     100.0   1.00
E_begin   0.500  0.511   0.025    1.771  ...   122.0     122.0     100.0   1.04

[8 rows x 11 columns]