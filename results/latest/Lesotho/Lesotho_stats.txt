1 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -1175.67    31.17
p_loo       32.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.6%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.275  0.040   0.205    0.335  ...   144.0     152.0      60.0   1.00
pu        0.875  0.022   0.840    0.899  ...   152.0     152.0      60.0   0.99
mu        0.137  0.028   0.091    0.190  ...    18.0      18.0      36.0   1.05
mus       0.165  0.036   0.104    0.230  ...    76.0     129.0      45.0   1.05
gamma     0.218  0.041   0.158    0.282  ...    86.0      75.0      46.0   1.03
Is_begin  0.381  0.381   0.000    1.173  ...    95.0      79.0      60.0   0.99
Ia_begin  0.687  0.772   0.011    2.059  ...    89.0      43.0      59.0   1.05
E_begin   0.363  0.467   0.000    1.062  ...    69.0      38.0      59.0   1.00

[8 rows x 11 columns]