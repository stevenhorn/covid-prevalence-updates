0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo   277.28    75.81
p_loo       60.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.6%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.269  0.056   0.177    0.350  ...   139.0     152.0      38.0   1.00
pu        0.873  0.026   0.823    0.900  ...   152.0     152.0      51.0   1.01
mu        0.149  0.024   0.111    0.192  ...    78.0      86.0      55.0   1.01
mus       0.202  0.029   0.146    0.243  ...   152.0     152.0      64.0   1.04
gamma     0.216  0.037   0.164    0.293  ...   152.0     152.0      74.0   1.12
Is_begin  0.462  0.374   0.013    1.300  ...   152.0     152.0      61.0   1.02
Ia_begin  0.942  0.927   0.005    2.804  ...    78.0      40.0      67.0   1.02
E_begin   0.384  0.433   0.013    1.497  ...    86.0      24.0      49.0   1.08

[8 rows x 11 columns]