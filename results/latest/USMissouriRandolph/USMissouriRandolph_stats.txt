0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -817.06    41.77
p_loo       32.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.047   0.165    0.332  ...    67.0      76.0      88.0   1.02
pu        0.858  0.018   0.831    0.888  ...    10.0      10.0      17.0   1.18
mu        0.123  0.022   0.091    0.167  ...    28.0      30.0      45.0   1.02
mus       0.162  0.034   0.114    0.231  ...   128.0     145.0      49.0   1.07
gamma     0.180  0.040   0.125    0.270  ...    77.0      73.0      54.0   1.01
Is_begin  0.830  0.668   0.005    2.158  ...    34.0      28.0      59.0   1.07
Ia_begin  1.351  1.099   0.069    3.962  ...    85.0      73.0     100.0   1.02
E_begin   0.783  0.709   0.017    2.050  ...    44.0      27.0      91.0   1.07

[8 rows x 11 columns]