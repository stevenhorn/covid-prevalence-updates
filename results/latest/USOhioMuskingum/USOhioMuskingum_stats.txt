0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -930.99    33.70
p_loo       21.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         22    7.5%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.054   0.170    0.343  ...    63.0      64.0      60.0   1.02
pu        0.839  0.021   0.796    0.871  ...    12.0      13.0      50.0   1.11
mu        0.124  0.026   0.076    0.160  ...     8.0       8.0      60.0   1.24
mus       0.162  0.043   0.094    0.236  ...   100.0     152.0      49.0   1.01
gamma     0.178  0.039   0.118    0.266  ...   152.0     152.0      49.0   1.03
Is_begin  0.804  0.747   0.017    2.280  ...    84.0      60.0      58.0   1.03
Ia_begin  1.742  1.824   0.013    4.732  ...    84.0      59.0      45.0   1.04
E_begin   0.694  0.871   0.003    2.609  ...    29.0      15.0      49.0   1.12

[8 rows x 11 columns]