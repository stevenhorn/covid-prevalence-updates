1 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -971.34    26.16
p_loo       23.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.056   0.167    0.342  ...    58.0      81.0      38.0   1.07
pu        0.812  0.053   0.708    0.883  ...    48.0      42.0      49.0   1.05
mu        0.168  0.026   0.118    0.199  ...    26.0      24.0      60.0   1.08
mus       0.212  0.039   0.152    0.292  ...    48.0      55.0      60.0   1.04
gamma     0.284  0.042   0.204    0.358  ...   152.0     152.0      77.0   0.99
Is_begin  2.579  1.940   0.086    6.228  ...    46.0      34.0      22.0   1.07
Ia_begin  7.157  5.038   0.157   15.811  ...    62.0      69.0      40.0   1.02
E_begin   7.158  7.080   0.046   21.025  ...    56.0      55.0      59.0   1.00

[8 rows x 11 columns]