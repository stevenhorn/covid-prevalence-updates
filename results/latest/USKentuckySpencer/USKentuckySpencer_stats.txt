0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -677.15    25.63
p_loo       21.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      269   91.2%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.052   0.156    0.329  ...   152.0     140.0      59.0   1.00
pu        0.875  0.018   0.839    0.898  ...    49.0      34.0      49.0   1.06
mu        0.126  0.031   0.069    0.182  ...    22.0      22.0      40.0   1.05
mus       0.153  0.031   0.096    0.204  ...   152.0     152.0      67.0   1.00
gamma     0.180  0.038   0.112    0.234  ...   152.0     152.0      79.0   1.00
Is_begin  0.861  0.602   0.084    1.913  ...   110.0     102.0      93.0   1.00
Ia_begin  1.825  1.449   0.017    4.429  ...    44.0      40.0      59.0   1.04
E_begin   0.866  0.834   0.005    2.329  ...    53.0      41.0      74.0   1.04

[8 rows x 11 columns]