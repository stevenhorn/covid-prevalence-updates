0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -1327.60    31.82
p_loo       16.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      282   95.3%
 (0.5, 0.7]   (ok)          9    3.0%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.299  0.020   0.273    0.340  ...    14.0      15.0      16.0   1.12
pu        0.855  0.009   0.836    0.866  ...     3.0       4.0      15.0   1.79
mu        0.168  0.005   0.159    0.175  ...     4.0       4.0      14.0   1.79
mus       0.175  0.017   0.149    0.201  ...     3.0       3.0      14.0   1.93
gamma     0.114  0.009   0.104    0.128  ...     3.0       3.0      14.0   2.27
Is_begin  2.512  1.047   0.814    4.113  ...     4.0       4.0      22.0   1.49
Ia_begin  2.951  1.140   1.081    4.887  ...     7.0       7.0      58.0   1.25
E_begin   1.382  1.180   0.107    3.648  ...     4.0       3.0      20.0   2.00

[8 rows x 11 columns]