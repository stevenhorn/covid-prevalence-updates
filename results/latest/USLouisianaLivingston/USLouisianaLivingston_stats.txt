0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1320.85    30.26
p_loo       25.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      254   86.1%
 (0.5, 0.7]   (ok)         31   10.5%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.280  0.045   0.195    0.349  ...     7.0       7.0      22.0   1.28
pu        0.837  0.032   0.748    0.884  ...     5.0       6.0      15.0   1.37
mu        0.181  0.027   0.124    0.212  ...     4.0       5.0      15.0   1.55
mus       0.164  0.031   0.117    0.231  ...    10.0      13.0      20.0   1.21
gamma     0.189  0.031   0.136    0.254  ...    24.0      20.0      37.0   1.07
Is_begin  1.443  0.941   0.032    3.284  ...    16.0      19.0      34.0   1.07
Ia_begin  0.779  0.677   0.004    1.889  ...    17.0      10.0      17.0   1.18
E_begin   1.030  1.067   0.014    3.513  ...    14.0      22.0      14.0   1.07

[8 rows x 11 columns]