0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -771.60    33.92
p_loo       27.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      284   96.3%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.234  0.051   0.150    0.328  ...    67.0      70.0      39.0   1.03
pu        0.733  0.019   0.702    0.762  ...    52.0      51.0      59.0   1.00
mu        0.150  0.026   0.106    0.202  ...    15.0      22.0      42.0   1.09
mus       0.176  0.035   0.106    0.245  ...   100.0     116.0      91.0   1.03
gamma     0.175  0.041   0.114    0.244  ...    57.0      57.0      60.0   1.02
Is_begin  0.712  0.641   0.006    1.922  ...    71.0      59.0      60.0   0.99
Ia_begin  1.330  1.556   0.007    4.764  ...    43.0      23.0      59.0   1.07
E_begin   0.436  0.464   0.005    1.119  ...    35.0      25.0      56.0   1.09

[8 rows x 11 columns]