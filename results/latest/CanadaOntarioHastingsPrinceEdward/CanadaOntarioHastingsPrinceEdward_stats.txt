0 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo  -484.18    26.33
p_loo       23.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      289   93.5%
 (0.5, 0.7]   (ok)         16    5.2%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.288   0.045   0.208    0.349  ...    87.0      79.0      59.0   1.03
pu         0.889   0.010   0.869    0.900  ...   152.0      53.0      22.0   1.03
mu         0.149   0.032   0.099    0.202  ...     7.0       7.0      57.0   1.23
mus        0.180   0.038   0.136    0.278  ...    86.0      97.0      63.0   1.03
gamma      0.218   0.037   0.156    0.284  ...   113.0     120.0      91.0   0.99
Is_begin   3.563   3.238   0.141    9.788  ...    68.0      69.0      77.0   1.03
Ia_begin  17.776  11.803   0.160   39.383  ...    33.0      23.0      39.0   1.07
E_begin    6.213   4.610   0.090   13.701  ...    70.0      50.0      59.0   1.03

[8 rows x 11 columns]