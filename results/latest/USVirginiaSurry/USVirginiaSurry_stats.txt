0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -438.34    23.19
p_loo       20.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.300  0.035   0.237    0.348  ...    67.0      62.0      34.0   1.03
pu        0.890  0.017   0.874    0.900  ...    29.0      27.0      35.0   1.11
mu        0.147  0.027   0.100    0.193  ...     7.0       7.0      14.0   1.24
mus       0.156  0.029   0.109    0.215  ...    57.0      58.0      59.0   1.02
gamma     0.187  0.043   0.117    0.263  ...    68.0      70.0      36.0   1.01
Is_begin  0.759  0.731   0.001    2.218  ...    10.0       7.0      42.0   1.27
Ia_begin  2.312  2.032   0.169    6.001  ...    60.0      62.0      55.0   1.05
E_begin   1.127  1.477   0.005    3.328  ...    43.0       9.0      43.0   1.20

[8 rows x 11 columns]