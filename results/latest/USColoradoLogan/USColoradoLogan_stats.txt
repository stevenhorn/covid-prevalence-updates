0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1086.68    50.49
p_loo       44.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)          8    2.7%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.222  0.047   0.153    0.312  ...   152.0     152.0      52.0   1.02
pu        0.722  0.014   0.701    0.747  ...    77.0      69.0      58.0   1.07
mu        0.146  0.018   0.115    0.178  ...    47.0      46.0      37.0   1.03
mus       0.187  0.028   0.143    0.241  ...    63.0      55.0      40.0   1.02
gamma     0.259  0.046   0.195    0.350  ...   152.0     141.0      79.0   1.02
Is_begin  0.566  0.706   0.019    2.612  ...   115.0     137.0      54.0   0.98
Ia_begin  0.832  0.691   0.028    1.934  ...    46.0     116.0      86.0   1.05
E_begin   0.656  0.601   0.020    1.767  ...   112.0      91.0      96.0   1.02

[8 rows x 11 columns]