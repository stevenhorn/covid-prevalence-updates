0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -1542.61    28.51
p_loo       27.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      266   89.9%
 (0.5, 0.7]   (ok)         25    8.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    0    0.0%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.257  0.055   0.166    0.350  ...    73.0      83.0      36.0   1.08
pu         0.804  0.051   0.717    0.886  ...    48.0      47.0      59.0   1.04
mu         0.158  0.016   0.119    0.181  ...     7.0       7.0      20.0   1.26
mus        0.228  0.045   0.155    0.291  ...   152.0     138.0      59.0   1.01
gamma      0.362  0.048   0.264    0.442  ...   128.0     141.0      83.0   1.00
Is_begin   2.630  2.275   0.021    6.320  ...     8.0       7.0      38.0   1.25
Ia_begin  10.257  6.763   1.362   25.151  ...    79.0      67.0      38.0   0.99
E_begin    6.958  7.919   0.058   22.571  ...    26.0      13.0      59.0   1.12

[8 rows x 11 columns]