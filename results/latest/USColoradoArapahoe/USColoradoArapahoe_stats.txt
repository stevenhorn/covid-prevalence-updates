7 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1392.07    22.84
p_loo       27.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      270   91.5%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.244   0.060   0.157    0.340  ...    29.0      30.0      40.0   1.03
pu         0.749   0.044   0.700    0.838  ...    58.0      60.0      21.0   1.01
mu         0.107   0.017   0.074    0.135  ...    30.0      29.0      60.0   1.07
mus        0.173   0.034   0.123    0.243  ...    66.0      98.0      19.0   0.98
gamma      0.273   0.053   0.174    0.367  ...    34.0      55.0      17.0   1.06
Is_begin   9.658   5.554   0.086   18.819  ...    32.0      30.0      34.0   1.22
Ia_begin  25.644  11.539   7.987   45.222  ...    69.0      64.0      36.0   1.06
E_begin   37.416  18.656   4.977   66.558  ...    68.0      62.0      99.0   0.99

[8 rows x 11 columns]