0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -632.42    27.81
p_loo       25.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         12    4.1%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.278  0.049   0.187    0.344  ...    43.0      44.0      39.0   1.02
pu        0.886  0.016   0.860    0.900  ...    77.0      64.0      57.0   1.00
mu        0.123  0.023   0.077    0.160  ...    14.0      13.0      42.0   1.12
mus       0.164  0.031   0.109    0.218  ...   152.0     152.0     100.0   0.98
gamma     0.201  0.038   0.138    0.266  ...   124.0     122.0      59.0   1.01
Is_begin  0.651  0.552   0.024    1.707  ...    83.0      61.0     100.0   1.01
Ia_begin  1.332  1.702   0.011    3.309  ...    98.0      82.0      56.0   1.01
E_begin   0.548  0.551   0.014    1.550  ...    77.0      64.0      83.0   1.01

[8 rows x 11 columns]