0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -522.82    37.96
p_loo       27.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.047   0.184    0.339  ...    23.0      23.0      79.0   1.04
pu        0.844  0.018   0.813    0.878  ...    20.0      20.0      55.0   1.05
mu        0.139  0.026   0.099    0.177  ...    15.0      13.0      49.0   1.13
mus       0.167  0.033   0.105    0.220  ...    33.0      35.0      58.0   1.04
gamma     0.183  0.038   0.115    0.245  ...   109.0     110.0      70.0   1.04
Is_begin  0.703  0.657   0.006    1.608  ...    82.0      55.0      60.0   1.02
Ia_begin  1.787  1.829   0.029    5.472  ...    89.0      66.0      39.0   0.99
E_begin   0.680  0.806   0.040    1.912  ...    94.0      68.0      49.0   1.00

[8 rows x 11 columns]