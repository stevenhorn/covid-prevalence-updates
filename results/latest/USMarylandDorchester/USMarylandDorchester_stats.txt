0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -818.47    36.08
p_loo       27.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)         2    0.7%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.273  0.049   0.193    0.350  ...    38.0      35.0      58.0   1.01
pu        0.851  0.037   0.782    0.900  ...    16.0      10.0      37.0   1.22
mu        0.133  0.023   0.082    0.163  ...    10.0      10.0      27.0   1.18
mus       0.183  0.036   0.107    0.236  ...    43.0      43.0      60.0   1.01
gamma     0.217  0.033   0.156    0.273  ...   113.0     152.0      72.0   1.05
Is_begin  0.936  0.912   0.006    2.516  ...   102.0      56.0      49.0   1.01
Ia_begin  2.316  2.233   0.037    6.605  ...    54.0      49.0      39.0   1.04
E_begin   1.077  1.160   0.046    3.393  ...    81.0      59.0      58.0   1.03

[8 rows x 11 columns]