0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1025.63    45.56
p_loo       39.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      280   94.9%
 (0.5, 0.7]   (ok)         10    3.4%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.280  0.044   0.201    0.350  ...    28.0      27.0      60.0   1.05
pu        0.878  0.013   0.860    0.899  ...    72.0      74.0      65.0   1.02
mu        0.144  0.029   0.098    0.202  ...     5.0       6.0      16.0   1.30
mus       0.183  0.030   0.134    0.232  ...    58.0      69.0      38.0   1.01
gamma     0.225  0.060   0.130    0.334  ...   102.0     115.0      60.0   1.02
Is_begin  1.028  0.889   0.013    3.010  ...   131.0     152.0      93.0   1.00
Ia_begin  2.189  1.832   0.072    5.064  ...    59.0      65.0      59.0   1.04
E_begin   1.137  1.229   0.011    3.252  ...    90.0      65.0      38.0   1.01

[8 rows x 11 columns]