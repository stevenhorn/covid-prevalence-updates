0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -827.15    58.40
p_loo       32.51        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.061   0.174    0.350  ...    77.0      71.0      38.0   1.01
pu        0.875  0.017   0.839    0.897  ...    35.0      33.0      59.0   1.07
mu        0.119  0.022   0.078    0.158  ...    41.0      37.0      40.0   1.00
mus       0.176  0.037   0.123    0.234  ...   137.0     122.0      81.0   1.00
gamma     0.206  0.044   0.131    0.286  ...   148.0     142.0     100.0   0.99
Is_begin  0.836  0.749   0.019    2.353  ...   122.0      77.0      49.0   1.01
Ia_begin  1.718  2.043   0.017    6.501  ...    97.0      68.0      59.0   1.02
E_begin   0.723  0.754   0.003    1.668  ...    93.0      96.0      31.0   1.03

[8 rows x 11 columns]