0 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo -1179.54    42.30
p_loo       37.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      288   93.2%
 (0.5, 0.7]   (ok)         14    4.5%
   (0.7, 1]   (bad)         4    1.3%
   (1, Inf)   (very bad)    3    1.0%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.246   0.052   0.172    0.349  ...    43.0      54.0      30.0   1.17
pu          0.803   0.049   0.732    0.896  ...    26.0      26.0      22.0   1.06
mu          0.171   0.027   0.130    0.216  ...    20.0      21.0      51.0   1.04
mus         0.213   0.035   0.153    0.285  ...   152.0     152.0     100.0   1.02
gamma       0.306   0.053   0.213    0.400  ...    93.0      96.0      41.0   1.01
Is_begin   11.766   6.478   1.442   24.274  ...    66.0      61.0      33.0   1.01
Ia_begin   97.201  31.366  43.855  149.244  ...    46.0      47.0      49.0   1.04
E_begin   118.591  60.585   5.041  227.274  ...    46.0      49.0      57.0   1.05

[8 rows x 11 columns]