0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1205.63    40.24
p_loo       35.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      272   92.2%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    2    0.7%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.237   0.054   0.152    0.328  ...   152.0     152.0      34.0   1.05
pu         0.759   0.042   0.700    0.832  ...    92.0      49.0      42.0   1.03
mu         0.150   0.025   0.117    0.194  ...    20.0      19.0      19.0   1.10
mus        0.235   0.035   0.161    0.292  ...   132.0     113.0      60.0   1.03
gamma      0.401   0.075   0.321    0.609  ...    93.0     136.0      32.0   1.03
Is_begin  13.541   7.925   1.310   29.547  ...    40.0      34.0      20.0   1.13
Ia_begin  32.146  13.995   1.070   53.836  ...    32.0      37.0      55.0   1.04
E_begin   51.792  23.006  16.238   95.131  ...    12.0      13.0      20.0   1.12

[8 rows x 11 columns]