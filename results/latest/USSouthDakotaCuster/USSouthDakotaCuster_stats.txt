0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -555.82    23.07
p_loo       21.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         16    5.4%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.051   0.163    0.339  ...    58.0      61.0      56.0   1.02
pu        0.824  0.031   0.749    0.860  ...    18.0      30.0      15.0   1.04
mu        0.129  0.024   0.089    0.165  ...    10.0      10.0      59.0   1.20
mus       0.166  0.030   0.121    0.222  ...   115.0     134.0     100.0   1.02
gamma     0.200  0.042   0.124    0.271  ...   123.0     133.0      39.0   1.03
Is_begin  0.483  0.612   0.001    2.066  ...    20.0      28.0      21.0   1.05
Ia_begin  0.722  1.417   0.003    3.125  ...    89.0      73.0      40.0   1.04
E_begin   0.269  0.416   0.000    1.186  ...    37.0      14.0      37.0   1.12

[8 rows x 11 columns]