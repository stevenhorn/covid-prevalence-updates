0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1260.83    28.06
p_loo       23.41        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         5    1.7%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.046   0.185    0.340  ...    86.0      86.0      59.0   0.99
pu        0.800  0.034   0.739    0.861  ...    27.0      31.0      96.0   1.07
mu        0.114  0.023   0.073    0.159  ...    12.0      12.0      40.0   1.15
mus       0.159  0.031   0.109    0.211  ...   152.0     152.0      88.0   1.00
gamma     0.197  0.033   0.142    0.268  ...    84.0     118.0      72.0   1.10
Is_begin  0.957  0.700   0.105    2.228  ...   114.0     152.0      93.0   0.99
Ia_begin  2.259  1.972   0.020    5.837  ...    91.0      87.0      60.0   1.05
E_begin   1.169  1.074   0.009    3.065  ...    77.0      58.0      43.0   1.02

[8 rows x 11 columns]