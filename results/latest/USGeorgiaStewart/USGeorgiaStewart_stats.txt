0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -815.06    41.25
p_loo       38.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      247   83.7%
 (0.5, 0.7]   (ok)         37   12.5%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    3    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.208  0.049   0.150    0.303  ...    55.0      41.0      55.0   1.01
pu        0.722  0.020   0.701    0.762  ...   115.0      98.0      60.0   1.00
mu        0.119  0.029   0.081    0.177  ...     4.0       5.0      24.0   1.43
mus       0.196  0.039   0.135    0.279  ...    56.0      65.0      59.0   1.01
gamma     0.237  0.056   0.117    0.334  ...    81.0      89.0      30.0   1.00
Is_begin  0.729  0.829   0.004    2.165  ...   102.0      52.0      60.0   1.00
Ia_begin  1.238  1.482   0.009    4.069  ...    86.0      45.0      44.0   1.01
E_begin   0.745  1.355   0.003    2.908  ...    93.0      58.0      60.0   1.04

[8 rows x 11 columns]