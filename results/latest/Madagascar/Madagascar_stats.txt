0 Divergences 
Passed validation 
Computed from 80 by 296 log-likelihood matrix

         Estimate       SE
elpd_loo -1399.78    26.01
p_loo       24.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      267   90.2%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         8    2.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.049   0.183    0.348  ...    69.0      40.0      14.0   1.04
pu        0.874  0.024   0.825    0.900  ...    19.0      12.0      34.0   1.14
mu        0.147  0.032   0.102    0.219  ...     4.0       4.0      20.0   1.57
mus       0.174  0.032   0.127    0.238  ...   104.0      97.0      76.0   1.00
gamma     0.211  0.041   0.155    0.296  ...    94.0      92.0      59.0   1.02
Is_begin  1.032  0.918   0.003    2.736  ...   102.0      72.0      43.0   1.01
Ia_begin  0.694  0.608   0.031    1.910  ...    89.0      63.0      57.0   1.02
E_begin   0.769  0.840   0.005    2.570  ...    80.0      60.0      60.0   1.05

[8 rows x 11 columns]