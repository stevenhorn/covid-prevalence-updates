0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -877.00    53.92
p_loo       32.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      279   94.6%
 (0.5, 0.7]   (ok)         11    3.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.058   0.153    0.336  ...   100.0      94.0      60.0   1.02
pu        0.828  0.030   0.772    0.873  ...    66.0      77.0      52.0   1.01
mu        0.136  0.024   0.103    0.186  ...    49.0      48.0      33.0   1.04
mus       0.204  0.035   0.159    0.289  ...    47.0      73.0      39.0   0.99
gamma     0.301  0.060   0.188    0.407  ...    76.0      90.0      53.0   0.99
Is_begin  0.719  0.696   0.002    2.009  ...    98.0      69.0      59.0   1.01
Ia_begin  1.249  1.409   0.006    3.870  ...   100.0     110.0      96.0   0.98
E_begin   0.528  0.654   0.000    1.399  ...   109.0      96.0      56.0   0.99

[8 rows x 11 columns]