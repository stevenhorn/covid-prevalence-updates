0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -748.08    34.66
p_loo       32.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      271   91.9%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.053   0.151    0.333  ...    52.0      64.0      56.0   1.04
pu        0.814  0.023   0.773    0.851  ...    44.0      43.0      43.0   0.99
mu        0.144  0.029   0.099    0.195  ...    26.0      23.0      58.0   1.04
mus       0.192  0.034   0.134    0.263  ...    49.0      45.0      44.0   1.03
gamma     0.252  0.052   0.183    0.364  ...   151.0     152.0      81.0   1.03
Is_begin  0.884  0.821   0.008    2.451  ...    75.0      87.0      40.0   1.01
Ia_begin  1.554  1.427   0.047    3.973  ...    21.0      14.0      59.0   1.15
E_begin   0.772  0.868   0.030    2.196  ...    40.0      35.0      59.0   1.08

[8 rows x 11 columns]