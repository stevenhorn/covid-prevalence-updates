0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -855.77    38.22
p_loo       31.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      278   94.2%
 (0.5, 0.7]   (ok)         13    4.4%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.054   0.172    0.342  ...    53.0      61.0      60.0   1.02
pu        0.762  0.033   0.701    0.809  ...     7.0       8.0      15.0   1.22
mu        0.147  0.020   0.112    0.184  ...    22.0      22.0      44.0   1.04
mus       0.224  0.030   0.178    0.290  ...   101.0     106.0      93.0   1.02
gamma     0.289  0.050   0.185    0.372  ...   130.0     152.0      72.0   1.00
Is_begin  0.907  0.836   0.002    2.291  ...    83.0      55.0      59.0   1.04
Ia_begin  1.806  1.833   0.116    5.208  ...    31.0      26.0      54.0   1.08
E_begin   0.717  0.730   0.009    2.425  ...    66.0      56.0      44.0   1.01

[8 rows x 11 columns]