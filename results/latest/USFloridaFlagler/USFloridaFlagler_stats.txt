0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -975.94    28.89
p_loo       25.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      266   90.2%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.043   0.150    0.312  ...    90.0      89.0      38.0   1.08
pu        0.811  0.050   0.727    0.883  ...    26.0      23.0      38.0   1.12
mu        0.133  0.027   0.078    0.183  ...     7.0       8.0      16.0   1.23
mus       0.172  0.033   0.119    0.233  ...    64.0      67.0      46.0   1.00
gamma     0.241  0.049   0.156    0.329  ...   120.0     128.0      80.0   1.01
Is_begin  1.446  0.904   0.038    3.040  ...    78.0      66.0      34.0   0.99
Ia_begin  3.063  2.716   0.162    7.749  ...   116.0      77.0      60.0   1.00
E_begin   2.662  2.769   0.034    8.397  ...    60.0      46.0      83.0   1.07

[8 rows x 11 columns]