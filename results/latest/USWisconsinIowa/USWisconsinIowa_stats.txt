0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -723.98    28.73
p_loo       19.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         18    6.1%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.059   0.157    0.338  ...   100.0     100.0      45.0   1.01
pu        0.849  0.021   0.811    0.883  ...    32.0      42.0      59.0   1.06
mu        0.116  0.027   0.072    0.165  ...    44.0      43.0      58.0   1.01
mus       0.162  0.040   0.101    0.240  ...    85.0     152.0      83.0   1.07
gamma     0.177  0.038   0.118    0.248  ...    67.0      71.0      91.0   1.03
Is_begin  0.738  0.902   0.002    2.337  ...   105.0      82.0      43.0   1.01
Ia_begin  1.329  1.259   0.029    4.434  ...    97.0     113.0     100.0   0.99
E_begin   0.756  0.930   0.003    1.998  ...    65.0      92.0      59.0   1.02

[8 rows x 11 columns]