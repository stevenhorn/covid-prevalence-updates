0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -913.28    25.24
p_loo       17.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      264   89.5%
 (0.5, 0.7]   (ok)         20    6.8%
   (0.7, 1]   (bad)        10    3.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.050   0.152    0.310  ...    32.0      29.0      33.0   1.08
pu        0.826  0.018   0.791    0.860  ...    34.0      33.0      37.0   1.06
mu        0.129  0.022   0.088    0.167  ...    30.0      29.0      40.0   1.02
mus       0.169  0.033   0.116    0.229  ...    40.0      43.0      77.0   1.01
gamma     0.200  0.051   0.124    0.285  ...    58.0      65.0      60.0   1.03
Is_begin  0.959  0.659   0.127    2.325  ...   107.0      78.0      60.0   1.01
Ia_begin  2.302  2.208   0.007    5.938  ...    33.0      28.0      40.0   1.06
E_begin   0.887  0.814   0.040    2.454  ...    69.0      57.0      60.0   1.03

[8 rows x 11 columns]