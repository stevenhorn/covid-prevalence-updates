0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -811.37    45.71
p_loo       32.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      277   93.9%
 (0.5, 0.7]   (ok)         15    5.1%
   (0.7, 1]   (bad)         1    0.3%
   (1, Inf)   (very bad)    2    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.063   0.151    0.338  ...    71.0      61.0      60.0   1.01
pu        0.757  0.027   0.708    0.799  ...    75.0      87.0      55.0   1.02
mu        0.147  0.020   0.112    0.188  ...    41.0      62.0      95.0   1.04
mus       0.207  0.037   0.160    0.280  ...    79.0      78.0      59.0   1.04
gamma     0.247  0.044   0.172    0.322  ...   152.0     152.0      96.0   0.98
Is_begin  0.895  0.891   0.024    2.377  ...   103.0      79.0      37.0   1.02
Ia_begin  1.582  1.490   0.034    4.350  ...    59.0      69.0      66.0   1.05
E_begin   0.719  0.646   0.002    2.048  ...    74.0      47.0      37.0   1.05

[8 rows x 11 columns]