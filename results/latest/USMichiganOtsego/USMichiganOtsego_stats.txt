0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -683.60    27.23
p_loo       20.08        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      276   93.6%
 (0.5, 0.7]   (ok)         14    4.7%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.282  0.049   0.196    0.349  ...    77.0      58.0      57.0   1.02
pu        0.888  0.013   0.862    0.900  ...    95.0      34.0      40.0   1.04
mu        0.147  0.025   0.105    0.191  ...    19.0      20.0      53.0   1.05
mus       0.191  0.033   0.140    0.254  ...   152.0     152.0      87.0   0.98
gamma     0.271  0.038   0.206    0.348  ...    63.0      35.0      59.0   1.06
Is_begin  1.568  1.033   0.052    3.474  ...    86.0      90.0      57.0   1.05
Ia_begin  4.040  2.908   0.199    8.904  ...    60.0      57.0      60.0   1.03
E_begin   3.449  3.624   0.024    9.156  ...    88.0      92.0      40.0   1.01

[8 rows x 11 columns]