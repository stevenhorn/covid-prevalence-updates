0 Divergences 
Failed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -956.10    31.07
p_loo       27.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      274   92.9%
 (0.5, 0.7]   (ok)         17    5.8%
   (0.7, 1]   (bad)         3    1.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.051   0.170    0.341  ...    98.0     100.0      15.0   1.02
pu        0.862  0.018   0.836    0.891  ...    52.0      72.0      65.0   1.04
mu        0.118  0.026   0.076    0.172  ...    23.0      23.0      47.0   1.10
mus       0.155  0.028   0.115    0.233  ...   133.0     145.0      88.0   1.03
gamma     0.169  0.027   0.116    0.214  ...   107.0      94.0      47.0   1.02
Is_begin  0.753  0.902   0.000    2.326  ...   114.0      72.0      42.0   1.03
Ia_begin  1.724  1.469   0.073    4.805  ...    97.0      68.0      65.0   1.06
E_begin   0.857  0.836   0.024    2.597  ...    26.0      15.0      54.0   1.11

[8 rows x 11 columns]