0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -904.47    23.50
p_loo       23.75        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      264   89.5%
 (0.5, 0.7]   (ok)         23    7.8%
   (0.7, 1]   (bad)         4    1.4%
   (1, Inf)   (very bad)    4    1.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.048   0.178    0.333  ...   152.0     152.0      69.0   1.00
pu        0.798  0.057   0.711    0.894  ...    60.0      54.0      24.0   1.04
mu        0.103  0.017   0.078    0.136  ...    55.0      55.0      96.0   1.01
mus       0.157  0.038   0.090    0.225  ...   152.0     152.0      48.0   1.03
gamma     0.217  0.038   0.160    0.293  ...   143.0     152.0      54.0   1.07
Is_begin  1.333  0.920   0.046    2.753  ...    70.0      51.0      59.0   1.03
Ia_begin  3.432  2.537   0.251    7.874  ...   126.0      99.0      60.0   1.01
E_begin   2.239  2.292   0.084    5.608  ...   103.0     124.0      61.0   0.99

[8 rows x 11 columns]