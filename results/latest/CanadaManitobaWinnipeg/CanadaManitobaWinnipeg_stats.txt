0 Divergences 
Passed validation 
Computed from 80 by 309 log-likelihood matrix

         Estimate       SE
elpd_loo -1141.57    34.47
p_loo       23.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      285   92.2%
 (0.5, 0.7]   (ok)         15    4.9%
   (0.7, 1]   (bad)         8    2.6%
   (1, Inf)   (very bad)    1    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.278   0.046   0.197    0.349  ...    87.0      90.0      39.0   1.01
pu         0.870   0.035   0.813    0.899  ...    34.0      46.0      31.0   1.05
mu         0.158   0.035   0.110    0.234  ...     6.0       7.0      17.0   1.28
mus        0.188   0.031   0.139    0.244  ...   152.0     152.0      49.0   1.02
gamma      0.254   0.046   0.177    0.336  ...   105.0     104.0      80.0   1.03
Is_begin   8.224   6.994   0.114   20.694  ...   146.0     103.0      51.0   1.00
Ia_begin  64.089  33.770   0.712  123.257  ...    72.0      56.0      22.0   1.09
E_begin   38.263  30.243   0.328  100.727  ...    92.0      89.0      54.0   0.99

[8 rows x 11 columns]