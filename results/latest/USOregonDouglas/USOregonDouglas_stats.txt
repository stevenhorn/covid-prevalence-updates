0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -704.55    27.51
p_loo       24.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      275   93.2%
 (0.5, 0.7]   (ok)         19    6.4%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    1    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.283  0.051   0.191    0.350  ...   134.0     152.0      32.0   0.99
pu        0.884  0.016   0.856    0.900  ...   126.0     100.0      58.0   1.01
mu        0.165  0.033   0.102    0.231  ...    21.0      20.0      40.0   1.08
mus       0.181  0.031   0.133    0.233  ...    88.0      94.0      72.0   1.02
gamma     0.234  0.045   0.163    0.328  ...   120.0     130.0      58.0   1.04
Is_begin  1.016  1.088   0.000    3.015  ...   108.0      34.0      37.0   1.04
Ia_begin  3.188  2.196   0.026    6.811  ...    60.0      71.0      53.0   1.03
E_begin   1.556  1.694   0.096    4.710  ...    26.0      62.0      22.0   1.06

[8 rows x 11 columns]