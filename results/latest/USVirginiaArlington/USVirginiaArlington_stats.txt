0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo -1127.55    18.06
p_loo       22.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      266   90.2%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         7    2.4%
   (1, Inf)   (very bad)    1    0.3%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.243  0.052   0.169    0.343  ...    31.0      25.0      24.0   1.06
pu         0.767  0.044   0.703    0.840  ...    56.0      61.0      35.0   1.01
mu         0.111  0.018   0.081    0.151  ...    28.0      25.0      38.0   1.03
mus        0.165  0.025   0.124    0.217  ...    61.0      67.0      24.0   1.14
gamma      0.224  0.031   0.181    0.293  ...    74.0      96.0      88.0   1.03
Is_begin   4.604  2.508   0.181    8.353  ...    91.0      77.0      59.0   1.04
Ia_begin  10.017  6.675   1.174   22.105  ...    85.0      72.0      43.0   1.03
E_begin   10.731  6.918   1.579   24.567  ...    66.0      60.0      59.0   1.02

[8 rows x 11 columns]