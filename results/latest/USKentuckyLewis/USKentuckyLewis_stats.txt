0 Divergences 
Passed validation 
Computed from 80 by 295 log-likelihood matrix

         Estimate       SE
elpd_loo  -696.73    30.28
p_loo       33.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      268   90.8%
 (0.5, 0.7]   (ok)         21    7.1%
   (0.7, 1]   (bad)         6    2.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.057   0.163    0.342  ...    86.0      78.0      42.0   1.01
pu        0.793  0.033   0.737    0.845  ...     6.0       6.0      21.0   1.34
mu        0.140  0.028   0.099    0.191  ...    11.0      11.0      57.0   1.18
mus       0.189  0.036   0.137    0.265  ...    79.0     134.0      81.0   1.09
gamma     0.249  0.038   0.182    0.329  ...    77.0      70.0      53.0   1.02
Is_begin  0.821  0.719   0.005    2.281  ...    51.0      34.0      61.0   1.05
Ia_begin  1.526  1.829   0.026    5.026  ...    67.0      82.0      60.0   1.02
E_begin   0.761  0.945   0.003    2.723  ...    45.0      83.0      46.0   1.05

[8 rows x 11 columns]